//abc
const _ = require('lodash');
const sails = require('sails');
_.defaults(sails.config.routes, {
 
 'GET    /api/user': 'app/UserController.getUser',
 'PUT    /api/user/verifyUser': 'app/UserController.verifyUser',
 'PUT    /api/user/v2/verifyUser': 'app/UserController.verifyUserV2',
 'GET    /api/user/:id': 'app/UserController.getProfile',
 'PUT    /api/user/updateSetting': 'app/UserController.updateSetting',
 'GET    /api/province': 'app/ProviceController.getProvince',
 'GET    /api/provinceSeller': 'app/ProviceController.getProvinceSeller',
 'GET    /api/provinceWarningSeller': 'app/ProviceController.getProvinceWarningSeller',
 'GET    /api/bank': 'app/UserController.getBank',
 'GET    /api/reason': 'app/ReasonController.getReason',
 'PUT    /api/user/updateProfile': 'app/UserController.updateProfile',
 'PUT    /api/user/avatar': 'app/UserController.updateAvatar',
 'POST    /api/user/login': 'app/UserController.login',
 'POST    /api/user/changePasswd': 'app/UserController.changePasswd',
  'POST    /api/user/v2/changePasswd': 'app/UserController.changePasswdV2',
 'POST    /api/user/forgotPasswd': 'app/UserController.forgotPasswd',
 'GET    /api/user/loginWithFacebook': 'app/UserController.loginWithFacebook',
 'POST    /api/user/updatePhoneAndPass': 'app/UserController.updatePhoneAndPass',
 'POST    /api/user/mergeSeller': 'app/UserController.mergeSeller',
 'POST    /api/user/logout': 'app/UserController.logout',
 'PUT    /api/user/fixAvatar': 'app/UserController.fixAvatar',


//

 // comment
 'GET     /api/comment': 'app/CommentController.getComments',
 'POST     /api/comment': 'app/CommentController.createComment',
 'PUT      /api/comment': 'app/CommentController.updateComment',
 'DELETE   /api/comment/:id': 'app/CommentController.deleteComment',
  'GET     /api/comment/send': 'app/CommentController.getSendComments',

  'GET     /api/v2/comment': 'app/CommentController.getReceiverCommentsV2',
   'GET     /api/v2/comment/send': 'app/CommentController.getSendCommentsV2',

 // bank
  'POST    /api/sellerBank': 'app/UserBankController.createUserBank',
  'GET    /api/sellerBank/:seller_id': 'app/UserBankController.getBankBySellerId',
  'PUT    /api/sellerBank': 'app/UserBankController.updateUserBank',

  //device
  'POST     /api/device': 'app/DeviceController.registerDevice',
   //like
  'POST     /api/like': 'app/LikeController.createLike',
  'GET     /api/like': 'app/LikeController.getLikes',
  'DELETE     /api/like': 'app/LikeController.deleteLike',

  //report
  'POST     /api/report/sellerExisted': 'app/ReportController.reportForSellerExisted',
  'POST     /api/report/sellerNotExist': 'app/ReportController.reportForSellerNotExist',
  'POST     /api/report/feedback': 'app/ReportController.createFeedback',
  'GET     /api/report': 'app/ReportController.listReport',
  'GET     /api/report/listReported': 'app/ReportController.listReportViaReporter',
  'GET     /api/report/getDetail': 'app/ReportController.getDetailReport',
  'GET     /api/report/getFeedBack': 'app/ReportController.getFeedBack',
  'DELETE     /api/report': 'app/ReportController.deleteReport',

  // System notification 

   'GET     /api/systemNotification': 'app/SystemNotificationController.getSystemNotification',
   'POST     /api/notification': 'app/SystemNotificationController.offNotification',

   // ///

   //  'GET      /api/notification': 'app/NotificationController.getNotifications',
   //  'GET      /api/countNotification': 'app/NotificationController.countNotifications',
   'GET     /v2/api/user/loginWithFacebook': 'app/AuthenController.loginWithFacebookV2CallBack',






});
