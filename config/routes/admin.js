const _ = require('lodash');
const sails = require('sails');
_.defaults(sails.config.routes, {
    'POST    /admin/account/login': 'admin/AccountController.login',
    'GET    /admin/account/profile/:id': 'admin/AccountController.getProfile',
    'POST    /admin/account/register': 'admin/AccountController.register',
    'PUT    /admin/account/changepw': 'admin/AccountController.changePassword',
    'POST    /admin/account/refreshToken': 'admin/AccountController.getNewToken',

    'GET    /admin/summary': 'admin/SummaryController.getSummary',

    'GET    /admin/seller/profile': 'admin/SellerController.getProfile',
    'PUT    /admin/seller/profile': 'admin/SellerController.updateProfile',
    'GET    /admin/seller/warning': 'admin/SellerController.getWarningSeller',
    'GET    /admin/seller/kyc': 'admin/SellerController.getKycSeller',
    'GET    /admin/seller/list': 'admin/SellerController.getListSeller',

    'POST    /admin/seller': 'admin/SellerController.createSeller',
    'PUT     /admin/seller': 'admin/SellerController.updateSeller',
    'DELETE  /admin/seller': 'admin/SellerController.deleteSeller',
    'PUT     /admin/seller/status': 'admin/SellerController.updateStatus',
    'POST    /admin/seller/uploadFileExcel': 'admin/SellerController.uploadFileExcel',

    

    'GET     /admin/seller/banks': 'admin/SellerBankController.getSellerBank',
    'GET     /admin/seller/banks/:seller_bank_id': 'admin/SellerBankController.getDetailSellerBank',
    'POST    /admin/seller/banks': 'admin/SellerBankController.createSellerBank',
    'PUT     /admin/seller/banks': 'admin/SellerBankController.updateSellerBank',
    'DELETE  /admin/seller/banks': 'admin/SellerBankController.deleteSellerBank',

    // notification 
    'GET     /admin/notification': 'admin/NotificationController.getNotification',
    'GET     /admin/notification/:id': 'admin/NotificationController.getDetailNotification',
    'POST     /admin/notification': 'admin/NotificationController.createNotification',
    'PUT     /admin/notification': 'admin/NotificationController.updateNotification',
    'DELETE  /admin/notification': 'admin/NotificationController.delNotification',


      // accuses 
    'POST     /admin/accuses/handle_invalid_accuses': 'admin/AccusesController.invalidAccusesHandle',
    'GET     /admin/accuses': 'admin/AccusesController.getListAccuses',
    'DELETE  /admin/accuses': 'admin/AccusesController.deleteReport',
    'GET     /admin/accuses/:accuses_id': 'admin/AccusesController.getDetailAccuses',
    'PUT     /admin/accuses': 'admin/AccusesController.handleAccuses',
    'GET    /admin/reason': 'admin/ReasonController.getReason',


    // bank 
    'GET     /admin/banks': 'admin/BankController.getBanks',
    'GET     /admin/banks/:id': 'admin/BankController.getDetailBank',
    'POST     /admin/banks': 'admin/BankController.createBank',
    'PUT     /admin/banks': 'admin/BankController.updateBank',
    'DELETE     /admin/banks': 'admin/BankController.deleteBank',


      // commetn
    'GET     /admin/comments': 'admin/SellerCommentController.getListComments',
    'GET     /admin/comments/receive': 'admin/SellerCommentController.getReceiveComments',
    'GET     /admin/comments/send': 'admin/SellerCommentController.getSendComments',
    'DELETE  /admin/comments/:id': 'admin/SellerCommentController.deleteComment',
    
 
 


});
