let {transports, createLogger, format} = require('winston');

let commonSettings = {
    colorize: false,
    maxsize: 10000000,
    maxfiles: 5,
    json: true,
     format: format.combine(
            format.timestamp({format: 'YYYY-MM-DD HH:mm:ss'}),
            format.json()
   )
};

let settings = [
    {filename: `${__dirname}/../logs/error.log`, level: 'error'},
    {filename: `${__dirname}/../logs/debug.log`, level: 'debug'},
    {filename: `${__dirname}/../logs/info.log`, level: 'info',silent:true}
];


let loggerSettings = {
    transports: [...settings.map(s => new transports.File({...s, ...commonSettings})), new transports.Console({
          format: format.combine(
            format.timestamp(),
            format.json()
        ),

    })],
    exitOnError: false
};

// This is the workaround
let winstonLogger = createLogger(loggerSettings);
let logger = {
    'info': function () {
        winstonLogger.info(...arguments);
    },
    'debug': function () {
        winstonLogger.debug(...arguments);
    },
    'error': function () {
        winstonLogger.error(...arguments);
    },
    'log': function () {
        winstonLogger.log(...arguments);
    }
};




// End of workaround

module.exports.log = {
    custom: logger,
    inspect: false
};