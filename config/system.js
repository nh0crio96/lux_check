module.exports.system = {
  token:{
    app_expire_time: 24 * 60 * 60 * 3000,
    admin_expire_time:  24 * 60 * 60 * 30,
    adm_refresh_expire_time:  24 * 60 * 60 * 32
  },
  firebaseConfig : {
   // apiKey:api_key_firebase,
    databaseUrl: "https://schoolly-37aa8.firebaseio.com",
    projectId: "schoolly-37aa8",
    storageBucket: "schoolly-37aa8.appspot.com",
    appId: "schoolly-37aa8"
  },
  PASS_DEAULT: "Lux@Famtech2020", 
  user_status:{
  	LOCK:-1,
  	PENDDING: 0,
  	ACTIVE: 1
  },
  maxFileSizeUpload: 10000000,
  invalid_extension:['heic','HEIC'],
  path_images: '/images/',
  path_cover: '/images/cover/',
  path_post: '/images/posts/',
  s3_img_type:{
  	before_identity:"before_img_identity_card",
  	after_identity:"after_img_identity_card",
    selfie:"selfie_img",
    avatar:"avatar",
    report:"report"
  },
  device_os: {
  	ANDROID: 1,
  	IOS: 2
  },
  facebookAuth : {
      appID : "812323829166059",
      keySecret : "72ccd741c86249032da615d65ace1d5f"
  },

 per_page:10,
 resize_img:{
    orther:{
      width_medium:650,
      width_low:500
    },
    avatar:{
      width_medium:300,
      width_low:150
    }
  }
};
