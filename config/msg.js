module.exports.msg = {
  user: {

     err_phone_required: {code: 'USER_PHONE', msg: 'Vui lòng nhập số điện thoại của bạn.'},
    err_phone_format_invalid: {code: 'PHONE_FORMAT_INVAILID', msg: 'Số điện thoại sai định dạng.'},
    err_password_required: {code: 'PASSWORD_REQUIRED', msg: 'Vui lòng nhập mật khẩu.'},
    err_password_not_match: {code: 'PASSWORD_NOT_MATCH', msg: 'Mật khẩu không đúng.'},
    err_phone_exist: {code: 'PHONE_EXIST', msg: 'Số điện thoại này đã tồn tại trên hệ thống.'},
    err_user_not_found: {code: 'USER_NOT_EXIST', msg: 'Seller không tồn tại trên hệ thống.'},
    err_phone_not_found: {code: 'PHONE_NOT_FOUND', msg: 'Số điện thoại này không tồn tại trên hệ thống'},
    err_incorrect_password: {code: 'INCORRECT_PASSWORD', msg: 'Mật khẩu không đúng.'},
    err_token_firebase_required: {code: 'TOKEN_FIREBASE_REQUIRED', msg: 'Token firebase  is required'},
    err_email_required: {code: 'EMAIL_REQUIRED', msg: 'Vui lòng nhập email.'},
    err_email_unique: {code: 'EMAIL_UNIQUE', msg: 'Email này đã tồn tại trên hệ thống.'},
    err_tonken_firebase_required: {code: 'TOKEN_FIREBASE_REQUIRED', msg: 'Token firebase is required'},
    err_old_password_required: {code: 'OLD_PASSWORD_REQUIRED', msg: 'Vui lòng nhập mật khẩu cũ.'},
    err_new_password_not_match: {code: 'NEW_PASSWORD_NOT_MATCH', msg: 'Mật khẩu mới và xác nhận phải giống nhau.'},
    err_new_password_required: {code: 'NEW_PASSWORD_REQUIRED', msg: 'Vui lòng nhập mật khẩu mới.'},
    err_old_password_not_match: {code: 'OLD_PASSWORD_NOT_MATCH', msg: 'Mật khẩu cũ không đúng.'},
    err_otp_required: {code: 'OTP_REQUIRED', msg: 'Vui lòng nhập mã OTP'},
    err_otp_wrong: {code: 'OTP_WRONG', msg: 'Mã OTP không đúng'},
    err_full_name_required: {code: 'FULLNAME_REQUIRED', msg: 'Vui lòng nhập tên của bạn'},
    err_user_id_required: {code: 'USER_ID_REQUIRED', msg: 'User ID is required'},
    err_cover_required: {code: 'COVER_REQUIRED', msg: 'Vui lòng upload ảnh cover của bạn.'},
    err_avatar_required: {code: 'AVATAR_REQUIRED', msg: 'Vui lòng upload avatar của bạn.'},
    err_province_required: {code: 'PROVINCE_REQUIRED', msg: 'Vui lòng chọn tỉnh thành'},
    err_website_required: {code: 'WEBSTTE_REQUIRED', msg: 'Vui lòng nhập web hoặc kênh bán hàng.'},
    err_website_format_invalid: {code: 'WEBSITE_FORMAT_INVAILID', msg: 'Định dạng website không hợp lệ.'},
    err_province_not_found: {code: 'PROVINCE_NAME_NOT_FOUND', msg: 'Tỉnh thành này không tồn tại trên hệ thống.'},
    err_before_identity_required: {code: 'BEFORE_IDENTITY_REQUIRED', msg: 'Vui lòng upload ảnh mặt trước của chứng minh thư.'},
    err_after_identity_required: {code: 'AFTER_IDENTITY_REQUIRED', msg: 'Vui lòng upload ảnh mặt sau của chứng minh thư.'},
    err_selfie_img_required: {code: 'SELFIE_IMG_REQUIRED', msg: 'Vui lòng upload ảnh selfie của bạn.'},
    err_password_wrong: {code: 'PASSWORD_WRONG', msg: 'Sai mật khẩu.'},
    err_login_facebook_failed: {code: 'LOGIN_FACEBOOK_FAILED', msg: 'Đăng nhập facebook thất bại.'},
    err_user_invalid: {code: 'USER_INVALID', msg: 'Seller không hợp lệ hoặc không tồn tại.'},
    err_param_agree_invalid: {code: 'PARAM_AGREE_INVALID', msg: 'Tham số khi đồng ý hoặc không là không hợp lệ.'},
    err_param_status_invalid: {code: 'PARAM_STATUS_INVALID', msg: 'Trạng seller không hợp lệ.'},
    err_user_token_required: {code: 'TOKEN_REQUIRED', msg: 'Nhập thông tin token'},
    err_user_token_not_found: {code: 'TOKEN_NOT_FOUND', msg: 'Không tìm thấy token trên hệ thống'},
    err_user_token_invalid: {code: 'TOKEN_INVALID', msg: 'Token không hợp lệ.'},
    err_verify_code_invalid: {code: 'VERIFY_CODE_INVALID', msg: 'Mã xác thực phải là số và lớn hơn 1001'},
    err_verify_code_exist: {code: 'VERIFY_CODE_EXIST', msg: 'Mã xác thực này đã tồn tại trên hệ thống.'},
    err_seller_have_verify_code: {code: 'VERIFY_CODE_EXIST_SELLER', msg: 'This seller has a verify code'},
    err_affect_user_not_found: {code: 'AFFECT_USER_NOT_EXIST', msg: 'The affect  user not found in the system'},
    err_seller_disble: {code: 'SELLER_DISABLE', msg: 'Không thể thực hiện duyệt, do seller đang bị disable'},
    err_extension_invalid: {code: 'EXTENSION_INVALID', msg: 'Định dạng ảnh không hỗ trợ..Vui lòng thử lại với định dạng ảnh khác'},
    err_upload_failed: {code: 'UPLOAD_FAILED', msg: 'Quá trình upload ảnh lỗi.Vui lòng thử lại với định dạng ảnh khác.'},
    err_old_pass_phone_require: {code: 'OLD_PASS_PHONE_REQUIRE', msg: 'Bạn cần nhập mật khẩu cũ hoặc số điện thoại'}
  
    
  },

  

 like: {
    err_like_user_required: {code: 'LIKE_USER_REQUIRED', msg: 'Like user is required'},
    err_affect_user_required: {code: 'AFFECT_USER_REQUIRED', msg: 'Cần chọn seller để thích.'},
    err_like_type_required: {code: 'LIKE_TYPE_REQUIRED', msg: 'Vui lòng nhập loại thích.'},
    err_user_liked: {code: 'USER_LIKED', msg: 'User liked'},
    err_like_not_found: {code: 'LIKE_NOT_FOUND', msg: 'Dữ liệu không tồn tại trên hệ thống.'},
    err_like_id_required: {code: 'LIKE_ID_REQUIRED', msg: 'Vui lòng nhập id của lượt thích này.'},
  },

    devices: {
    err_device_id_required: {code: 'DEVICE_ID_REQUIRED', msg: 'Hệ thống yêu cầu id của thiết bị và hệ điều hành tương ứng.'},
    err_device_token_required: {code: 'DEVICE_TOKEN_REQUIRED', msg: 'Vui lòng gửi token của thiết bị.'}
  },

  comment: {
     err_comment_id_required: {code: 'COMMENT_ID_REQUIRED', msg: 'Vui lòng chọn tin nhắn'},
    err_comment_not_found: {code: 'COMMENT_NOT_FOUND', msg: 'Tin nhắn không tồn tại trên hệ thống.'},
    err_sender_id_required: {code: 'SENDER_ID_REQUIRED', msg: 'Thiếu thông tin người gửi tin nhắn.'},
    err_receiver_id_required: {code: 'RECEICER_ID_REQUIRED', msg: 'Thiếu thông tin người nhận tin nhắn.'},
    err_content_required: {code: 'CONTENT_REQUIRED', msg: 'Vui lòng nhập nội dung của tin nhắn.'},
    err_sender_not_found: {code: 'SENDER_NOT_FOUND', msg: 'Seller gửi tin nhắn không tồn tại trên hệ thống.'},
    err_receiver_not_found: {code: 'RECEIVER_NOT_FOUND', msg: 'Seller nhận tin nhắn không tồn tại trên hệ thống.'},
  },

bank: {
   err_seller_bank_id_required: {code: 'SELLER_BANK_ID_REQUIRED', msg: 'Vui lòng chọn thông tin ngân hàng của seller này.'},
    err_bank_not_found: {code: 'BANK_NOT_FOUND', msg: 'Ngân hàng này không tồn tại trên hệ thống.'},
    err_bank_id_required: {code: 'BANK_ID_REQUIRED', msg: 'Vui lòng chọn ngân hàng.'},
    err_bank_name_required: {code: 'NAME_REQUIRED', msg: 'Vui lòng nhập tên ngân hàng.'},
    err_account_name_required: {code: 'ACCOUNT_NAME_REQUIRED', msg: 'Vui lòng nhập tên tài khoản.'},
    err_account_number_required: {code: 'ACCOUNT_NUMBER_REQUIRED', msg: 'Vui lòng nhập số tài khoản.'},
    err_seller_and_bank_exist: {code: 'SELLER_AND_BANK_EXIST', msg: 'Thông tin ngân hàng của seller này đã tồn tại trên hệ thống.'},
    err_account_number_exist: {code: 'ACCOUNT_NUMBER_EXIST', msg: 'Số tài khoản đã tồn tại trên hệ thống.'}
  },

   report: {
      affect_user_required: {code: 'AFECT_NOT_FOUND', msg: 'Affect seller not found'},
     reason_id_required: {code: 'REASON_ID_REQUIRED', msg: 'Vui lòng cho lý do cho tố cáo này.'},
     err_report_id_required: {code: 'REPORT_ID_REQUIRED', msg: 'Vui lòng nhập id của tố cáo.'},
     err_affect_user_required: {code: 'AFFECT_USER_REQUIRED', msg: 'Vui lòng nhập tên của người muốn tố cáo.'},
     err_report_id_required: {code: 'REPORT_ID_REQUIRED', msg: 'Vui lòng nhập id của tố cáo.'},
     err_feedback_content_required: {code: 'FEEDBACK_CONTENT_REQUIRED', msg: 'Vui lòng nhập nội dùng phản hồi.'},
     err_report_not_found: {code: 'REPORT_NOT_FOUND', msg: 'Không tìm thấy tố cáo trên hệ thống.'},
     err_param_status_invalid: {code: 'PARAM_STATUS_INVALID', msg: 'Trạng thái tố cáo không hợp lệ.'},
     err_report_unique: {code: 'REPORT_UNIQUE', msg: 'Tố cáo của bạn với seller này đã tồn tại trên hệ thống.'},
     err_cannot_handle_invalid_accuses: {code: 'CANNOT_HANDLE_INVALID_ACCUSES', msg: 'Không thể xử lý cho seller'},
     err_number_change_invalid: {code: 'NUMBER_CHANGE_INVALID', msg: 'Dữ liệu số không hợp lệ.'},
     err_less_than_or_equal: {code: 'LESS_THAN_OR_EQUAL', msg: 'Dữ liệu số phải nhỏ hơn hoặc bằng số tố cáo hiện tại.'},
     err_change_number_is_required: {code: 'CHANGE_NUMBER_IS_REQUIRED', msg: 'Cần nhập số để thay đổi.'},
  },

  notification: {
     err_id_required: {code: 'ID_NOTIFICATION_REQUIRED', msg: 'Thiếu id notification.'},
     err_notification_notfound: {code: 'NOTIFICATION_NOTFOUND', msg: 'Không tìm thấy thông báo trên hệ thống.'},
     err_notification_type_required: {code: 'NOTIFICATION_TYPE_REQUIRED', msg: 'Vui lòng nhập loại thông báo.'},
     err_notification_type_invalid: {code: 'NOTIFICATION_TYPE_INVALID', msg: 'Loại thông báo không hợp lệ.'},
     err_content_required: {code: 'CONTENT_REQUIRED', msg: 'Vui lòng nhập nội dung cho thông báo.'},
     err_title_required: {code: 'TITLE_REQUIRED', msg: 'Vui lòng nhập tiêu đề cho thông báo.'}
   
  },
  common:{
      err_seller_invalid: {code: 'SELLER_INVALID', msg: 'Seller không hợp lệ'},
  }
  
};
