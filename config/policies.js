/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

  'app/CommentController': {
    '*': 'isAuthorizedApp',
    'getSendComments': true,
    'getComments': true,
     'getSendCommentsV2': true,
    'getReceiverCommentsV2': true
  },

  'app/DeviceController': {
    '*': 'isAuthorizedApp'
  },


  'app/LikeController': {
    '*': 'isAuthorizedApp'
  },

  'app/ReasonController': {
    '*': 'isAuthorizedApp'
  },

   'app/ReportController': {
    '*': 'isAuthorizedApp',
    'listReport': true
  },

  'app/SystemNotificationController': {
    '*': 'isAuthorizedApp'
  },

   'app/UserBankController': {
    '*': 'isAuthorizedApp',
    'getBankBySellerId': true,
  },

  'app/NotificationController': {
    '*': 'isAuthorizedApp'
  },

  'app/UserController': {
    '*': 'isAuthorizedApp',
    'login': true,
    'forgotPasswd': true,
    'getUser':true,
    'loginWithFacebook': true,
    'mergeSeller':true,
    'getProfile': true,
    'fixAvatar':true
  },

   'app/ProviceController': {
    'getProvince': true,
    'getProvinceSeller':true,
    'getProvinceWarningSeller': true
  },

  // admin policies setting

  'admin/AccountController': {
    '*':'isAuthorizedAdmin',
    'login': true,
    'register': true,
    'getNewToken': true
  },

   'admin/AccusesController': {
    '*':'isAuthorizedAdmin'
  },

  'admin/NotificationController': {
    '*':'isAuthorizedAdmin'
  },

   'admin/BankController': {
     '*':'isAuthorizedAdmin'
  },

   'admin/ReasonController': {
      '*':'isAuthorizedAdmin'
  },

   'admin/SellerBankController': {
      '*':'isAuthorizedAdmin'
  },

   'admin/SellerCommentController': {
      '*':'isAuthorizedAdmin'
  },

  'admin/SellerController': {
      '*':'isAuthorizedAdmin'
  },

  'admin/SummaryController': {
      '*':'isAuthorizedAdmin'
  },

};
