 /**
 * @api {get} /summary Get summary
 * @apiName getSummary
 * @apiGroup Summary
 * @apiDescription Get summary.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 * 
 * 
 * @apiSuccess {Object} Object data
 */

