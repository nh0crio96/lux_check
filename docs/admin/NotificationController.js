 /**
 * @api {get} /notification Get list notification
 * @apiName getNotification
 * @apiGroup Notification
 * @apiDescription Get list notification.
 * @apiPermission: user
 * 
 * @apiHeader {String} authorization Authorization token
 * @apiParam {Number} page Page number, start from 1, optional.
 * @apiParam {Number} limit Number of items per page, default 10, optional.
 * 
 * @apiSuccess {Object} listNotification: contain array of notification object,panigate:panigate object.
 */


 /**
 * @api {post} /notification Create notification
 * @apiName createNotification
 * @apiGroup Notification
 * @apiDescription Create notification.
 * @apiPermission: user
 * 
 * @apiHeader {String} authorization Authorization token

 * @apiParam {String} title Required, Title of notification.
 * @apiParam {String} content Required, Content of notification.
 * 
 * @apiSuccess {Object} Data created.
 */


  /**
 * @api {put} /notification Update notification
 * @apiName updateNotification
 * @apiGroup Notification
 * @apiDescription Update notification.
 * @apiPermission: user
 * 
 * @apiHeader {String} authorization Authorization token

 * @apiParam {Number} id Required, id of notification.
 * @apiParam {String} title Required, Title of notification.
 * @apiParam {String} content Required, Content of notification.
 * 
 * @apiSuccess {Object} Data updated.
 */


 /**
 * @api {delete} /notification Delete notification
 * @apiName delNotification
 * @apiGroup Notification
 * @apiDescription Delete notification.
 * @apiPermission: user
 * 
 * @apiHeader {String} authorization Authorization token

 * @apiParam {Number} id Required, id of notification.
 * 
 * @apiSuccess {Object} Data success.
 */

   /**
 * @api {get} /notification/:id Get detail notification
 * @apiName getDetailNotification
 * @apiGroup Notification
 * @apiDescription Get detail notification.
 * @apiPermission user
 * 
 *
 * @apiHeader {String} authorization Authorization token  

 * @apiParam {Number} id Id of notification,Require.
 * 
 * @apiSuccess {Object}  Notification data object.
 */




