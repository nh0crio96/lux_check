 /**
 * @api {get} /banks Get list bank
 * @apiName getBanks
 * @apiGroup Bank
 * @apiDescription Get banks.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiSuccess {Array} data Array of bank objects.
 */


  /**
 * @api {POST} /banks Create new bank
 * @apiName createBank
 * @apiGroup Bank
 * @apiDescription  Create new bank
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 

 * @apiParam {String} bank_name Name of bank,Required.
 *
 * @apiSuccess {Array} data Array of bank objects.
 */


   /**
 * @api {Put} /banks Update  bank
 * @apiName updateBank
 * @apiGroup Bank
 * @apiDescription  Update  bank
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 

 * @apiParam {String} id Id of bank,Required.
 * @apiParam {String} bank_name Name of bank,Required.
 *
 * @apiSuccess {Array} data Array of bank objects.
 */

  /**
 * @api {Put} /banks Delete  bank
 * @apiName deleteBank
 * @apiGroup Bank
 * @apiDescription  Delete  bank
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 

 * @apiParam {String} id Id of bank,Required.

 *
 * @apiSuccess {Array} data Array of bank objects.
 */


  /**
 * @api {get} /banks/:id Get detail bank
 * @apiName getDetailBank
 * @apiGroup Bank
 * @apiDescription Get detail bank.
 * @apiPermission user
 * 
 *
 * @apiHeader {String} authorization Authorization token  

 * @apiParam {Number} id Id of bank,Require.
 * 
 * @apiSuccess {Object}  Bank data object.
 */




