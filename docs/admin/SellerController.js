  /**
 * @api {get} /seller/profile Get detail seller
 * @apiName getProfile
 * @apiGroup Profile
 * @apiDescription Get profile seller.
 * @apiPermission user
 * 
 *
 * @apiHeader {String} authorization Authorization token  

 * @apiParam {Number} user_id Id of seller,Require.
 * 
 * @apiSuccess {Object}  Objects of seller.
 */


 /**
 * @api {put} /seller/profile Update profile seller
 * @apiName updateProfile
 * @apiGroup Profile
 * @apiDescription Update profile seller.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiParam {Number} user_id Id of seller, required.
 * @apiParam {String} fullname Fullname of seller,required.
 * @apiParam {Number} sex=1 Sex of user ,optional.
 * @apiParam {String} phone_number Phone of user ,required.
 * @apiParam {String} address      Seller's address ,optional.
 * @apiParam {String} birthday     Bithday of seller,ex: 2020-01-03 ,required.
 * @apiParam {String} email        Email of user ,optional.

 * @apiSuccess {Object} Object .
 */


 /**
 * @api {get} /seller/warning Get list warning user
 * @apiName getWarningSeller
 * @apiGroup Warning
 * @apiDescription Get list warning user
 * @apiPermission user
 *  
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiParam {String} txtSearch Optional, Search text via fullname.
 * @apiParam {Number} page Page number, start from 1, optional.
 * @apiParam {Number} limit Number of items per page, default 10, optional.
 * 
 * @apiSuccess {Object} listSeller contain array of seller object.
 * @apiSuccess {Object} pagination panigate object.
 */



 /**
 * @api {get} /seller/kyc Get list kyc seller
 * @apiName getKycSeller
 * @apiGroup KYC
 * @apiDescription Get list kyc seller
 * @apiPermission user
 *  
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiParam {String} txtSearch Optional, Search text via fullname.
 * @apiParam {Number} page Page number, start from 1, optional.
 * @apiParam {Number} limit Number of items per page, default 10, optional.

 * @apiSuccess {Object} listSeller contain array of seller object.
 * @apiSuccess {Object} pagination panigate object.
 * 
 */



 /**
 * @api {get} /seller/list Get list seller
 * @apiName getListSeller
 * @apiGroup Users
 * @apiDescription Get list seller.
 * @apiPermission user
 * 
 *
 * @apiHeader {String} authorization Authorization token  

 
 * @apiParam {String} txtSearch Optional, Search text via fullname.
 * @apiParam {String} txtPhone Optional, Search text via phone number.
 * @apiParam {String} txtStatus Optional, Search text via status( Verify_level_two,Guest,Verify_level_one,Pending).
 * @apiParam {Number} page Page number, start from 1, optional.
 * @apiParam {Number} limit Number of items per page, default 10, optional.
 * 
 * @apiSuccess {Object} listSeller contain array of seller object.
 * @apiSuccess {Object} pagination panigate object.
 */



 /**
 * @api {post} /seller Create new seller
 * @apiName createSeller
 * @apiGroup Users
 * @apiDescription Create new seller.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiParam {String} fullname Fullname  of seller,required.
 * @apiParam {String} phone_number Phone of user ,required.
 * @apiParam {String} email        Email of user ,optional.
 * @apiParam {Number} sex=1 Sex of user ,optional.
 * @apiParam {String} address      Seller's address ,optional.
 * @apiParam {String} birthday     Bithday of seller,ex: 2020-01-03 ,required.
 * @apiParam {File} avatar_images  Avatar image  of user update, optional
 * 
 * 
 * @apiSuccess {Object}  Objects user update success.
 */

 /**
 * @api {put} /seller Update new seller
 * @apiName updateSeller
 * @apiGroup Users
 * @apiDescription Update new seller.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiParam {Number} seller_id Id of seller, required.
 * @apiParam {String} fullname Fullname  of seller,required.
 * @apiParam {Number} sex=1 Sex of user ,optional.
 * @apiParam {String} phone_number Phone of user ,required.
 * @apiParam {String} birthday     Bithday of seller,ex: 2020-01-03 ,required.
 * @apiParam {String} email        Email of user ,optional.
 * @apiParam {String} address      Seller's address ,optional.
 * @apiParam {File} avatar_images  Avatar image  of user update, optional
 * 
 * 
 * @apiSuccess {Object}  Objects user update success.
 */

 /**
 * @api {delete} /seller Delete seller
 * @apiName deleteSeller
 * @apiGroup Users
 * @apiDescription  Delete seller.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 * 
 * @apiParam {Number} seller_id Required, ID of seller who you want to delete.
 * 
 * @apiSuccess {Object} success data objects.
 */


  /**
 * @api {put} /seller/status Update status seller
 * @apiName updateStatus
 * @apiGroup Users
 * @apiDescription Update status selle.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiParam {Number} seller_id Id of seller, required.
 * @apiParam {String} note note from admin, optional.
 * @apiParam {Number} verify_code verfify , optional.
 * @apiParam {String} status status  of seller value in Verify_level_two,Guest,Verify_level_one,required.
 * 
 * 
 * @apiSuccess {Object}  Objects user update success.
 */



  /**
 * @api {post} /seller/uploadFileExcel Import  seller
 * @apiName uploadFileExcel
 * @apiGroup Users
 * @apiDescription Import  seller.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 *

 * @apiParam {File} fileImport  File import
 * 
 * 
 * @apiSuccess {Object}  listSellerValid List seller valid.
 * @apiSuccess {Object}  listSellerInValid List seller Invalid.
 */




