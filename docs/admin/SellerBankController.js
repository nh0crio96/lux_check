  /**
 * @api {get} /seller/banks Get list seller bank
 * @apiName getSellerBank
 * @apiGroup SellerBanks
 * @apiDescription Get list seller bank.
 * @apiPermission user
 * 
 *
 * 
 * @apiHeader {String} authorization Authorization token 
 * 
 * @apiParam {String} name Search via fullname or account name of seller, optional.
 * @apiParam {String} bankName Search via bank name, optional.
 * @apiParam {String} accountNumber Search via account number, optional.
 * @apiParam {Number} page=1 Page number, start from 1, optional.
 * @apiParam {Number} limit=10 Number of items per page, default 10, optional.
 * 
 * @apiSuccess {Array} data Array of comment objects.
 */


 /**
 * @api {post} /seller/banks Create seller bank
 * @apiName createSellerBank
 * @apiGroup SellerBanks
 * @apiDescription Create seller bank.
 * @apiPermission user
 *
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiParam {Number} bank_id Bank id, required.
 * @apiParam {Number} seller_id Id of seller, required.
 * @apiParam {String} account_name Account Name, required.
 * @apiParam {String} account_number Account number, required.
 * @apiParam {String} branch_name Branch name, optional.
 * 
 * @apiSuccess {Objet} seller bank objects.
 */


 /**
 * @api {put} /seller/banks Update seller bank
 * @apiName updateSellerBank
 * @apiGroup SellerBanks
 * @apiDescription Update seller bank.
 * @apiPermission user
 *
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiParam {Number} id Seller bank id, required.
 * @apiParam {Number} seller_id Id of seller, required.
 * @apiParam {Number} bank_id Bank id, required.
 * @apiParam {String} account_name Account Name, required.
 * @apiParam {String} account_number Account number, required.
 * @apiParam {String} branch_name Branch name, optional.
 * 
 * @apiSuccess {Objet} seller bank objects.
 */

  /**
 * @api {delete} /seller/banks Delete seller bank
 * @apiName deleteSellerBank
 * @apiGroup SellerBanks
 * @apiDescription Delete seller bank.
 * @apiPermission user
 *
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiParam {Number} id Seller bank id, required.

 * 
 * @apiSuccess {Objet} success object data.
 */


   /**
 * @api {get} /seller/banks/:seller_bank_id Get detail seller bank
 * @apiName getDetailSellerBank
 * @apiGroup SellerBanks
 * @apiDescription Get detail seller bank
 * @apiPermission user
 * 
 *
 * 
 * @apiHeader {String} authorization Authorization token 
 * 
 * @apiParam {Number} seller_bank_id Seller bank id, required.
 * 
 * @apiSuccess {Objet} Object data seller bank.
 */





