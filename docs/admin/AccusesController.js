 /**
 * @api {get} /accuses Get list accuses
 * @apiName getListAccuses
 * @apiGroup Accuses
 * @apiDescription Get list accuses.
 * @apiPermission user
 * 
 *
 * @apiHeader {String} authorization Authorization token  

 
 * @apiParam {String} txtName Optional, Search text via fullname of reporter or affect seller.
 * @apiParam {Number} reason Optional, Search text via reason.
 * @apiParam {String} txtStatus Optional, Search text via status report.
 * @apiParam {Number} page Page number, start from 1, optional.
 * @apiParam {Number} limit Number of items per page, default 10, optional.
 * 
 * @apiSuccess {Object} listSeller contain array of seller object.
 * @apiSuccess {Object} pagination panigate object.
 */


  /**
 * @api {delete} /accuses Delete accuses
 * @apiName deleteReport
 * @apiGroup Accuses
 * @apiDescription  Delete accuses.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 * 
 * @apiParam {Number} accuses_id Required, ID of accuses who you want to delete.
 * 
 * @apiSuccess {Object} data data objects.
 */


   /**
 * @api {put} /accuses Handle accuses
 * @apiName handleAccuses
 * @apiGroup Accuses
 * @apiDescription Handle accuses
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiParam {Number} accuses_id Required, ID of accuses .
 * @apiParam {String} status Required,status of accuses value in Reject,Accept.
 * 
 * 
 * @apiSuccess {Object}  data user update success.
 */


 /**
 * @api {get} /accuses/:accuses_id Get detail accuses
 * @apiName getDetailAccuses
 * @apiGroup Accuses
 * @apiDescription Get detail accuses.
 * @apiPermission user
 * 
 *
 * @apiHeader {String} authorization Authorization token  
 * @apiParam {Number} accuses_id Required,Id of accuses.
 * 
 * @apiSuccess {Object} accuses contain data of accuses.
 * @apiSuccess {Object} reporter_seller Reporter seller.
 * @apiSuccess {Object} affect_seller Affect seller.
 * @apiSuccess {Array}  dataImage List report image.
 * @apiSuccess {Array}  listFeedBack List report feedback.
 
 */


  /**
 * @api {post} /accuses/handle_invalid_accuses Handle invalid accuses
 * @apiName invalidAccusesHandle
 * @apiGroup Accuses
 * @apiDescription Handle invalid accuses.
 * @apiPermission user
 * 
 *
 * @apiHeader {String} authorization Authorization token  
 *
 * @apiParam {Number} affect_user_id Id of seller, required.
 * @apiParam {Number} reset_to_zezo=0 0:Allow custom,1:allow reset to zero,required.
 * @apiParam {Number} change_number     Number which admin want to change,required.
 * 
 * @apiSuccess {Object} user data after handle .
 
 
 */



