/**
 * @api {post} /account/login Login
 * @apiName login
 * @apiGroup Account
 * @apiDescription Login for account.
 * @apiPermission guest
 * 
 * @apiParam {String} email  User's email.
 * @apiParam {String} password User's password .
 *
 * @apiSuccess {Object}  Objects user and token.
 */


 /**
 * @api {post} /account/register Register
 * @apiName register
 * @apiGroup Account
 * @apiDescription Register for account.
 * @apiPermission guest
 * 
 * @apiParam {String} email  User's email.
 * @apiParam {String} password User's password .
 * @apiParam {String} username User's username .
 *
 * @apiSuccess {Object}  Objects user and token.
 */


  /**
 * @api {PUT} /account/changepw Change password
 * @apiName changePassword
 * @apiGroup Account
 * @apiDescription Change password
 * @apiPermission: user
 * 
 * @apiHeader {String} authorization Authorization token
 *
 * @apiParam {String} oldPass Old password,Required.
 * @apiParam {String} newPass New password ,Required
 * @apiParam {String} verify_new_pass New password fill again,Required
 * 
 * @apiSuccess {Object} data user  successfully.
 */


   /**
 * @api {get} /account/profile/:id Get detail account
 * @apiName getProfile
 * @apiGroup Account
 * @apiDescription Get detail user.
 * @apiPermission user
 * 
 *
 * @apiHeader {String} authorization Authorization token  

 * @apiParam {Number} id Id  of account,Required.
 * 
 * @apiSuccess {Object}  Objects of account.
 */


 /**
 * @api {post} /account/refreshToken Get new token
 * @apiName getNewToken
 * @apiGroup Account
 * @apiDescription Get new token.
 * @apiPermission guest
 * 
 * @apiParam {String} refresh_token  refresh token.

 *
 * @apiSuccess {Object}  Objects user and token.
 */
