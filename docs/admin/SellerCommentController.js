 /**
 * @api {get} /comments Get list comment
 * @apiName getListComments
 * @apiGroup Comment
 * @apiDescription Get list comment.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 * 
 * @apiParam {Number} seller_id Search via seller_id, Optional.
 * @apiParam {Number} seller_name Search via fullname, Optional.
 * @apiParam {Number} page=1 Page number, start from 1, optional.
 * @apiParam {Number} limit=10 Number of items per page, default 10, optional.
 * 
 * @apiSuccess {Object} listComments contain array of comment object.
 * @apiSuccess {Object} pagination panigate object.
 */


 /**
 * @api {delete} /comments/:id Delete comment
 * @apiName deleteComment
 * @apiGroup Comment
 * @apiDescription Delete comment.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 * 
 * @apiParam {Number} id Required, ID of comment who you want to delete.
 * 
 * @apiSuccess {Object} success data objects.
 */

 /**
 * @api {get} /comments/receive Get list receive comment
 * @apiName getReceiveComments
 * @apiGroup Comment
 * @apiDescription Get list receive comment.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 * 
 * @apiParam {Number} seller_id Receiver id, Required.
 * @apiParam {Number} page=1 Page number, start from 1, optional.
 * @apiParam {Number} limit=10 Number of items per page, default 10, optional.
 * 
 * @apiSuccess {Object} listComments contain array of comment object.
 * @apiSuccess {Object} pagination panigate object.
 */


  /**
 * @api {get} /comments/send Get sender comment
 * @apiName getSendComments
 * @apiGroup Comment
 * @apiDescription Get sender comment.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 * 
 * @apiParam {Number} seller_id sender id, Required.
 * @apiParam {Number} page=1 Page number, start from 1, optional.
 * @apiParam {Number} limit=10 Number of items per page, default 10, optional.
 * 
 * @apiSuccess {Object} listComments contain array of comment object.
 * @apiSuccess {Object} pagination panigate object.
 */



