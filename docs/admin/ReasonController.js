/**
 * @api {get} /reason Get list reason
 * @apiName getReason
 * @apiGroup Accuses
 * @apiDescription Get reason.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 * 
 * @apiSuccess {Array} data Array of reason objects.
 */