  /**
 * @api {post} /report/sellerExisted Report for seller existed
 * @apiName reportForSellerExisted
 * @apiGroup Report
 * @apiDescription Create report for seller existed.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiParam {Number} seller_id  Seller is reported ,required.
 * @apiParam {Number} reason_id  Reason id ,required.
 * @apiParam {String} description Description for reason ,Optional.
 * @apiParam {File} report_images  Image report , required
 * 
 * 
 * @apiSuccess {Object}  Objects report create success.
 */


  /**
 * @api {post} /report/sellerNotExist Report for seller not existed
 * @apiName reportForSellerNotExist
 * @apiGroup Report
 * @apiDescription Create report for seller not existed.
 * @apiPermission user
 *
 * @apiHeader {String} authorization Authorization token  
 *
 * @apiParam {String} fullname Fullname  of seller,required.
 * @apiParam {Number} sex=1 Sex of user ,required.
 * @apiParam {Number} province_id  Province of user ,required.
 * @apiParam {String} phone_number Phone of user ,required.
 * @apiParam {String} website      Website of user ,required.
 * @apiParam {String} bithday      Bithday of user,ex: 2020-01-03 ,optional.
 * @apiParam {Number} bank_id      Bank Name  ,required.
 * @apiParam {String} account_name Banking account name  of user ,optional.
 * @apiParam {String} account_number  Banking account number of ,required.
 * @apiParam {String} branch_name  Branch of banking user create, optional
 * @apiParam {Number} reason_id  Reason id ,required.
 * @apiParam {String} description Description for reason ,Optional.
 * @apiParam {File} report_images  Image report , required
 * 
 * @apiSuccess {Object}  Objects report create success.
 */


  /**
 * @api {post} /report/feedback Create feedback
 * @apiName createFeedback
 * @apiGroup Report
 * @apiDescription Create feedback for report.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiParam {Number} report_id  Report id,required.
 * @apiParam {String} feedback_content Content for feedback ,required.
 * 
 * 
 * @apiSuccess {Object}  Objects feedback create success.
 */


   /**
 * @api {get} /report Get list report via affect seller id
 * @apiName listReport
 * @apiGroup Report
 * @apiDescription Get list report via affect seller id.
 * @apiPermission user
 * 
 *
 * 
 * @apiParam {Number} page=1 Page number, start from 1, optional.
 * @apiParam {Number} limit=10 Number of items per page, default 10, optional.
 * @apiParam {String} status  Status report value in Reject,Accept,optional.
 * @apiParam {Number} seller_id  seller id,required.
 * 
 * @apiSuccess {Object}  Array off objects list seller report.
 */


    /**
 * @api {get} /report/listReported Get list report via reporter id
 * @apiName listReportViaReporter
 * @apiGroup Report
 * @apiDescription Get list report via reporter id.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 *
 * 
 * @apiParam {Number} page=1 Page number, start from 1, optional.
 * @apiParam {Number} limit=10 Number of items per page, default 10, optional.
 * 
 * @apiSuccess {Object}  Array off objects list seller report.
 */



    /**
 * @api {get} /report/getDetail Get detail report via report id
 * @apiName getDetailReport
 * @apiGroup Report
 * @apiDescription Get detail report via report id.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 *
 * 
 * @apiParam {Number} report_id  Report id,required.
 * 
 * @apiSuccess {Object} reportData contain data of report.
 * @apiSuccess {Object} reporter_seller Reporter seller.
 * @apiSuccess {Object} affect_seller Affect seller.
 * @apiSuccess {Array}  reportImage List report image.
 */


     /**
 * @api {get} /report/getFeedBack Get list feedback report via report id
 * @apiName getFeedBack
 * @apiGroup Report
 * @apiDescription Get list feedback report via report id.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 *
 * 
 * @apiParam {Number} report_id  Report id,required.
 * 

 * @apiSuccess {Array}  List feedback report.
 */


 /**
 * @api {delete} /report Delete report
 * @apiName deleteReport
 * @apiGroup Report
 * @apiDescription Delete report.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiParam {Number} report_id  Report id,required.

 * 
 * 
 * @apiSuccess {Object}  Objects Report delete success.
 */










