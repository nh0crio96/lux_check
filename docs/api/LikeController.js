/**
 * @api {post} /like Like orther seller
 * @apiName createLike
 * @apiGroup Like
 * @apiDescription Like orther seller.
 * @apiPermission user
 *
 * @apiHeader {String} authorization Authorization token 
 * 
 * @apiParam {Number} affect_user_id Required, seller who is liked.
 * 
 * @apiSuccess {Object} Object success.
 */

  /**
 * @api {get} /like Get list like
 * @apiName getLikes
 * @apiGroup Like
 * @apiDescription Get list like.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiParam {Number} page=1 Page number, start from 1, optional.
 * @apiParam {Number} limit=10 Number of items per page, default 10, optional.
 * 
 * @apiSuccess {Array} data Array of like seller objects.
 */


   /**
 * @api {delete} /like Delete like
 * @apiName deleteLike
 * @apiGroup Like
 * @apiDescription Delete like.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiParam {Number} like_id Required, Like id.
 * 
 * @apiSuccess {Object} data Like update.
 */



