/**
 * @api {post} /comment Create comment
 * @apiName createComment
 * @apiGroup Comment
 * @apiDescription Sent comment to orther seller.
 * @apiPermission: user
 *
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiParam {Number} receiver_id Receiver id, required.
 * @apiParam {String} content content of comment , required.
 *
 * @apiSuccess {Object} data of comment objects.
 */


 /**
 * @api {put} /comment Update comment
 * @apiName updateComment
 * @apiGroup Comment
 * @apiDescription Update comment.
 * @apiPermission: user
 * 
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiParam {Number} id Id of comment, required.
 * @apiParam {Number} receiver_id Receiver id, required.
 * @apiParam {String} content content of comment , required.
 *
 * @apiSuccess {Object} data of comment objects.
 */


 /**
 * @api {delete} /comment/:id Delete comment
 * @apiName deleteComment
 * @apiGroup Comment
 * @apiDescription Delete comment.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 * 
 * @apiParam {Number} id Required, ID of comment who you want to delete.
 * 
 * @apiSuccess {Object} success data objects.
 */

 /**
 * @api {get} /comment Get list receive comment.(v1)
 * @apiName getComments
 * @apiGroup Comment
 * @apiDescription Get list receive comment.(v1)
 * @apiPermission user
 * 
 * @apiParam {Number} seller_id Receiver id, Required.
 * @apiParam {Number} page=1 Page number, start from 1, optional.
 * @apiParam {Number} limit=10 Number of items per page, default 10, optional.
 * 
 * @apiSuccess {Array} data Array of comment objects.
 */



  /**
 * @api {get} /comment/send Get sender comment.(v1)
 * @apiName getSendComments
 * @apiGroup Comment
 * @apiDescription Get sender comment.(v1)
 * @apiPermission user
 * 
 * 
 * @apiParam {Number} seller_id sender id, Required.
 * @apiParam {Number} page=1 Page number, start from 1, optional.
 * @apiParam {Number} limit=10 Number of items per page, default 10, optional.
 * 
 * @apiSuccess {Array} data Array of comment objects.
 */



  /**
 * @api {get} /v2/comment Get list receive comment (v2)
 * @apiName getReceiverCommentsV2
 * @apiGroup Comment
 * @apiDescription Get list receive comment(v2).
 * @apiPermission user
 * 
 * @apiParam {Number} seller_id Receiver id, Required.
 * @apiParam {Number} page=1 Page number, start from 1, optional.
 * @apiParam {Number} limit=10 Number of items per page, default 10, optional.
 * @apiParam {String} sort value in DESC:ascending,ASC:decrease, optional.
 * 
 * @apiSuccess {Object} list_comment contain array of data comment.
 * @apiSuccess {Number} total_comment Total comment.
 */



  /**
 * @api {get} /v2/comment/send Get sender comment(v2)
 * @apiName getSendCommentsV2
 * @apiGroup Comment
 * @apiDescription Get sender comment(v2).
 * @apiPermission user
 * 
 * 
 * @apiParam {Number} seller_id sender id, Required.
 * @apiParam {Number} page=1 Page number, start from 1, optional.
 * @apiParam {Number} limit=10 Number of items per page, default 10, optional.
 * @apiParam {String} sort value in DESC:ascending,ASC:decrease, optional.
 * 
 * @apiSuccess {Object} list_comment contain array of data comment.
 * @apiSuccess {Number} total_comment Total comment.
 */





