/**
 * @api {post} /device Register device
 * @apiName RegisterDevice
 * @apiGroup Device
 * @apiDescription Register or update device information.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiParam {String} device_id Required, unique device ID.
 * @apiParam {Number} device_os Required, 1 for orroid and 2 for iOS.
 * @apiParam {String} token Device token.
 * 
 * @apiSuccess {Object} Object success.
 */