/**
 * @api {get} /systemNotification Get list notification
 * @apiName getNotification
 * @apiGroup SystemNotifcation
 * @apiDescription Get notification.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
\
 * @apiParam {Number} page=1 Page number, start from 1, optional.
 * @apiParam {Number} limit=10 Number of items per page, default 10, optional.
 * 
 * @apiSuccess {Array} data Array of notification from system .
 */


   /**
 * @api {post} /notification Delete notification
 * @apiName offNotification
 * @apiGroup SystemNotifcation
 * @apiDescription Delete notification.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiParam {Number} notification_id  Notification id ,required.
 * @apiParam {String} notifcation_type Type notification value in ADMIN,SYSTEM, required.

 * 
 * 
 * @apiSuccess {Object}  Objects off notiifcation create success.
 */
