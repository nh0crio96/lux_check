/**
 * @api {get} /user Get list user
 * @apiName Get user
 * @apiGroup User
 * @apiDescription Get user's.
 * @apiPermission user
 * 
 * 
 * @apiParam {String} txtSearch Optional, Search text via fullname.
 * @apiParam {Number} province_id Province id of user, optional.
 * @apiParam {Number} sex Sex of user, optional.
 * @apiParam {Number} is_report value in 0,1, optional.
 * @apiParam {String} sort value in DESC:ascending,ASC:decrease, optional.
 * @apiParam {String} status value in All:all,Verify_level_two:advance,Verify_level_one: basic, optional.
 * @apiParam {Number} current_user_id current user id case login, optional.
 * @apiParam {Number} page Page number, start from 1, optional.
 * @apiParam {Number} limit Number of items per page, default 10, optional.
 * 
 * @apiSuccess {Array} data Array of user objects.
 */


  /**
 * @api {get} /user/:id Get detail seller
 * @apiName getProfile
 * @apiGroup User
 * @apiDescription Get detail user.
 * @apiPermission user
 * 
 *
 * @apiHeader {String} authorization Authorization token  

 * @apiParam {Number} id Id  of seller,required.
 * @apiParam {Number} current_user_id current user id case login, optional.
 * 
 * @apiSuccess {Object}  Objects of user.
 */


 /**
 * @api {put} /user/updateSetting Update setting
 * @apiName updateSetting
 * @apiGroup User
 * @apiDescription Update setting(like,comment,common)
 * @apiPermission user
 *
 * @apiHeader {String} authorization Authorization token  
 *
 * @apiParam {Number} like_notification=1 allow or not allow like notification,value is 0 or 1,required.
 * @apiParam {Number} cmt_notification=1 allow or not allow comment notification,value is 0 or 1,required..
 * @apiParam {Number} common_notification=1 allow or not allow common notification,value is 0 or 1,required..
 * 
 * @apiSuccess {Object}  Objects success.
 */


  /**
 * @api {put} /user/verifyUser Verify  user
 * @apiName createUser
 * @apiGroup User
 * @apiDescription Verify user.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiParam {String} fullname Fullname  of seller,required.
 * @apiParam {Number} sex=1 Sex of user ,required.
 * @apiParam {Number} province_id  Province of user ,required.
 * @apiParam {String} phone_number Phone of user ,required.
 * @apiParam {String} website      Website of user ,required.
 * @apiParam {String} bithday      Bithday of user,ex: 1010-01-03 ,Optional.
 * @apiParam {Number} bank_id      Bank Name  ,required.
 * @apiParam {String} account_name Banking account name  of user ,required.
 * @apiParam {String} account_number  Banking account number of ,required.
 * @apiParam {String} branch_name  Branch of banking user create, optional
 * @apiParam {String} trading_address  Trading address, optional,
 * @apiParam {String} describe_yourself  Describe yourself, optional,
 * @apiParam {File} before_img_identity_card Before image identity card  of user ,required.
 * @apiParam {File} after_img_identity_card  After image identity card  of user ,required.
 * @apiParam {File} selfie_img  Selfie image  of user create, required
 * 
 * 
 * @apiSuccess {Object}  Objects user creaate success.
 */


   /**
 * @api {put} /user/v2/verifyUser Verify  user
 * @apiName verifyUserV2
 * @apiGroup User
 * @apiDescription Verify user.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiParam {String} fullname Fullname  of seller,required.
 * @apiParam {String} phone_number Phone of user ,required.
 * @apiParam {File} before_img_identity_card Before image identity card  of user ,required.
 * @apiParam {File} after_img_identity_card  After image identity card  of user ,required.
 * @apiParam {File} selfie_img  Selfie image  of user create, required
 * 
 * 
 * @apiSuccess {Object}  Objects user creaate success.
 */




 /**
 * @api {put} /user/updateProfile Update user
 * @apiName updateProfile
 * @apiGroup User
 * @apiDescription Update  user.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiParam {String} fullname Fullname  of seller,required.
 * @apiParam {Number} sex=1 Sex of user ,required.
 * @apiParam {Number} province_id  Province of user ,required.
 * @apiParam {String} phone_number Phone of user ,required.
 * @apiParam {String} website      Website of user ,required.
 * @apiParam {String} trading_address  Trading address, optional,
 * @apiParam {String} address  Detail address, optional,
 * @apiParam {String} describe_yourself  Describe yourself, optional,
 * @apiParam {String} birthday      Bithday of user,ex: 1010-01-03 ,required.
 * @apiParam {String} email        Email of user ,optional.
 * @apiParam {File} avatar_images  Avatar image  of user update, optional
 * 
 * 
 * @apiSuccess {Object}  Objects user update success.
 */


  /**
 * @api {put} /user/avatar Update avatar  user
 * @apiName updateAvatar
 * @apiGroup User
 * @apiDescription Update avatar for seller.
 * @apiPermission user
 *
 * @apiHeader {String} authorization Authorization token 
 * 
 * @apiParam {File} avatar_images  Avatar image  of user update, required
 * 
 * 
 * @apiSuccess {Object}  Objects user update success.
 */


/**
 * @api {post} /user/login Login
 * @apiName UserLogin
 * @apiGroup User
 * @apiDescription Login for user.
 * @apiPermission guest
 * 
 * @apiParam {String} phone_number  User's phone number.
 * @apiParam {String} password User's password (case phone_number: password is required).
 * @apiParam {String} firebase_token Firebase token(case phone_number: fullname is required).
 * @apiParam {String} fullname Fullname.
 *
 * @apiSuccess {Object}  Objects user and token.
 */


  /**
 * @api {POST} /user/changePasswd Change password
 * @apiName changePasswd
 * @apiGroup User
 * @apiDescription Change password
 * @apiPermission: user
 * 
 * @apiHeader {String} authorization Authorization token
 *
 * @apiParam {String} oldPass Old password,Required.
 * @apiParam {String} newPass New password ,Required
 * @apiParam {String} verify_new_pass New password fill again,Required
 * 
 * @apiSuccess {Object} data user  successfully.
 */



  /**
 * @api {POST} /user/v2/changePasswd Change password V2
 * @apiName changePasswdV2
 * @apiGroup User
 * @apiDescription Change password V2
 * @apiPermission: user
 * 
 * @apiHeader {String} authorization Authorization token
 *
 * @apiParam {String} phone_number Phone number,Optional.
 * @apiParam {String} oldPass Old password,Optional.
 * @apiParam {String} newPass New password ,Required
 * @apiParam {String} verify_new_pass New password fill again,Required
 * 
 * @apiSuccess {Object} data user  successfully.
 */



   /**
 * @api {POST} /user/forgotPasswd Forgot password
 * @apiName forgotPasswd
 * @apiGroup User
 * @apiDescription Forgot password
 * @apiPermission: user
 * 
 *
 * @apiParam {String} password User's password,Required.
 * @apiParam {String} firebase_token Firebase token,Required.
 * 
 * @apiSuccess {Object} data user update password successfully.
 */


   /**
 * @api {get} /user/loginWithFacebook Login facebook
 * @apiName loginWithFacebook
 * @apiGroup User
 * @apiDescription Login with facebook account.
 * @apiPermission user
 * 
 * @apiParam {String} access_token Token of facebook user,required.
 * 
 * @apiSuccess {Object}  Objects of user created or token.
 */



   /**
 * @api {POST} /user/updatePhoneAndPass Update password for facebook Acc
 * @apiName updatePhoneAndPass
 * @apiGroup User
 * @apiDescription  Update phone and password for facebook Acc
 * @apiPermission: user
 * 
 *
 * @apiParam {Number} user_id Id  of seller,required.
 * @apiParam {String} phone_number  User's phone number.,Required.
 * @apiParam {String} newPass New password ,Required
 * @apiParam {String} verify_new_pass New password fill again,Required
 * 
 * @apiSuccess {Object} data user  successfully.
 */


    /**
 * @api {POST} /user/mergeSeller Merge seller
 * @apiName mergeSeller
 * @apiGroup User
 * @apiDescription  Merge seller existed or not merge
 * @apiPermission: user
 * 
 *
 * @apiParam {Number} new_user_id Id of seller has just created ,required.
 * @apiParam {Number} existed_user_id Id  of seller existed ,required.
 * @apiParam {Number} is_agree  0:not merge,1:argee merge,Required.
 * 
 * @apiSuccess {Object} data user and token.
 */



/**
 * @api {post} /user/logout Logout
 * @apiName logout
 * @apiGroup User
 * @apiDescription Logout for user.
 * @apiPermission guest
 * 
 * @apiHeader {String} authorization Authorization token
 * @apiParam {String} device_id Required, unique device ID.
 * @apiSuccess {Object}  Objects success.
 */








