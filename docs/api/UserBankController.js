 /**
 * @api {get} /bank Get list bank
 * @apiName Get banks
 * @apiGroup Bank
 * @apiDescription Get banks.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiSuccess {Array} data Array of bank objects.
 */


  /**
 * @api {post} /sellerBank Create seller bank
 * @apiName createUserBank
 * @apiGroup Bank
 * @apiDescription Create seller bank.
 * @apiPermission user
 *
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiParam {Number} bank_id Bank id, required.
 * @apiParam {String} account_name Account Name, required.
 * @apiParam {String} account_number Account number, required.
 * @apiParam {String} branch_name Branch name, optional.
 * 
 * @apiSuccess {Array} data Array of bank objects.
 */



 /**
 * @api {put} /sellerBank Update seller bank
 * @apiName updateUserBank
 * @apiGroup Bank
 * @apiDescription Update seller bank.
 * @apiPermission user
 *
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiParam {Number} id Seller bank id, required.
 * @apiParam {Number} bank_id Bank id, required.
 * @apiParam {String} account_name Account Name, required.
 * @apiParam {String} account_number Account number, required.
 * @apiParam {String} branch_name Branch name, optional.
 * 
 * @apiSuccess {Array} data Array of bank objects.
 */


  /**
 * @api {get} /sellerBank/:seller_id Get detail seller bank
 * @apiName getBankBySellerId
 * @apiGroup Bank
 * @apiDescription Get detail seller bank.
 * @apiPermission user 
 * 
 
 * @apiParam {Number} seller_id Id  of seller,required.
 *
 * @apiSuccess {Array} data Array of seller bank.
 */





