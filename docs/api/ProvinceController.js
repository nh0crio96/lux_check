/**
 * @api {get} /province Get list province
 * @apiName getProvince
 * @apiGroup Province
 * @apiDescription Get province.
 * @apiPermission user
 *
 * 
 * @apiSuccess {Array} data Array of province objects.
 */



 /**
 * @api {get} /provinceSeller Get list province seller
 * @apiName getProvinceSeller
 * @apiGroup Province
 * @apiDescription Get province seller
 * @apiPermission user
 *
 * 
 * @apiSuccess {Array} data Array of province objects.
 */



 /**
 * @api {get} /provinceWarningSeller Get list province waring seller
 * @apiName getProvinceWarningSeller
 * @apiGroup Province
 * @apiDescription Get list province waring seller
 * @apiPermission user
 *
 * 
 * @apiSuccess {Array} data Array of province objects.
 */