/**
 * @api {get} /notification Get list notification
 * @apiName getNotifications
 * @apiGroup Notifcation
 * @apiDescription Get notification.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 *
 * @apiParam {Number} page Page number, start from 1, optional.
 * @apiParam {Number} limit Number of items per page, default 10, optional.
 * 
 * @apiSuccess {Array} data Array of notification from system .
 */


 /**
 * @api {get} /countNotification Get total notification
 * @apiName countNotifications
 * @apiGroup Notifcation
 * @apiDescription Get total notification.
 * @apiPermission user
 * 
 * @apiHeader {String} authorization Authorization token 
 *

 * 
 * @apiSuccess {Object} totalNotification Total notification .
 */