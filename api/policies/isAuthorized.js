module.exports = async function (req, res, next) {
  let token;
  let error = {};

  // Check if authorization header is present
  if (req.headers && req.headers.authorization) {
    // authorization header is present
    let parts = req.headers.authorization.split(' ');
    if (parts.length === 2) {
      let scheme = parts[0];
      let credentials = parts[1];

      if (/^Bearer$/i.test(scheme)) {
        token = credentials;
      }
    } else {
      sails.log.error('Format is Authorization: Bearer [token]');
      error.error_code = 'TokenFormat';
      error.message = 'Format is Authorization: Bearer [token]';
      return res.status(401).json(error);
    }
  } else {

    // authorization header is not present
    sails.log.error('No Authorization header was found');
    error.error_code = 'AuthorizationMissing';
    error.message = 'No Authorization header was found';
    return res.status(401).json(error);
  }

  jwToken.verify(token,  (err, decoded) =>{
    if (err) {
      sails.log.error(JSON.stringify(err));

      let errorData = {};
      errorData.error_code = err.name;
      errorData.message = err.message;

      return res.status(401).json(errorData);
    }
    req.current_user = decoded.data;
    sails.log.info('current_user' + JSON.stringify(req.current_user));
    next();
  });
};
