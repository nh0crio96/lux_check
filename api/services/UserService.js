module.exports = {

  getListUser :(txtSearch,current_user_id,status,sex,is_report,province_id,arr_order,sort,page_start,limit) => {
       
      let query ='';

      if (txtSearch) {
       query = `Select users.id,fullname,province_id,website,reported_number,avatar,sex,phone_number,like_notification,
       cmt_notification,common_notification, users.created_at, users.updated_at,before_img_identity_card,after_img_identity_card,selfie_img,
       birthday,total_like,email,social_id,verify_code,kyc_level,address,note,describe_yourself,trading_address,avatar_size 
       from users
       JOIN users_bank ON users_bank.user_id = users.id
       JOIN banks ON banks.id = users_bank.bank_id
       where users.is_delete = 0 AND (users.fullname  LIKE '%${txtSearch}%' OR users.phone_number  LIKE '%${txtSearch}%' OR users.address  LIKE '%${txtSearch}%' OR users.verify_code  LIKE '%${txtSearch}%'  OR users.website  LIKE '%${txtSearch}%'  OR banks.bank_name  LIKE '%${txtSearch}%' )`;
       
      }else{
       query = `Select users.id,fullname,province_id,website,reported_number,avatar,sex,phone_number,like_notification,
       cmt_notification,common_notification, users.created_at, users.updated_at,before_img_identity_card,after_img_identity_card,selfie_img,
       birthday,total_like,email,social_id,verify_code,kyc_level,address,note,describe_yourself,trading_address,avatar_size 
       from users
       where users.is_delete = 0`;
      }

    
       if (current_user_id) {
          query += ` AND users.id != ${current_user_id} `;
       }
      

       if (status == 'All'){
          query += ` AND users.status IN ('Verify_level_two','Verify_level_one')`;
       }else{
           query += ` AND users.status = '${status}'`;
       }



       if(is_report == 1){
          query += ` AND users.reported_number > 0  `;
       }else{
           query += ` AND users.reported_number = 0  `;
       }

       if(province_id){
          query += ` AND users.province_id = ${province_id}  `;
       }

       if (arr_order.length > 0) {
        let order_by = `ORDER BY `;
        let or =[];
        for (let i = 0 ; i < arr_order.length; i++) {
          or.push(`users.${arr_order[i]} ${sort}`)
        }
       
        order_by +=   or.join();
        if(is_report == 1){
          
           query +=`${order_by},users.reported_number ${sort},users.id ${sort} `
         }else{
        
           query +=`${order_by},users.total_like ${sort},users.id ${sort}`
         }
        

             

       }


        let limitstr = ` LIMIT ${page_start},${limit} `;

        query += limitstr;

      sails.log.debug(`getListUser ${query}`);
      
        return new Promise((resolve, reject) => {
         User.getDatastore().sendNativeQuery(query, (err, result)=> { 
              if(err){
            
               reject(err)  
              }
             resolve(result.rows)
        });
    })
  },


  getKycUser :(txtSearch,page_start,limit) => {
       
       let query = `Select id,fullname,status,province_id,website,reported_number,avatar,sex,phone_number,like_notification,
       cmt_notification,common_notification,created_at,updated_at,before_img_identity_card,after_img_identity_card,selfie_img,
       birthday,total_like,email,social_id,verify_code,kyc_level,address,note,describe_yourself,trading_address 
       from users where is_delete = 0`;
       
       if(txtSearch){
          query += ` AND (fullname  LIKE '%${txtSearch}%' OR phone_number  LIKE '%${txtSearch}%' OR verify_code  LIKE '%${txtSearch}%') `;
       }

       query += ` AND status IN ('Verify_level_two','Verify_level_one','Pending')`;

       query += ` ORDER BY FIELD(status, 'Pending') DESC `
     
       query += ` LIMIT ${page_start},${limit} `;

     

        sails.log.debug(`getKycUser ${query}`);
      
        return new Promise((resolve, reject) => {
         User.getDatastore().sendNativeQuery(query, (err, result)=> { 
              if(err){
            
               reject(err)  
              }
             resolve(result.rows)
        });
    })
  },

    countKycUser :(txtSearch) => {
       
       let query = `Select COUNT(id) as total_user from users where is_delete = 0`;
       
       if(txtSearch){
          query += ` AND (fullname  LIKE '%${txtSearch}%' OR phone_number  LIKE '%${txtSearch}%' OR verify_code  LIKE '%${txtSearch}%') `;
       }

       query += ` AND status IN ('Verify_level_two','Verify_level_one','Pending')`;
     
    

      

        sails.log.debug(`countKycUser ${query}`);
      
        return new Promise((resolve, reject) => {
         User.getDatastore().sendNativeQuery(query, (err, result)=> { 
              if(err){
            
               reject(err)  
              }
             resolve(result.rows)
        });
    })
  },


  getUsers: (criteriaObj,limit,page,arr_sort) => {
   
    return new Promise((resolve, reject) => {
      criteriaObj.is_delete = 0;
     User.find({
      where: criteriaObj,
      skip: page,
      limit: limit,
      sort: arr_sort
     }).then(users => {
        resolve(users);
      })
      .catch(err => {
          reject(err);
      });

    });
  },

    bulkInsertsSeller: (data) => {
      return new Promise((resolve, reject) => {
        User.createEach(data)
          .then(dataInsert => {
            resolve(dataInsert);
          })
          .catch(err => {
            reject(err);
          });
      });
    },


 

  createUser: (data) => {
    return new Promise((resolve, reject) => {
      User.create(data).fetch()
        .then(user => {
          resolve(user);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  findOneByCriteria: (criteria, select = []) => {
    return new Promise((resolve, reject) => {
      User.findOne({
        where: criteria,
        select: select
      })
        .then(user => {
          resolve(user);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  findOneById: (id) => {
    return new Promise((resolve, reject) => {
      User.findOne({ id: id,is_delete : 0})
        .then(user => {
          resolve(user);
        })
        .catch(err => {
          reject(err);
        });
    });
  },


  updateByCriteria: (criteriaObj, dataUpdate) => {
    return new Promise((resolve, reject) => {
      User.update(criteriaObj).set(dataUpdate).fetch()
        .then(dataUpdate => {
          resolve(dataUpdate[0]);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

   deleteUser: function(id,current_account_id){
     sails.log.debug(`deleteUser ${id} === ${current_account_id}`);
      return new Promise((resolve, reject) => {
          User.update({id:id}).set({is_delete:1,deleted_by:current_account_id}).fetch()
            .then(dataUpdate => {
              resolve(dataUpdate[0]);
            })
            .catch(err => {
              reject(err);
            });
        });
  },

  updateUser:(user)=>{
     return new Promise((resolve, reject) => {
      User.update({id:user.id}).set(user).fetch()
        .then(dataUpdate => {
          resolve(dataUpdate[0]);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

   getProfile :  (seller_id) => {
     let query = `SELECT u.id,u.fullname,u.address,u.website,u.reported_number,u.avatar,u.sex,u.phone_number,u.social_id,u.like_notification,u.cmt_notification,u.common_notification,u.trading_address,u.describe_yourself,
          u.before_img_identity_card,u.after_img_identity_card,u.selfie_img,u.birthday,u.email,u.total_like,u.verify_code,p.province_name,u.kyc_level,u.status,u.created_at,u.updated_at,u.note,p.id as province_id,p.province_name FROM users u 
          LEFT JOIN provinces p ON u.province_id = p.id
           WHERE u.id = ${seller_id}`;
          
      return new Promise((resolve, reject) => {
       User.getDatastore().sendNativeQuery(query, (err, result)=> { 
            if(err){
             reject(err)  
            }
      
           resolve(result.rows)
      });
    })
  },


  countUserByCriterial: (criteria) => {
      criteria.is_delete = 0;
    return new Promise((resolve, reject) => {
      User.count(criteria)
        .then(total => {
          resolve(total);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  deleteById: function(id){
    return new Promise((resolve, reject) =>{
      User.destroy({id: id}).then(data =>{
        resolve(data);
      }).catch(err => {
          reject(err);
        });
    });
  },

  updateUserWithoutFetch:(user)=>{
     return new Promise((resolve, reject) => {
      User.update({id:user.id}).set(user)
        .then(dataUpdate => {
          resolve(true);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

}
