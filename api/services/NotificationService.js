module.exports = {

  createNotification: (data) => {
    return new Promise((resolve, reject) => {
      Notifications.create(data).fetch()
        .then(notification => {
          resolve(notification);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  getNotification :  (start,limit,current_user_id) => {
     let query = "SELECT n.id,n.meta_data,n.target_type,n.target_id,n.created_at,u.fullname,u.avatar,u.reported_number FROM  notifications n  " +
                "JOIN users AS u ON n.seller_id = u.id " +
                "JOIN users AS a ON n.affect_seller_id = a.id WHERE n.affect_seller_id ="+current_user_id+" AND n.is_read = 0 ORDER BY n.id DESC LIMIT " +start+","+limit;
      sails.log.info(`getNotification param ${query}`);
      return new Promise((resolve, reject) => {
       Notifications.getDatastore().sendNativeQuery(query, (err, result)=> { 
            if(err){
             reject(err)  
            }
           //console.log(result)
           resolve(result.rows)
      });
    })
  },

  updateIsRead:(listId)=>{
     return new Promise((resolve, reject) => {
      Notifications.update({where: {id: {'in': listId}}}).set({is_read:1})
        .then(dataUpdate => {
          resolve(true);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  getTotalNoti: (user_id) => {
    return new Promise((resolve, reject) => {
      Notifications.count({affect_seller_id: user_id,is_read:0}).then((total) => {
        resolve(total);
      }).catch((err) => {
        reject(err);
      });
    });
  }
  

}