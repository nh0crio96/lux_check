
module.exports = {

	findByCriteria: (crit) => {
		return new Promise((resolve, reject) => {
			Devices.find(crit).then((devices) => {
				resolve(devices);
			}).catch((err) => {
				reject(err);
			});
		});
	},
	findOneByCriteria: (crit) => {
		return new Promise((resolve, reject) => {
			Devices.findOne(crit).then((device) => {
				resolve(device);
			}).catch((err) => {
				reject(err);
			});
		});
	},
	// Create new device
	createDevice: (data) => {
		return new Promise((resolve, reject) => {
			Devices.create(data).then((res) => {
				resolve(true);
			}).catch((err) => {
				reject(err);
			});
		});
	},
	// Update by device_id
	updateById: (id, data) => {
		return new Promise((resolve, reject) => {
			Devices.update({id: id}).set(data).then((res) => {
				resolve(true);
			}).catch((err) => {
				reject(err);
			})
		});
	},

findListDevice: (criteria, select = []) => {
    return new Promise((resolve, reject) => {
      Devices.find({
        where: criteria,
        select: select
      })
        .then(post => {
          resolve(post);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  // get list seller allow get common notification 
  getListSellerCommon :  (seller_id) => {
     let query = `SELECT user_id, token FROM devices JOIN users ON users.id = devices.user_id WHERE users.common_notification = 1`;
          
      return new Promise((resolve, reject) => {
       Devices.getDatastore().sendNativeQuery(query, (err, result)=> { 
            if(err){
             reject(err)  
            }
      
           resolve(result.rows)
      });
    })
  },

  deleteDevice: function(criteria){
    return new Promise((resolve, reject) =>{
      Devices.destroy(criteria).then(data =>{
        resolve(data);
      }).catch(err => {
          reject(err);
        });
    });
  },


};