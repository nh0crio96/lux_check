module.exports = {

  createOffNotification: (data) => {
    return new Promise((resolve, reject) => {
      OffNotification.create(data).fetch()
        .then(notification => {
          resolve(notification);
        })
        .catch(err => {
          reject(err);
        });
    });
  },
}