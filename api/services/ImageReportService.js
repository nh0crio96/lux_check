
module.exports = {
    bulkInsertsImg: (data) => {
      return new Promise((resolve, reject) => {
        ImageReport.createEach(data).fetch()
          .then(dataInsert => {
            resolve(dataInsert);
          })
          .catch(err => {
            reject(err);
          });
      });
    },

 saveFile: (data) => {
    return new Promise((resolve, reject) => {
      Files.create(data)
        .then(file => {
          resolve(file);
        })
        .catch(err => {
          console.log(err)
          reject(err);
        });
    });
  },


  countByArrSourceId: (arrSourceId,source_type) => {
    return new Promise((resolve, reject) => {
      Files.count({where: {source_id: {'in': arrSourceId},source_type:source_type}})
        .then(total => {
          resolve(total);
        })
        .catch(err => {
          reject(err);
        });
    });
  },
  

}
