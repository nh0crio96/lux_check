const fs = require('fs-extra')
module.exports={
  removeFolder :async (dir)=> {
    try {
      if(!dir.trim()){
        return 
      }
      let pathDelete = '';
      if(dir.includes('\\')) { // user for local
          let arrayStr = dir.split("\\");
          arrayStr.pop();
         pathDelete= arrayStr.join("\\");
        
      }

      if(dir.includes('/')) { // user for serve linux,centos...
          let arrayStr = dir.split("/");
           arrayStr.pop();
          pathDelete= arrayStr.join("/");
       
      }
        if(!pathDelete.trim()){
        return 
      }
      sails.log.info(`pathDelete ${pathDelete}`);


      await fs.remove(pathDelete )
     // console.log('success!')
    } catch (err) {
      sails.log.error("Delete path server" + err);
      console.error(err)
    }
  },

}