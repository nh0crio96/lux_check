module.exports = {

  createRefreshToken: (data) => {
    return new Promise((resolve, reject) => {
      RefreshToken.create(data).fetch()
        .then(refreshToken => {
          resolve(refreshToken);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  findOneByUserId: (user_id) => {
    return new Promise((resolve, reject) => {
      RefreshToken.findOne({ user_id: user_id})
        .then(refreshToken => {
          resolve(refreshToken);
        })
        .catch(err => {
          reject(err);
        });
    });
  },



    updateRefreshToken:(refreshToken)=>{
     return new Promise((resolve, reject) => {
      RefreshToken.update({user_id:refreshToken.user_id}).set(refreshToken).fetch()
        .then(dataUpdate => {
          resolve(dataUpdate[0]);
        })
        .catch(err => {
          reject(err);
        });
    });
  },


}
