module.exports = {

  createAccount: (data) => {
    return new Promise((resolve, reject) => {
      Accounts.create(data).fetch()
        .then(user => {
          resolve(user);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  findOneByCriteria: (criteria, select = []) => {
    return new Promise((resolve, reject) => {
      Accounts.findOne({
        where: criteria,
        select: select
      })
        .then(user => {
          resolve(user);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  findOneById: (id) => {
    return new Promise((resolve, reject) => {
      Accounts.findOne({ id: id})
        .then(user => {
          resolve(user);
        })
        .catch(err => {
          reject(err);
        });
    });
  },


  updateByCriteria: (criteriaObj, dataUpdate) => {
    return new Promise((resolve, reject) => {
      Accounts.update(criteriaObj).set(dataUpdate).fetch()
        .then(dataUpdate => {
          resolve(dataUpdate[0]);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

   deleteAccount: function(id){
    return new Promise((resolve, reject) =>{
      Accounts.destroy({id: id}).then(data =>{
        resolve(data);
      }).catch(err => {
          reject(err);
        });
    });
  },

    updateAccount:(user)=>{
     return new Promise((resolve, reject) => {
      Accounts.update({id:user.id}).set(user).fetch()
        .then(dataUpdate => {
          resolve(dataUpdate[0]);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

   getProfile :  (seller_id) => {
     let query = `SELECT u.id,u.fullname,u.website,u.reported_number,u.avatar,u.sex,u.phone_number,u.like_notification,u.cmt_notification,u.common_notification,
          u.before_img_identity_card,u.after_img_identity_card,u.selfie_img,u.birthday,u.email,u.total_like,p.province_name,p.id as province_id FROM users u 
          LEFT JOIN provinces p ON u.province_id = p.id
           WHERE u.id = ${seller_id}`;
          
      return new Promise((resolve, reject) => {
       User.getDatastore().sendNativeQuery(query, (err, result)=> { 
            if(err){
             reject(err)  
            }
      
           resolve(result.rows)
      });
    })
  },


}
