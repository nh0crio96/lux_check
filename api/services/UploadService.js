

const fs = require('fs')
const Jimp = require('jimp');
const { promisify } = require('util')
// const sharp = require('sharp');
module.exports = {

  uploadFiles: function(req){
    return new Promise((resolve, reject) =>{
      req.file("fileImport").upload(
        {
          maxBytes: 100000000,
          dirname: "../../" + sails.config.system.path_images
        },
        (err, files) =>{
          if(err) reject(err);
          resolve(files[0].fd);
        }
      );
    });
  },

    uploadImgToServer: function(file, id){
      let fileSizeAllow = sails.config.system.maxFileSizeUpload;
      let imgInfo = [];
      return new Promise((resolve, reject) =>{
       file.upload(
          {
            maxBytes: fileSizeAllow,
            dirname:  process.cwd() +  sails.config.system.path_images + id + "/"
          },
          (err, files) =>{
            if(err) reject(err);
            if(files && files[0] && files[0].fd  ){
             sails.log.debug("====uploadImgToServer===file_extension========" + files[0].fd.split('.').pop());
            }
            for(let i = 0; i < files.length; i++){
               let imgName = files[i].fd.split("/").splice(-1);
              imgInfo.push({imgName:imgName[0],imgPath:files[i].fd})
            }
          
            resolve(imgInfo);
          }
        );
      });
    },
  uploadImgVerifyToServer: function(file, id){

      let fileSizeAllow = sails.config.system.maxFileSizeUpload;
      let invalid_extension = sails.config.system.invalid_extension;
      let imgInfo = [];
      return new Promise((resolve, reject) =>{
       file.upload(
          {
            maxBytes: fileSizeAllow,
            dirname:  process.cwd() +  sails.config.system.path_images + id + "/"
          },
          (err, files) =>{
            if(err) reject(err);
            sails.log.debug("====uploadImgVerifyToServer===file_extension========" + files[0].fd.split('.').pop());
            // if(invalid_extension.includes(files[0].fd.split('.').pop())){
            //   resolve(files[0].fd.split('.').pop())
            // }
            for(let i = 0; i < files.length; i++){
               let imgName = files[i].fd.split("/").splice(-1);
              imgInfo.push({imgName:imgName[0],imgPath:files[i].fd})
            }
            resolve(imgInfo);
          }
        );
      });
    },

    uploadCoverToServer: function(req, id){
      let fileSizeAllow = sails.config.system.maxFileSizeUpload;
      let imgInfo = [];
      return new Promise((resolve, reject) =>{
        req.file('cover_images').upload(
          {
            maxBytes: fileSizeAllow,
            dirname:  process.cwd() +  sails.config.system.path_cover + id + "/"
          },
          (err, files) =>{
            if(err) reject(err);
          
            for(let i = 0; i < files.length; i++){
               let imgName = files[i].fd.split("/").splice(-1);
              imgInfo.push({imgName:imgName[0],imgPath:files[i].fd})
            }
            resolve(imgInfo);
          }
        );
      });
    },

    saveFileVerify: async (obj) => {
      try{
        let path_local = obj.imgPath;
        let filename =  obj.imgName;
        let high_path = process.env.BUCKET_NAME_S3 +'/'+ obj.img_type+'/'+obj.id+'/high';
        const readFileAsync = promisify(fs.readFile)

        const data = await readFileAsync(path_local)
        await  AwsService.uploadBufferToS3(high_path, filename, data)
        return '/'+ obj.img_type+'/'+obj.id+'/high/'+filename;
      }catch(err){
        sails.log.error(`saveFileVerify get  ${err}`); 
      }

    },
  saveFile:  (obj) => {
      try
      {
        
          let path_local = obj.imgPath;
          let filename =  obj.imgName;
          let high_path = process.env.BUCKET_NAME_S3 +'/'+ obj.img_type+'/'+obj.id+'/high';
          let medium_path = process.env.BUCKET_NAME_S3 +'/'+ obj.img_type+'/'+obj.id+'/medium';
          let low_path = process.env.BUCKET_NAME_S3 +'/'+ obj.img_type+'/'+obj.id+'/low';

            let resize_number=  sails.config.system.resize_img;

          fs.readFile(path_local, async function(err, data) {
              if (err) 
                  ails.log.error(err)
              else
              {
                  await  AwsService.uploadBufferToS3(high_path, filename, data)

                  if (obj.img_type == sails.config.system.s3_img_type.avatar) {
                    obj.widthMedium = resize_number.avatar.width_medium;
                    obj.widthLow = resize_number.avatar.width_low;
                  }else{
                      obj.widthMedium =  resize_number.orther.width_medium;
                      obj.widthLow =     resize_number.orther.width_low;
                  }
                    
                  try
                  {

                  await  Jimp.read(path_local)
                      .then(lenna => {
                          lenna
                          .resize(obj.widthMedium,Jimp.AUTO) 
                          .quality(100)
                          .getBase64(Jimp.AUTO, (err, res) => {
                              AwsService.uploadImageToS3(medium_path, filename, res)
                          })

                      })
                      .catch(err => {
                          console.error(err)
                      })

               

                  await  Jimp.read(path_local)
                        .then(lenna => {
                        lenna
                        .resize(obj.widthLow,Jimp.AUTO) // resize
                        .quality(100)
                        .getBase64(Jimp.AUTO,(err, res) => {
                            AwsService.uploadImageToS3(low_path, filename, res)
                        })
                    })
                    .catch(err => {
                        console.error(err)
                    })
                  }
                  catch(e)
                  {
                      console.log(e)
                  }

                  await  FileService.removeFolder(path_local);
              }
          })
       
          return '/'+ obj.img_type+'/'+obj.id+'/high/'+filename;
      }
      catch(e)
      {
          console.log(e)
       
      }
  }
}
