const admin_firebase = require('firebase-admin');
const QRCode = require('qrcode')
let fs = require('fs');
module.exports = {

  getInfoByTokenId: (tokenId)=> {
  if (!admin_firebase.apps.length) {
     admin_firebase.initializeApp({
        credential: admin_firebase.credential.cert({
  "type": "service_account",
  "project_id": "luxuria-77786",
  "private_key_id": "6d602c421ad31dc60c178ca856d6d3b277c665b6",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCvE+S+t84BHmrw\nNGEjtlDDTHNWIRvYROUbGWNlyZd+dR7ylxwNxCjn9/kKSiBLj8VTmUv5EXhZxTOF\nCPQhkToZ/si5MHwZj1JsTObR9lN7t+LUDBX1hOlEKkekF2nigRmq1x2QnjCk968J\npuw9s6qZZcRNohFxVn9U7CDDdksJCyp+3dBfU1oklfZLBePwprQHelvaOxclxQUp\nnH9GHcQfRQ7LP+Rtjr8TvAGccDjX2bouP55wCob8uMLtA05ODeLAW/K+AAH4Ck/Z\n/ObWc0L2mTHQhZzS4AauK2glenNd/ZZJ2HlTX0V/eqFxlmx9S/R8TPYJy7ua9hI7\nnCB4dno9AgMBAAECggEAASuFB9ovM7/YY7Se+OY5cM/grhuBAH/1of8YZ5aRQ2BQ\nh0OhUmXUXElLDCIfamUORPceZVs2O1F7OMJGhpX31hDWL/ZejWiCSbv+OsMs4y6J\nlBa6DehZLYZ5EvUnocKj1rSJP8920n2J6lmyhbtwAlMoaXIi/PiccUutNQrKbz8x\nZB9mJssMPBG/uvSJK9fe/MMqiZIDz7K2d+a6Z0wL1O1wPpdXyjMjCy8uqId8KZ4v\nK7QUQUL/EnoiioiHXWp3m1+RgG1Oe+/vlHK2cctOadjkAZBGOQhbUMttYHvWLW+r\n0SsGlGqPDlsONfEz90P1xKrjYvzUNW6d3Swv4Gj/RwKBgQDbxRrURW5/D8K4udTc\neXVJhQ+cs9hJfp6Suyq2hn1bwAD8opZFpbf4UKUANRP+YZggpPxPx8IKabZ6sTJ9\nE1GyP19IPox2aPU1siTDGS55LwDVHCcp2iXdItO4sgXDT1VFUkFZwGHQjNU87Hc2\ncVqUcaONJAgL+RDiGRPZzBL2fwKBgQDL8KdfH5BvWCvfkvIFoHluMR42uKQf1Cng\nVRQQPXHN/xdGLuehHPzhlEEzzVVk73BPO0JHipQ9cTM3GzeTrETbMuPgpYSqQnC0\nsUTnS8imvFS9s/2fKD26l42sMNUKB9k/lYChXxTpiA/acDoiHQ3cS1rCnaogDumr\n8T89p2qJQwKBgQCzt562AcVXYLMSztyUIzrXkVlwAt4XU9UnFqnaM2bmFcB42Bhd\nAiTMil40KjOcbYCzCVhPgYJbc5FHCgrIn3jKc+peMNA6sJ/9dh178ebAMMDUhT49\nimg2SxmqSuJNkl2f36ETXTBgFBr4xELEobO5+qgNbm6qI9Y7nthN3gWPzwKBgFUy\n5Ylc02dgy3tslUV6hCBfzsKHorFVXmVExcv/D3NCtYNxhv0M8ix/VjoA90LisOW4\n2PiwUTvV9dJ4wWAlIOL+QKsSGvlSJmmmgSWryFi35yMVdcgu9JLmS557DBVTsBPb\nLlC8Oy1Q1Wapae/keWzl4jXZzj/0WvSlJYlpTp2nAoGAexxJBzjeZoSP2AvKmKzO\nm1lrGuzUIjV2brw0Ot6ut5tjsSw0Qf6SUc5VqMFAExbJupZdpXDcfwfWeVo/HBqA\nBWRVjPMXXgaUxUuF8wH0EUkBl8+1XmvAWMdHN8oHb8HOCRRSX2pwy9jROuzxzCsJ\n+1zudIRsn7iBp7r+DxjJ4E4=\n-----END PRIVATE KEY-----\n",
  "client_email": "firebase-adminsdk-4msv6@luxuria-77786.iam.gserviceaccount.com",
  "client_id": "117141617489585913642",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-4msv6%40luxuria-77786.iam.gserviceaccount.com"
}),
        databaseURL: "https://luxuria-77786.firebaseio.com"
    }); 
  }
  
    return new Promise((resolve, reject) => {
      admin_firebase.auth().verifyIdToken(tokenId)
        .then((decodedData)=> {
          sails.log.info(decodedData)
          resolve(decodedData)
        })
        .catch(function(err) {
          reject(err);
        });
    })
  },

  getQrCode: (textInput)=> {
    return new Promise((resolve, reject) => {
      QRCode.toDataURL(textInput)
        .then((url)=> {
          resolve(url)
        })
        .catch(function(err) {
          reject(err);
        });
    })
  },

 uploadImgToServer: (before_img_identity_card,after_img_identity_card,selfie_img,id)=> {
  return Promise.all([
    UploadService.uploadImgVerifyToServer(before_img_identity_card,id),
    UploadService.uploadImgVerifyToServer(after_img_identity_card,id),
    UploadService.uploadImgVerifyToServer(selfie_img,id)]).then(function(data) {
    return {
        'before_img_identity_card': data[0],
        'after_img_identity_card': data[1],
        'selfie_img': data[2] 
    }
  });
},

 deleteSellerAndSellerBank: (seller_id,seller_acc_bank)=> {
  return Promise.all([
    UserService.deleteUser(seller_id),
    BankService.deleteUserBank(seller_id,seller_acc_bank)]).then(function(data) {
    return {
        'deleteUser': data[0],
        'deleteUserBank': data[1]
    }
  });
},

updateTimeTwoSeller: (seller1,seller2)=> {
  return Promise.all([
   UserService.updateUserWithoutFetch(seller1),
   UserService.updateUserWithoutFetch(seller2)]).then(function(data) {
    return {
        'seller1': data[0],
        'seller2': data[1]
    }
  });
}
}
