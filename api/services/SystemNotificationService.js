module.exports = {

  getAll: (seller_id,page_start,limit) => {
       let query = `SELECT s.id,s.title,s.content,s.created_at FROM system_notification s 
          WHERE id NOT IN(SELECT notifcation_id FROM off_notification WHERE seller_id = ${seller_id} AND notifcation_type ="SYSTEM")  
          ORDER BY s.id DESC  LIMIT ${page_start},${limit} `;
          sails.log.info(`getAll query ${query}`);
        return new Promise((resolve, reject) => {
         SystemNotification.getDatastore().sendNativeQuery(query, (err, result)=> { 
              if(err){
            
               reject(err)  
              }
             resolve(result.rows)
        });
    })
  },

  createNotification: (data) => {
    return new Promise((resolve, reject) => {
      SystemNotification.create(data).fetch()
        .then(notification => {
          resolve(notification);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  findOneById: (id) => {
    return new Promise((resolve, reject) => {
      SystemNotification.findOne({ id: id})
        .then(bank => {
          resolve(bank);
        })
        .catch(err => {
          reject(err);
        });
    });
  },




  updateNotification:(notification)=>{
     return new Promise((resolve, reject) => {
      SystemNotification.update({id:notification.id}).set(notification).fetch()
        .then(dataUpdate => {
          resolve(dataUpdate[0]);
        })
        .catch(err => {
          reject(err);
        });
    });
  },


  getNotification: (offset, limit) => {
    return new Promise((resolve, reject) => {
      SystemNotification.find().skip(offset).limit(limit).sort('id DESC').then((listItem) => {
        resolve(listItem);
      }).catch((err) => {
        reject(err);
      });
    });
  },

  countNotification: () => {
    return new Promise((resolve, reject) => {
      SystemNotification.count().then(total => {
          resolve(total);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  deleteNotification: function(id){
    return new Promise((resolve, reject) =>{
      SystemNotification.destroy({id: id}).then(data =>{
        resolve(data);
      }).catch(err => {
          reject(err);
        });
    });
  },


  




}