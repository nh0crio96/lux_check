module.exports = {


  getComments :  (seller_id,seller_name,page_start,limit) => {

    let query =`SELECT c.*,us.fullname AS sender_name,us.address AS sender_address, 
    ur.fullname AS receiver_name,ur.address AS receiver_address FROM comments c 
              JOIN users us ON us.id = c.sender_id
              JOIN users ur ON ur.id = c.receiver_id where 1=1`

              if(seller_id){
               query += ` AND (c.sender_id  = ${seller_id} OR c.receiver_id  = ${seller_id}) `;
              }

              if(seller_name){
               query += ` AND (us.fullname  LIKE '%${seller_name}%' OR ur.fullname  LIKE '%${seller_name}%') `;
              }

              query += ` ORDER BY c.id DESC LIMIT ${page_start},${limit} `;
              
      sails.log.info(`getComments query  ${query}`);
      return new Promise((resolve, reject) => {
       Comments.getDatastore().sendNativeQuery(query, (err, result)=> { 
            if(err){
             reject(err)  
            }
           resolve(result.rows)
      });
    })
  },

    getTotalComment :(seller_id,seller_name) => {
       
        let query =`SELECT COUNT(c.id) as total_comment FROM comments c 
        JOIN users us ON us.id = c.sender_id
        JOIN users ur ON ur.id = c.receiver_id where 1=1`

        if(seller_id){
         query += ` AND (c.sender_id  = ${seller_id} OR c.receiver_id  = ${seller_id}) `;
        }

        if(seller_name){
         query += ` AND (us.fullname  LIKE '%${seller_name}%' OR ur.fullname  LIKE '%${seller_name}%') `;
        }



        sails.log.info(`getTotalComment query ${query}`);
      
        return new Promise((resolve, reject) => {
         Comments.getDatastore().sendNativeQuery(query, (err, result)=> { 
              if(err){
            
               reject(err)  
              }
             resolve(result.rows)
        });
    })
  },



 getReceiveComments :  (receiver_id,page_start,limit,sort) => {
     let query = "SELECT c.*,u.avatar,u.fullname,u.address FROM comments c  " +
                "JOIN users u ON u.id = c.sender_id " +
                "WHERE c.receiver_id = $1  ORDER BY c.id "+sort+" LIMIT "+page_start+","+limit;
      return new Promise((resolve, reject) => {
       Comments.getDatastore().sendNativeQuery(query,[receiver_id], (err, result)=> { 
            if(err){
             reject(err)  
            }
           //console.log(result)
           resolve(result.rows)
      });
    })
  },



   getSendComments :  (sender_id,page_start,limit,sort) => {
     let query = "SELECT c.*,u.avatar,u.fullname,u.address FROM comments c  " +
                "JOIN users u ON u.id = c.receiver_id " +
                "WHERE c.sender_id = $1 ORDER BY c.id "+sort+" LIMIT "+page_start+","+limit;
      return new Promise((resolve, reject) => {
       Comments.getDatastore().sendNativeQuery(query,[sender_id], (err, result)=> { 
            if(err){
             reject(err)  
            }
           //console.log(result)
           resolve(result.rows)
      });
    })
  },



 findOneByCriteria: (criteria, select = []) => {
    return new Promise((resolve, reject) => {
      Comments.findOne({
        where: criteria,
        select: select
      })
        .then(post => {
          resolve(post);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  saveComments: (data) => {
    return new Promise((resolve, reject) => {
      Comments.create(data).fetch()
        .then(comment => {
          resolve(comment);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  updateByCriteria: (criteriaObj, dataUpdate) => {
    return new Promise((resolve, reject) => {
      Comments.update(criteriaObj).set(dataUpdate).fetch()
        .then(dataUpdate => {
          resolve(dataUpdate[0]);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  deleteComment: function(id){
    return new Promise((resolve, reject) =>{
      Comments.destroy({id: id}).then(data =>{
        resolve(data);
      }).catch(err => {
          reject(err);
        });
    });
  },

  countCommentReceiver :  (arr_receiver_id) => {
       let string_arr = arr_receiver_id.join();
       let query = "SELECT COUNT(id) AS total_comment,receiver_id FROM comments WHERE receiver_id IN ("+string_arr+")  GROUP BY receiver_id" ;
        return new Promise((resolve, reject) => {
         Comments.getDatastore().sendNativeQuery(query, (err, result)=> { 
              if(err){
            
               reject(err)  
              }
             resolve(result.rows)
        });
    })
  },


  countCommentSender :  (arr_sender_id) => {
       let string_arr = arr_sender_id.join();
       let query = "SELECT COUNT(id) AS total_comment,sender_id FROM comments WHERE sender_id IN ("+string_arr+")  GROUP BY sender_id" ;
        return new Promise((resolve, reject) => {
         Comments.getDatastore().sendNativeQuery(query, (err, result)=> { 
              if(err){
            
               reject(err)  
              }
             resolve(result.rows)
        });
    })
  },

  countCommentByCriterial: (criteria) => {
    return new Promise((resolve, reject) => {
      Comments.count(criteria)
        .then(total => {
          resolve(total);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  countTotalComment: (criteria) => {
    return new Promise((resolve, reject) => {
      Comments.count().then(total => {
          resolve(total);
        })
        .catch(err => {
          reject(err);
        });
    });
  },



};
