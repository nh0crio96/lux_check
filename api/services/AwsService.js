
    const AWS = require('aws-sdk');
    require('dotenv').config()
    const ep = new AWS.Endpoint(process.env.S3_CLOUD);
    const s3 = new AWS.S3({
      accessKeyId: process.env.S3_PRIVATE_KEY,
      secretAccessKey: process.env.S3_SECRET_KEY,
      endpoint: ep
    });
 const uploadImageToS3 = (folder, filename, data) => {
    const buf = Buffer.from(
        data.replace(/^data:image\/\w+;base64,/, ''),
        'base64'
    )


    const params = {
        Bucket: folder,
        Key: filename,
        Body: buf,
        ACL: 'public-read'
    }

    return new Promise((resolve, reject) => {
        s3.putObject(params, (error, res) => {
            if (error) {
                sails.log.error(`Error uploading image ${filename} get  ${err}`); 
                reject(error)
            }
            sails.log.info(`Uploading file ${filename} successfully`);   
            resolve(res)
        })
    })
}

const uploadBufferToS3 = (folder, filename, data) => {

    const params = {
        Bucket: folder,
        Key: filename,
        Body: data,
        ACL: 'public-read'
    }

    return new Promise((resolve, reject) => {
        s3.putObject(params, (error, res) => {
            if (error) {
               sails.log.error(`Error uploading image ${filename} get  ${err}`); 
                reject(error)
            }
            sails.log.info(`Uploading file ${filename} successfully`);   
            resolve(res)
        })
    })
}


const emptyS3Directory =async (bucket, dir)=>{

    const listParams = {
        Bucket: bucket,
        Prefix: dir
    };

    const listedObjects = await s3.listObjectsV2(listParams).promise();

    if (listedObjects.Contents.length === 0) return;

    const deleteParams = {
        Bucket: bucket,
        Delete: { Objects: [] }
    };

    listedObjects.Contents.forEach(({ Key }) => {
        deleteParams.Delete.Objects.push({ Key });
    });

    await s3.deleteObjects(deleteParams).promise();

    if (listedObjects.IsTruncated) await emptyS3Directory(bucket, dir);
}
  

module.exports = {
emptyS3Directory,
uploadBufferToS3,
uploadImageToS3
}