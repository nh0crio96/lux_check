
module.exports = {

  getReportsByAffectId :  (affect_user_id,status,page_start,limit) => {
      let query =  `SELECT re.*,r.reason_name,u.avatar,u.fullname,u.phone_number FROM reports re 
                JOIN reason r ON re.reason_id = r.id  
                JOIN users u ON u.id = re.reporter_id 
                WHERE affect_user_id =  ${affect_user_id} `;

                if(status){
                  query = query + `and re.status = '${status}' `
                }

                let order = `ORDER BY re.id DESC LIMIT ${page_start},${limit}`;
                query = query + order;
                 sails.log.info(`getReportsByAffectId query ${query}`);
      return new Promise((resolve, reject) => {
       Reports.getDatastore().sendNativeQuery(query, (err, result)=> { 
            if(err){
             reject(err)  
            }
           resolve(result.rows)
      });
    })
  },

  getReportByReportertId :  (reporter_id,page_start,limit) => {
      let query =  "SELECT re.*,r.reason_name,u.avatar,u.fullname,u.phone_number FROM reports re " +
                "JOIN reason r ON re.reason_id = r.id " + 
                "JOIN users u ON u.id = re.affect_user_id " +
                "WHERE reporter_id =  $1 AND re.is_delete = 0  ORDER BY re.id DESC LIMIT " +page_start+","+limit;
                sails.log.info(`getReportByReportertId query ${query}`);
      return new Promise((resolve, reject) => {
       Reports.getDatastore().sendNativeQuery(query,[reporter_id], (err, result)=> { 
            if(err){
             reject(err)  
            }
           //console.log(result)
           resolve(result.rows)
      });
    })
  },

  saveReport: (data) => {
    return new Promise((resolve, reject) => {
      Reports.create(data).fetch()
        .then(report => {
          resolve(report);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

   findOneByCriteria: (criteria, select = []) => {
    return new Promise((resolve, reject) => {
      Reports.findOne({
        where: criteria,
        select: select
      })
        .then(report => {
          resolve(report);
        })
        .catch(err => {

          reject(err);
        });
    });
  },

  countTotalReport: () => {
    return new Promise((resolve, reject) => {
      Reports.count().then(total => {
          resolve(total);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  countReportCriterial: (criteria) => {
    return new Promise((resolve, reject) => {
      Reports.count(criteria)
        .then(total => {
          resolve(total);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  updateReport:(report)=>{
     return new Promise((resolve, reject) => {
      Reports.update({id:report.id}).set(report).fetch()
        .then(dataUpdate => {
          resolve(dataUpdate[0]);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  deleteReport: function(id){
    return new Promise((resolve, reject) =>{
      Reports.destroy({id: id}).then(data =>{
        resolve(data);
      }).catch(err => {
          reject(err);
        });
    });
  },

  getFeedbackViaReportId :  (reporter_id) => {
      let query = `
            SELECT handle_report.*,users.fullname FROM handle_report  
            JOIN users ON handle_report.feedback_seller_id = users.id WHERE report_id = ${reporter_id} ` ;
      sails.log.info(`getFeedbackViaReportId query ${query}`);
      return new Promise((resolve, reject) => {
       HandleReport.getDatastore().sendNativeQuery(query, (err, result)=> { 
            if(err){
             reject(err)  
            }
           //console.log(result)
           resolve(result.rows)
      });
    })
  },

  


}