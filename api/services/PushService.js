const OneSignal = require('onesignal-node');
const rp = require('request-promise');
module.exports = {

 	pushNotifications: async (tokens, data, title, body) => {
        try {
		let app_id = process.env.APP_ID_ONESIGNAL;
		let api_key = process.env.AUTHORIZATION_ONESIGNAL;
		let  onesignal_url = process.env.URL_ONESIGNAL
	    let client = new OneSignal.Client(app_id, api_key, { apiRoot: onesignal_url});
	    let notification = {
			contents: {"en":body},
			headings: {"en":title},
			include_player_ids: tokens,
			ios_badgeCount:1,
			data:data,
			small_icon: 'ic_stat_onesignal_default.png',
			large_icon: 'ic_stat_onesignal_default.png',
		};
       
	   const response = await client.createNotification(notification);

      } catch (e) {
          if (e instanceof OneSignal.HTTPError) {
                // When status code of HTTP response is not 2xx, HTTPError is thrown.
               sails.log.debug(e.statusCode);
                sails.log.debug(e.body);
              }
      }
	},


 
 pushNotification : (token, data, title, body) => {
  
    let  onesignal_url = process.env.URL_ONESIGNAL
    let post_body ={}
    post_body.app_id = process.env.APP_ID_ONESIGNAL
    post_body.contents ={"en":body}
    post_body.headings ={"en":title}
    post_body.include_player_ids =token
    post_body.ios_badgeCount =1
    post_body.data =data
    post_body.small_icon = 'ic_stat_onesignal_default.png'
    post_body.large_icon = 'ic_stat_onesignal_default.png'
    let authorization = process.env.AUTHORIZATION_ONESIGNAL
    let  options = {
        method: 'POST',
        uri: onesignal_url,
        body: post_body,
        json: true,
         headers: {
                'Authorization': 'Basic ' + authorization
        }
    };
    return new Promise((resolve, reject) => {
       rp(options)
        .then(function (data) {
           resolve(data)
        })
        .catch(function (err) {
            console.log("err" + JSON.stringify(err))
            reject(err)
        });
    })
 }
}
