module.exports = {

    getAccuses :(name,reason,txtStatus,offset,limit) => {
       
       let query = `
        SELECT re.id as accuses_id,re.affect_user_id,re.reporter_id,re.status,re.created_at ,re.description,
        ur.fullname AS creator_name,ur.address AS creator_address,
        ua.fullname AS affect_name,ua.address AS affect_address,
        r.reason_name FROM reports AS re 
        JOIN users ur ON re.reporter_id = ur.id
        JOIN users ua ON re.affect_user_id = ua.id
        JOIN reason r ON r.id = re.reason_id WHERE 1= 1 `;
       
       if(name){
          query += ` AND (ur.fullname  LIKE '%${name}%' OR ua.fullname  LIKE '%${name}%') `;
       }

       if(reason){
          query += ` AND re.reason_id = ${reason}  `;
       }

       if(txtStatus){
          query += ` AND re.status = "${txtStatus}"   `;
       }

        query += ` ORDER BY re.id DESC LIMIT ${offset},${limit} `;

        sails.log.info(`getAccuses ${query}`);
      
        return new Promise((resolve, reject) => {
         Reports.getDatastore().sendNativeQuery(query, (err, result)=> { 
              if(err){
            
               reject(err)  
              }
             resolve(result.rows)
        });
    })
  },

   getTotalAccuses :(name,reason,txtStatus) => {
       
       let query = `
        SELECT COUNT(re.id) as total FROM reports AS re 
        JOIN users ur ON re.reporter_id = ur.id
        JOIN users ua ON re.affect_user_id = ua.id
        JOIN reason r ON r.id = re.reason_id WHERE 1= 1 `;
       
       if(name){
          query += ` AND ( ur.fullname  LIKE '%${name}%' OR ua.fullname  LIKE '%${name}%') `;
       }

       if(reason){
          query += ` AND re.reason_id = ${reason}  `;
       }

       if(txtStatus){
          query += ` AND re.status = "${txtStatus}"   `;
       }



        sails.log.info(`getAccuses ${query}`);
      
        return new Promise((resolve, reject) => {
         Reports.getDatastore().sendNativeQuery(query, (err, result)=> { 
              if(err){
            
               reject(err)  
              }
             resolve(result.rows)
        });
    })
  },


   getDetailAccuses :(reporter_id) => {
       
       let query = `
        SELECT re.id as id,re.affect_user_id,re.reporter_id,re.status,re.created_at ,re.description,
        ur.fullname AS creator_name,ur.address AS ur_address,ur.avatar AS ur_avatar, ur.phone_number AS ur_phone_number,ur.status AS ur_status,
        ur.after_img_identity_card AS ur_after_img_identity_card, ur.before_img_identity_card AS ur_before_img_identity_card,ur.selfie_img AS ur_selfie_img,
        
        ua.fullname AS affect_name,ua.address AS ua_address,ua.avatar AS ua_avatar, ua.phone_number AS ua_phone_number,ua.website AS ua_website,ua.status AS ua_status,
        ua.after_img_identity_card AS ua_after_img_identity_card, ua.before_img_identity_card AS ua_before_img_identity_card,ua.selfie_img AS ua_selfie_img,
        r.reason_name FROM reports AS re 
        JOIN users ur ON re.reporter_id = ur.id
        JOIN users ua ON re.affect_user_id = ua.id
        JOIN reason r ON r.id = re.reason_id  WHERE re.id = ${reporter_id} `;
       
    
        sails.log.info(`getDetailAccuses ${query}`);
      
        return new Promise((resolve, reject) => {
         Reports.getDatastore().sendNativeQuery(query, (err, result)=> { 
              if(err){
            
               reject(err)  
              }
             resolve(result.rows)
        });
    })
  },


  getListImgReport: (reportId) => {
    return new Promise((resolve, reject) => {
      ImageReport.find({report_id:reportId})
        .then(listFile => {
          resolve(listFile);
        })
        .catch(err => {
          reject(err);
        });
    });
  },
}