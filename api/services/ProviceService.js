module.exports = {

  getAll: () => {
    return new Promise((resolve, reject) => {
     Provice.find().then(provices => {
        resolve(provices);
      })
      .catch(err => {
          reject(err);
      });

    });
  },

  findOneById: (id) => {
    return new Promise((resolve, reject) => {
      Provice.findOne({ id: id})
        .then(province => {
          resolve(province);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  getProvinceSeller :() => {
       
       let query = "SELECT * FROM provinces WHERE id IN( SELECT DISTINCT province_id FROM users where reported_number = 0 AND status in('Verify_level_two','Verify_level_one')) "; 
        return new Promise((resolve, reject) => {
       
         Provice.getDatastore().sendNativeQuery(query, (err, result)=> { 
              if(err){
            
               reject(err)  
              }
             resolve(result.rows)
        });
    })
  },

    getProvinceWarningSeller :() => {
       
       let query = "SELECT * FROM provinces WHERE id IN( SELECT DISTINCT province_id FROM users where reported_number > 0 AND status in('Verify_level_two','Verify_level_one')) "; 
        return new Promise((resolve, reject) => {
       
         Provice.getDatastore().sendNativeQuery(query, (err, result)=> { 
              if(err){
            
               reject(err)  
              }
             resolve(result.rows)
        });
    })
  },





}