module.exports = {

    getAll: (seller_id,page_start,limit) => {
       let query = `SELECT an.id,an.payload,an.created_at,ua.fullname, ur.fullname AS relation_name FROM admin_notification an  
          JOIN users ua ON an.affect_seller_id  = ua.id
          JOIN users ur ON an.relation_seller_id = ur.id
          WHERE an.affect_seller_id = ${seller_id} AND an.id NOT IN (SELECT notifcation_id FROM off_notification WHERE seller_id = ${seller_id} AND notifcation_type ="ADMIN")
          ORDER BY an.id DESC  LIMIT ${page_start},${limit} `;
        sails.log.info(`getAll query  ${query}`);
        return new Promise((resolve, reject) => {
         AdminNotification.getDatastore().sendNativeQuery(query, (err, result)=> { 
              if(err){
            
               reject(err)  
              }
             resolve(result.rows)
        });
    })
  },

  createAdminNotification: (data) => {
    return new Promise((resolve, reject) => {
      AdminNotification.create(data).fetch()
        .then(notification => {
          resolve(notification);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

   findOneByCriteria: (criteria, select = []) => {
    return new Promise((resolve, reject) => {
      AdminNotification.findOne({
        where: criteria,
        select: select
      })
        .then(post => {
          resolve(post);
        })
        .catch(err => {
          reject(err);
        });
    });
  },
}