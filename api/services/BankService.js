module.exports = {

  getAll: () => {
    return new Promise((resolve, reject) => {
     Bank.find().then(list_bank => {
        resolve(list_bank);
      })
      .catch(err => {
          reject(err);
      });

    });
  },

  findOneById: (id) => {
    return new Promise((resolve, reject) => {
      Bank.findOne({ id: id})
        .then(bank => {
          resolve(bank);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  findOneByCriteria: (criteria, select = []) => {
    return new Promise((resolve, reject) => {
      UsersBank.findOne({
        where: criteria,
        select: select
      })
        .then(user => {
          resolve(user);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  updateBank:(banks)=>{
     return new Promise((resolve, reject) => {
      Bank.update({id:banks.id}).set(banks).fetch()
        .then(dataUpdate => {
          resolve(dataUpdate[0]);
        })
        .catch(err => {
          reject(err);
        });
    });
  },


  createBank: (banks) => {
    return new Promise((resolve, reject) => {
      Bank.create(banks).fetch()
        .then(bank => {
          resolve(bank);
        })
        .catch(err => {
          reject(err);
        });
    });
  },


  deleteBank: (id) => {
    return new Promise((resolve, reject) =>{
      Bank.destroy({id: id}).then(data =>{
        resolve(data);
      }).catch(err => {
          reject(err);
        });
    });
  },

  createUserBank: (userBankObj) => {
    return new Promise((resolve, reject) => {
      UsersBank.create(userBankObj).fetch()
        .then(userBank => {
          resolve(userBank);
        })
        .catch(err => {
          reject(err);
        });
    });
  },


  deleteUserBank: function(user_id,seller_acc_bank){
    return new Promise((resolve, reject) =>{
      UsersBank.destroy({user_id: user_id,account_number: seller_acc_bank}).then(data =>{
        resolve(data);
      }).catch(err => {
          reject(err);
        });
    });
  },

  deleteById: function(id){
    return new Promise((resolve, reject) =>{
      UsersBank.destroy({id: id}).then(data =>{
        resolve(data);
      }).catch(err => {
          reject(err);
        });
    });
  },


  updateByCriteria: (criteriaObj, dataUpdate) => {
    return new Promise((resolve, reject) => {
      UsersBank.update(criteriaObj).set(dataUpdate).fetch()
        .then(dataUpdate => {
          resolve(dataUpdate[0]);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

    getDetailById :(seller_bank_id) => {
       
       let query = "SELECT u.id,u.fullname,u.address,b.`id` as bank_id,b.`bank_name`,ub.`id` as seller_bank_id,ub.`account_name`,ub.`account_number`,ub.`branch_name` FROM users u  " +
       "JOIN users_bank ub ON ub.`user_id` = u.`id` " +
       "JOIN banks b ON ub.`bank_id` = b.`id` " +
       "WHERE ub.`id` = "+seller_bank_id+" ORDER BY ub.`id` DESC ";
      
      sails.log.info(`getDetailById query  ${query}`);
        return new Promise((resolve, reject) => {
       
         UsersBank.getDatastore().sendNativeQuery(query, (err, result)=> { 
              if(err){
            
               reject(err)  
              }
             resolve(result.rows)
        });
    })
  },


  getDetailSellerBank :(seller_id) => {
       
       let query = "SELECT u.id,u.fullname,b.`id` as bank_id,b.`bank_name`,ub.`id` as seller_bank_id,ub.`account_name`,ub.`account_number`,ub.`branch_name` FROM users u  " +
       "JOIN users_bank ub ON ub.`user_id` = u.`id` " +
       "JOIN banks b ON ub.`bank_id` = b.`id` " +
       "WHERE u.`id` =$1 ORDER BY ub.`id` DESC ";
      
        return new Promise((resolve, reject) => {
         let params = [seller_id];
         User.getDatastore().sendNativeQuery(query,params, (err, result)=> { 
              if(err){
            
               reject(err)  
              }
             resolve(result.rows)
        });
    })
  },

  getListSellerBanks :(name,bankName,accountNumber,offset,limit) => {
       
       let query = `SELECT u.id as user_id,u.fullname,u.address,b.id as bank_id,b.bank_name,ub.id as seller_bank_id,ub.account_name,ub.account_number,ub.branch_name FROM users u 
       JOIN users_bank ub ON ub.user_id = u.id
       JOIN banks b ON ub.bank_id = b.id  WHERE 1= 1`
       

       if(name){
          query += ` AND (u.fullname  LIKE '%${name}%' OR ub.account_name  LIKE '%${name}%') `;
       }

       if(bankName){
          query += ` AND b.bank_name  LIKE '%${bankName}%'  `;
       }

       if(accountNumber){
          query += ` AND ub.account_number  LIKE '%${accountNumber}%'  `;
       }

        query += ` ORDER BY ub.id DESC LIMIT ${offset},${limit} `;

        sails.log.info(`getSellerBanks ${query}`);
      
        return new Promise((resolve, reject) => {
         User.getDatastore().sendNativeQuery(query, (err, result)=> { 
              if(err){
            
               reject(err)  
              }
             resolve(result.rows)
        });
    })
  },


  getTotalSellerBanks :(name,bankName,accountNumber) => {
       
       let query = `SELECT COUNT(u.id) as total_seller_bank  FROM users u 
       JOIN users_bank ub ON ub.user_id = u.id
       JOIN banks b ON ub.bank_id = b.id  WHERE 1= 1`
       

       if(name){
          query += ` AND (u.fullname  LIKE '%${name}%' OR ub.account_name  LIKE '%${name}%') `;
       }

       if(bankName){
          query += ` AND b.bank_name  LIKE '%${bankName}%'  `;
       }

       if(accountNumber){
          query += ` AND ub.account_number  LIKE '%${accountNumber}%'  `;
       }


        sails.log.info(`getTotalSellerBanks ${query}`);
      
        return new Promise((resolve, reject) => {
         User.getDatastore().sendNativeQuery(query, (err, result)=> { 
              if(err){
            
               reject(err)  
              }
             resolve(result.rows)
        });
    })
  },








}