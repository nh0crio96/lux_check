/**
 * Service to generate JWT
 */
var jwt = require('jsonwebtoken');

module.exports = {
  'sign': function(payload) {
    return jwt.sign({
      data: payload
    }, sails.config.custom.secret, {expiresIn: sails.config.system.token.app_expire_time}); // expired in 30 days
  },
  'verify': function(token, callback) {
    jwt.verify(token, sails.config.custom.secret, callback);
  },

  'signTokenAdmin': function(payload) {
    return jwt.sign({
      data: payload
    }, sails.config.custom.secret_token_admin, {expiresIn: sails.config.system.token.admin_expire_time}); // expired in 30 days
  },
  'verifyTokenAdmin': function(token, callback) {
    jwt.verify(token, sails.config.custom.secret_token_admin, callback);
  },

  'signRefreshTokenAdmin': function(payload) {
    return jwt.sign({payload}, sails.config.custom.secret_token_refresh_admin, {expiresIn: sails.config.system.token.adm_refresh_expire_time}); // expired in 30 days
  },
  'verifyRefreshTokenAdmin': function(token, callback) {
    jwt.verify(token, sails.config.custom.secret_token_refresh_admin, callback);
  }
};
