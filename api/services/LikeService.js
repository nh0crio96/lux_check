
module.exports = {
  createLike: (data) => {
    return new Promise((resolve, reject) => {
      Likes.create(data).fetch()
        .then(like => {
          resolve(like);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  countLikeByCriterial: (criteria) => {
    return new Promise((resolve, reject) => {
      Likes.count(criteria)
        .then(total => {
          resolve(total);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  countTotalLike: () => {
    return new Promise((resolve, reject) => {
      Likes.count().then(total => {
          resolve(total);
        })
        .catch(err => {
          reject(err);
        });
    });
  },


  findOneByCriteria: (criteria, select = []) => {
    return new Promise((resolve, reject) => {
      Likes.find({
        where: criteria,
        select: select
      })
        .then(likes => {
          resolve(likes);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  updateByCriteria: (criteriaObj, dataUpdate) => {
    return new Promise((resolve, reject) => {
      Likes.update(criteriaObj).set(dataUpdate).fetch()
        .then(dataUpdate => {
          resolve(dataUpdate[0]);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  deleteByCriteria: (criteriaObj) => {
    return new Promise((resolve, reject) => {
      Likes.destroy(criteriaObj)
        .then(dataDelete => {
          resolve(dataDelete);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  getByArrSourceId: (user_id,arr_source_id,source_type) => {
    return new Promise((resolve, reject) => {
      Likes.find({where: {like_user_id:user_id,source_id: {'in': arr_source_id},source_type:source_type}})
        .then(listFile => {
          resolve(listFile);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  countLike :(arr_source_id,source_type) => {
       let string_source = arr_source_id.join();
       let query = "SELECT COUNT(id) AS total_like,source_id FROM likes WHERE source_id IN ("+string_source+") AND source_type = "+source_type+" GROUP BY source_id" ;
        return new Promise((resolve, reject) => {
         Likes.getDatastore().sendNativeQuery(query, (err, result)=> { 
              if(err){
            
               reject(err)  
              }
             resolve(result.rows)
        });
    })
  },

  getUsersLike :(seller_id,limit, offset) => {
          sails.log.info(`getUsersLike params  ${seller_id}, ${limit}, ${offset}`);
       let query = "SELECT u.fullname,u.avatar,u.id,l.id as like_id FROM users u " +
       "JOIN likes l ON l.like_user_id = u.id " +
       "where l.`affect_user_id` = $1  AND  l.`is_delete` = 0 " +
       "ORDER BY l.`id` DESC " +
       "LIMIT $2 OFFSET $3 ";
       sails.log.info(`getUsersLike query  ${query}`);
        return new Promise((resolve, reject) => {
           let params = [seller_id,limit, offset];
         Likes.getDatastore().sendNativeQuery(query,params, (err, result)=> { 
              if(err){
            
               reject(err)  
              }
             resolve(result.rows)
        });
    })
  },


    getIsLike: (current_seller_id, seller_id) => {
      return new Promise((resolve, reject) => {
        Likes.findOne(
            {like_user_id: current_seller_id, affect_user_id: parseInt(seller_id)}
           
          ).then((item) => {
          resolve(item);
        }).catch((err) => {
          reject(err);
        });
      });
    },


}