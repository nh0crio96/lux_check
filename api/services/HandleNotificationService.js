module.exports = {

	approveReportStatus : async (status,reportId,reporterId,reporterName,affectId,affectName,statuMsg) => {
      
          let foundDeviceReporter = await DeviceService.findListDevice({user_id: reporterId}, ['token','os','device_id']);
          let arr_tokens_reporter = foundDeviceReporter.map(d => d.token);

          let foundDeviceAffect = await DeviceService.findListDevice({user_id: affectId}, ['token','os','device_id']);
          let arr_tokens_affect = foundDeviceAffect.map(d => d.token);

          let dataAttach = {report_id:reportId,type:Notifications.TYPES.ADMIN_CHANGE_REPORT_STATUTS};
          let bodyForReporter=`Admin đã ${statuMsg} tố cáo của bạn về hoạt động bán hàng của ${affectName}`;
          let bodyForAffect=`Admin đã ${statuMsg} tố cáo của ${reporterName} về hoạt động bán hàng của bạn`;

         let findReporterSetting = await UserService.findOneByCriteria({id: reporterId}, [ 'id','common_notification']);
         if (findReporterSetting.common_notification !== 0) { // off common notification 
             PushService.pushNotification(arr_tokens_reporter,dataAttach,"Luxuria", bodyForReporter) ;
         }

         let findAffectSetting = await UserService.findOneByCriteria({id: affectId}, [ 'id','common_notification']);
         if (findAffectSetting.common_notification !== 0) { // off common notification 
            PushService.pushNotification(arr_tokens_affect,dataAttach,"Luxuria", bodyForAffect) ;
         }
          
        return true;
    },


   handleLikeNotification : async (fullnameSender,receiver_id,dataAttach) => {
       
        let findSellerSetting = await UserService.findOneByCriteria({id: receiver_id}, [ 'id','like_notification']);
        if (findSellerSetting.like_notification == 0) { // off comment notification 
          return true;
        }
        let foundDevice = await DeviceService.findListDevice({user_id: receiver_id}, ['token','os','device_id']);
        let arr_tokens = foundDevice.map(d => d.token);
       
        PushService.pushNotification(arr_tokens,dataAttach,"Luxuria", fullnameSender + " đã thích bạn.") ;
        return true;
   },



   handleCmtNotification : async (fullnameSender,receiver_id,dataAttach) => {
        let findSellerSetting = await UserService.findOneByCriteria({id: receiver_id}, [ 'id','cmt_notification']);
        if (findSellerSetting.cmt_notification == 0) { // off comment notification 
          return true;
        }
        let foundDevice = await DeviceService.findListDevice({user_id: receiver_id}, ['token','os','device_id']);
        let arr_tokens = foundDevice.map(d => d.token);
       
        PushService.pushNotification(arr_tokens,dataAttach,"Luxuria", fullnameSender + " đã gửi cho bạn 1 bình luận.") ;
        return true;
   },


    approveSellerStatus : async (seller_id,body,note) => {

       let findSellerSetting = await UserService.findOneByCriteria({id: seller_id}, [ 'id','common_notification']);
       if (findSellerSetting.common_notification == 0) { // off common notification 
          return true;
       }
      
        let foundDevice = await DeviceService.findListDevice({user_id: seller_id}, ['token','os','device_id']);
        let arr_tokens = foundDevice.map(d => d.token);
        let bodySend = body + "\n" + note;
      
        PushService.pushNotification(arr_tokens,{type:Notifications.TYPES.ADMIN_CHANGE_SELLER_STATUS,user_id:seller_id},"Luxuria", bodySend) ;
        return true;
   },

   handleSystemNotification : async (dataAttach,content) => {
      
      let foundDevice = await DeviceService.getListSellerCommon();
      
      if (foundDevice.length > 0) {
        let arr_tokens = foundDevice.map(d => d.token);
        PushService.pushNotification(arr_tokens,dataAttach,"Luxuria",content) ;
     
      }
    
      return true;
   },

   handleReportNotification : async (fullnameSender,receiver_id,dataAttach) => {
      
      let foundDevice = await DeviceService.findListDevice({user_id: receiver_id}, ['token','os','device_id']);
      let arr_tokens = foundDevice.map(d => d.token);
     
      PushService.pushNotification(arr_tokens,dataAttach,"Luxuria", fullnameSender + " đã tố cáo hoạt động bán hàng của bạn.") ;
      return true;
  },





}