module.exports = {
  _config: {
    actions: true,
    shortcuts: true,
    rest: true
  },

  // get notification from system 
  getSystemNotification: async (req, res)=>{
    try{
       const seller_id = req.current_user.id;
       let limit  = req.param('limit') || sails.config.system.per_page;
       let page   = req.param('page') || 1;
       let page_start = (parseInt(page)-1)*parseInt(limit);
         sails.log.debug(`getSystemNotification param ${JSON.stringify(req.allParams())}`);

      let listSystemNoti= await SystemNotificationService.getAll(seller_id,page_start,limit);
      let listAdminNoti = await AdminNotificationService.getAll(seller_id,page_start,limit);


      listSystemNoti = listSystemNoti.map((element) => {
          return {
            id: element.id,
            type: "SYSTEM",
            title:element.title,
            content: element.content,
            created_at: element.created_at
          };
      });

      listAdminNoti = listAdminNoti.map((element) => {
          return {
            id: element.id,
            type: "ADMIN",
            title:'Admin',
            content: element.payload.replace("{{username}}",element.relation_name),
            created_at: element.created_at
          };
      });

      let merge_array =[...listSystemNoti,...listAdminNoti];
      if (merge_array.length > 0){
        merge_array.sort(function compare(a, b) {
        var dateA = new Date(a.created_at);
        var dateB = new Date(b.created_at);
        return dateB - dateA;
      });
      }
  
      res.ok(merge_array);

    } catch (err) {
      return res.negotiate('getSystemNotification', err);
    }
  },

  offNotification: async (req, res)=>{
    try{
    
      const notiMsg = sails.config.msg.notification; 

      const seller_id = req.current_user.id;
      let notification_id = req.body.notification_id;
      let notifcation_type = req.body.notifcation_type;
       sails.log.debug(`offNotification param ${JSON.stringify(req.body)}`);
      if(!notification_id){
        return res.badRequest(notiMsg.err_id_required.code, notiMsg.err_id_required.msg);
      }

      if(!notifcation_type){
        return res.badRequest(notiMsg.err_notification_type_required.code, notiMsg.err_notification_type_required.msg);
      }

      if(![OffNotification.TYPE.ADMIN,OffNotification.TYPE.SYSTEM].includes(notifcation_type)){
        return res.badRequest(notiMsg.err_notification_type_invalid.code, notiMsg.err_notification_type_invalid.msg);
      }

      let foundNotification = {};

      if(notifcation_type == OffNotification.TYPE.ADMIN){
        foundNotification = await AdminNotificationService.findOneByCriteria({id:notification_id,affect_seller_id:seller_id},['id']);
      }


      if(notifcation_type == OffNotification.TYPE.SYSTEM){
        foundNotification = await SystemNotificationService.findOneById(notification_id);
      }

      if (_.isEmpty(foundNotification)) {
          return res.notFound(notiMsg.err_notification_notfound.code, notiMsg.err_notification_notfound.msg);
      }

      let offNotification = await OffNotificationService.createOffNotification({notifcation_id:notification_id,seller_id:seller_id,notifcation_type:notifcation_type,created_at: new Date()})

      res.ok(offNotification)
      

    } catch (err) {
      return res.negotiate('offNotification', err);
    }
  },

};


