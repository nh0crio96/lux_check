var passport = require('passport');
const QRCode = require('qrcode');
const fs = require('fs');
var uniqueRandom = require('unique-random');
const Jimp = require('jimp');
module.exports = {
  _config: {
    actions: true,
    shortcuts: true,
    rest: true
  },


  getUser: async (req, res)=>{
    try{

      let secondsStart = new Date().getTime() / 1000;
      let limit = req.param("limit") || sails.config.system.per_page;
      let page = req.param("page") || 1;
      let txtSearch = req.param("txtSearch");
      let province_id = req.param("province_id");
      let sex = req.param("sex");
      let is_report = req.param("is_report");
      let status =  req.param("status") || 'All';
      let sort = req.param("sort") || 'DESC';
      let current_user_id = req.param("current_user_id") || '';
      let criterial = {};
      let page_start = (parseInt(page)-1)*parseInt(limit);

      let arr_sort = [];

      let arr_sort_reputation = [];
      let arr_sort_warning = [];
      let array_order_reputation=['updated_at','time_comment','time_like'];
      let array_order_warning=['updated_at','time_comment','time_like','time_report','time_handle_report'];

      let random_order_reputation = _.sample(array_order_reputation, 1); // get random 3 element 
      let random_order_warning = _.sample(array_order_warning, 2); // get random 4 element 
  

      if(is_report == 0){
         criterial["reported_number"] =0;
         arr_sort= random_order_reputation;
      }

      if(is_report == 1){
         criterial["reported_number"] = { '>': 0 };
          arr_sort= random_order_warning;
      }

      let data_res =[];
     
      let start_query = new Date().getTime() / 1000;
      let list= await UserService.getListUser(txtSearch,current_user_id,status,sex,is_report,province_id,arr_sort,sort,page_start,parseInt(limit));
      let end_query = new Date().getTime() / 1000;
      sails.log.debug('Total Time process query get user ' + (end_query - start_query));
      if (list.length == 0) {
         return res.ok();
      }
      
      if (txtSearch) {
         list  = _.uniq(list, 'id'); 
      }
      let arr_user_id = list.map((user) =>{return user.id;}); 
    
      let start_orther = new Date().getTime() / 1000;

      let totalReceiverComment = await CommentService.countCommentReceiver(arr_user_id);
      let  totalReceiver = _.mapValues(_.indexBy(totalReceiverComment, 'receiver_id'), cmt => cmt.total_comment);
    
      let size_avatar=  sails.config.system.resize_img.avatar;
      let data = list.map((item) => {
        item.avatar = sails.helpers.getMultipleUrl(item.avatar);
        item.before_img_identity_card = sails.helpers.getMultipleUrl(item.before_img_identity_card);
        item.after_img_identity_card = sails.helpers.getMultipleUrl(item.after_img_identity_card);
        item.selfie_img = sails.helpers.getMultipleUrl(item.selfie_img);
        item.avatar_size = {height:item.avatar_size ? JSON.parse(item.avatar_size).height : '',
        width_medium: size_avatar.width_medium,
        width_low: size_avatar.width_low,
        width_high: item.avatar_size ? JSON.parse(item.avatar_size).width : ''}
        item.total_comment = totalReceiver[item.id] ?  totalReceiver[item.id] : 0;
        return item ;
     });
      let secondsEnd = new Date().getTime() / 1000;
      sails.log.debug('Total Time process orther get user ' + (secondsEnd - start_orther));
      sails.log.debug('Total Time process get list user ' + (secondsEnd - secondsStart));
      res.ok(data);
    } catch (err) {
      return res.negotiate('getUser', err);
    }
  },


  getBank: (req, res)=>{
   try{
       BankService.getAll().then(listBanks =>{
         res.ok(listBanks);
      });

    } catch (err) {
      return res.negotiate('getBank', err);
    }
  },

  getProfile: async (req, res) => {
    try {  
     
      const userMsg = sails.config.msg.user;
      if(!req.param('id')){
        return res.badRequest(userMsg.err_user_id_required.code, userMsg.err_user_id_required.msg);
      }

       sails.log.debug(`getProfile param ${JSON.stringify(req.allParams())}`);

      let seller_id = req.param('id').trim();
      let data = await UserService.getProfile(seller_id);
     
      if (data.length ==0) {
        return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
      }
      let foundUserById = data[0];
      let textInput = foundUserById.id;
      // let qrCode = await CommonService.getQrCode(textInput.toString());
      let qrCode = await QRCode.toDataURL(textInput.toString());
      let total_comment = await CommentService.countCommentByCriterial({receiver_id:seller_id});
     
      foundUserById.avatar = sails.helpers.getMultipleUrl(foundUserById.avatar ? foundUserById.avatar :"");
      foundUserById.before_img_identity_card = sails.helpers.getMultipleUrl(foundUserById.before_img_identity_card ? foundUserById.before_img_identity_card :"");
      foundUserById.after_img_identity_card = sails.helpers.getMultipleUrl(foundUserById.after_img_identity_card ? foundUserById.after_img_identity_card :"");
      foundUserById.selfie_img = sails.helpers.getMultipleUrl(foundUserById.selfie_img ? foundUserById.selfie_img :"");
      foundUserById.qrCode = qrCode;
      foundUserById.total_comment = total_comment;

      if (req.param('current_user_id') && (req.param('current_user_id') !=seller_id)) {
        let foundLike = await LikeService.getIsLike(req.param('current_user_id'),seller_id);
        foundUserById.is_like = _.isEmpty(foundLike) ? 0: 1;
      }


   
      return res.ok(foundUserById);
    } catch (err) {
      return res.negotiate('getProfile', err);
    }
  },

   updateSetting: async (req, res) => {
    try {  
    
      let seller_id =  req.current_user.id;
      let like_notification = req.body.like_notification || 1;
      let cmt_notification = req.body.cmt_notification || 1;
      let common_notification = req.body.common_notification || 1;


       sails.log.debug(`updateSetting param ${JSON.stringify(req.body)}`);

      if (!seller_id) {
        return res.badRequest(userMsg.err_user_id_required.code, userMsg.err_user_id_required.msg);
      }

      let foundUser = await UserService.findOneByCriteria({id: seller_id}, [ 'id']);

      if (_.isEmpty(foundUser)) {
        return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
      }
      let obj_update={
          like_notification:like_notification,
          cmt_notification:cmt_notification,
          common_notification:common_notification,
          updated_at:new Date(),
      }
       let dataUpdate = await UserService.updateByCriteria({id:seller_id},obj_update);
      
      return res.ok(dataUpdate);

    } catch (err) {
      return res.negotiate('updateSetting', err);
    }
  },


  verifyUserV2: async (req, res) => {
    try {
      let secondsStart = new Date().getTime() / 1000;
      const userMsg = sails.config.msg.user;
      const bankMsg = sails.config.msg.bank;
      let invalid_extension = sails.config.system.invalid_extension;

      let id_seller =  req.current_user.id;
  
      const phone_number = req.param('phone_number');
      const fullname = req.param('fullname');
   
      sails.log.debug(`${id_seller} verifyUser param ${JSON.stringify(req.allParams())}`);
      let selectField = ['id','before_img_identity_card','after_img_identity_card','selfie_img'];

      let foundUser = await UserService.findOneByCriteria({id: id_seller}, selectField);
      if (_.isEmpty(foundUser)) {
        return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
      }
      if (!phone_number) {
        return res.badRequest(userMsg.err_phone_required.code, userMsg.err_phone_required.msg);
      }
      let phone = phone_number.trim(); 

      // if(!sails.helpers.validatePhoneNumber(phone)){
      //    return res.badRequest(userMsg.err_phone_format_invalid.code, userMsg.err_phone_format_invalid.msg);
      // }

      // check phone existed
      let findSellerByPhone = await UserService.findOneByCriteria({phone_number: phone,id:id_seller}, [ 'id']);
      if (_.isEmpty(findSellerByPhone)) {
        return res.serverError(userMsg.err_user_invalid.code, userMsg.err_user_invalid.msg);
      }

      // insert basic info seller
      let obj_seller_data ={
        id:id_seller,
        phone_number:phone,
        status:User.STATUS.Pending,
        updated_at: new Date()
      };

      sails.log.debug(`obj_seller_data  ${JSON.stringify(obj_seller_data)}`);
     
      let obj_update ={};

      let dataUpload = await CommonService.uploadImgToServer(req.file('before_img_identity_card'),req.file('after_img_identity_card'),req.file('selfie_img'),id_seller);

      // let values_upload = Object.values(dataUpload)


      // if(invalid_extension.includes(values_upload[0])){
      //    return res.badRequest(userMsg.err_extension_invalid.code, userMsg.err_extension_invalid.msg);
      // }

      // if(!Array.isArray(values_upload[0])){
      //   return res.badRequest(userMsg.err_upload_failed.code, userMsg.err_upload_failed.msg);
      // }
      sails.log.debug("dataUpload=========" + JSON.stringify(dataUpload));


      if (dataUpload.before_img_identity_card.length > 0) {
        dataUpload.before_img_identity_card[0].id = id_seller;
        dataUpload.before_img_identity_card[0].img_type = sails.config.system.s3_img_type.before_identity;
        let path_before_img_identity_card = await UploadService.saveFileVerify(dataUpload.before_img_identity_card[0]);
        obj_seller_data.before_img_identity_card = path_before_img_identity_card;
      }
      else{
        if (!foundUser.before_img_identity_card) {
           return res.badRequest(userMsg.err_before_identity_required.code, userMsg.err_before_identity_required.msg);
        }
      }

      if (dataUpload.after_img_identity_card.length > 0) {
        dataUpload.after_img_identity_card[0].id = id_seller;
        dataUpload.after_img_identity_card[0].img_type = sails.config.system.s3_img_type.after_identity;
        let path_after_img_identity_card =  await UploadService.saveFileVerify(dataUpload.after_img_identity_card[0]);
        obj_seller_data.after_img_identity_card = path_after_img_identity_card;
      }else{
         if (!foundUser.after_img_identity_card) {
           return res.badRequest(userMsg.err_after_identity_required.code, userMsg.err_after_identity_required.msg);
         }
         
      }


      if (dataUpload.selfie_img.length > 0) {
        dataUpload.selfie_img[0].id = id_seller;
        dataUpload.selfie_img[0].img_type = sails.config.system.s3_img_type.selfie;
        let path_selfie_img =  await UploadService.saveFileVerify(dataUpload.selfie_img[0]);
        obj_seller_data.selfie_img = path_selfie_img;
      }else{
         if (!foundUser.selfie_img) {
           return res.badRequest(userMsg.err_selfie_img_required.code, userMsg.err_selfie_img_required.msg);
         }
      }

      sails.log.debug("obj_update========" + JSON.stringify(obj_update))
    
    
      let dataUpdate = await UserService.updateByCriteria({id:id_seller},obj_seller_data);

      dataUpdate.avatar = sails.helpers.getMultipleUrl(dataUpdate.avatar);
      dataUpdate.before_img_identity_card = sails.helpers.getSingleUrl(dataUpdate.before_img_identity_card);
      dataUpdate.after_img_identity_card = sails.helpers.getSingleUrl(dataUpdate.after_img_identity_card);
      dataUpdate.selfie_img = sails.helpers.getSingleUrl(dataUpdate.selfie_img);

        let secondsEnd = new Date().getTime() / 1000;
  
        sails.log.debug('Total Time process verifyUser ' + (secondsEnd - secondsStart));
        res.ok(dataUpdate);

    } catch (err) {
      return res.negotiate('verifyUser', err);
    }
  },

  verifyUser: async (req, res) => {
    try {
      let secondsStart = new Date().getTime() / 1000;
      const userMsg = sails.config.msg.user;
      const bankMsg = sails.config.msg.bank;
      let invalid_extension = sails.config.system.invalid_extension;

      let id_seller =  req.current_user.id;
      const fullname = req.param('fullname');
      const sex = req.param('sex') || 1;
      const province_id = req.param('province_id');
      const phone_number = req.param('phone_number');
      const website = req.param('website') || '';
      const birthday = req.param('birthday') || null;
      const trading_address = req.param('trading_address') || '';
      const describe_yourself = req.param('describe_yourself') || '';
      
      //2020-01-03 15:33:17

      const bank_id = req.param('bank_id');
      const account_name = req.param('account_name');
      const account_number = req.param('account_number');
      const branch_name = req.param('branch_name') || "";

      sails.log.debug(`verifyUser param ${JSON.stringify(req.allParams())}`);
      let selectField = ['id','before_img_identity_card','after_img_identity_card','selfie_img'];

      let foundUser = await UserService.findOneByCriteria({id: id_seller}, selectField);

      if (_.isEmpty(foundUser)) {
        return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
      }

      if (!fullname.trim()) {
        return res.badRequest(userMsg.err_full_name_required.code, userMsg.err_full_name_required.msg);
      }

      if (!province_id) {
        return res.badRequest(userMsg.err_province_required.code, userMsg.err_province_required.msg);
      }

      if (!phone_number) {
        return res.badRequest(userMsg.err_phone_required.code, userMsg.err_phone_required.msg);
      }
      let phone = phone_number.trim(); 

      // if(!sails.helpers.validatePhoneNumber(phone)){
      //    return res.badRequest(userMsg.err_phone_format_invalid.code, userMsg.err_phone_format_invalid.msg);
      // }

      if (website) {
          if(!sails.helpers.validateWebsite(website.trim())){
             return res.badRequest(userMsg.err_website_format_invalid.code, userMsg.err_website_format_invalid.msg);
         }
      }

      if (!bank_id) {
        return res.badRequest(bankMsg.err_bank_id_required.code, bankMsg.err_bank_id_required.msg);
      }

      if (!account_name.trim()) {
        return res.badRequest(bankMsg.err_account_name_required.code, bankMsg.err_account_name_required.msg);
      }

      if (!account_number.trim()) {
        return res.badRequest(bankMsg.err_account_number_required.code, bankMsg.err_account_number_required.msg);
      }

      // check province existed
      let findProvince = await ProviceService.findOneById(province_id);
      if (_.isEmpty(findProvince)) {
        return res.notFound(userMsg.err_province_not_found.code, userMsg.err_province_not_found.msg);
      }

       // check bank existed
      let findBank = await BankService.findOneById(bank_id);
      if (_.isEmpty(findBank)) {
        return res.notFound(bankMsg.err_bank_not_found.code, bankMsg.err_bank_not_found.msg);
      }

      // check account bank existed
      // let findAccountBank = await BankService.findOneByCriteria({account_number:account_number.trim()},['id']);
      // if (!_.isEmpty(findAccountBank)) {
      //   return res.serverError(bankMsg.err_account_number_exist.code, bankMsg.err_account_number_exist.msg);
      // }

      // check phone existed
      let findSellerByPhone = await UserService.findOneByCriteria({phone_number: phone,id:id_seller}, [ 'id']);
      if (_.isEmpty(findSellerByPhone)) {
        return res.serverError(userMsg.err_user_invalid.code, userMsg.err_user_invalid.msg);
      }

      // insert basic info seller
      let obj_seller_data ={
        id:id_seller,
        sex:sex,
        fullname:fullname.trim(),
        province_id:province_id,
        phone_number:phone,
        website:website,
        birthday:birthday,
        status:User.STATUS.Pending,
        trading_address:trading_address,
        describe_yourself:describe_yourself,
        updated_at: new Date()
      };

      sails.log.debug(`obj_seller_data  ${JSON.stringify(obj_seller_data)}`);
     
      let findUserAndBank = await BankService.findOneByCriteria({user_id:id_seller,bank_id:bank_id},['id']);

      let obj_update ={};

      let dataUpload = await CommonService.uploadImgToServer(req.file('before_img_identity_card'),req.file('after_img_identity_card'),req.file('selfie_img'),id_seller);

      // let values_upload = Object.values(dataUpload)


      // if(invalid_extension.includes(values_upload[0])){
      //    return res.badRequest(userMsg.err_extension_invalid.code, userMsg.err_extension_invalid.msg);
      // }

      if(!Array.isArray(values_upload[0])){
        return res.badRequest(userMsg.err_upload_failed.code, userMsg.err_upload_failed.msg);
      }
      sails.log.debug("dataUpload=========" + JSON.stringify(dataUpload));

      let 

      if (dataUpload.before_img_identity_card.length > 0) {
        dataUpload.before_img_identity_card[0].id = id_seller;
        dataUpload.before_img_identity_card[0].img_type = sails.config.system.s3_img_type.before_identity;
        let path_before_img_identity_card = UploadService.saveFile(dataUpload.before_img_identity_card[0]);
        obj_seller_data.before_img_identity_card = path_before_img_identity_card;
      }
      else{
        if (!foundUser.before_img_identity_card) {
           return res.badRequest(userMsg.err_before_identity_required.code, userMsg.err_before_identity_required.msg);
        }
      }

      if (dataUpload.after_img_identity_card.length > 0) {
        dataUpload.after_img_identity_card[0].id = id_seller;
        dataUpload.after_img_identity_card[0].img_type = sails.config.system.s3_img_type.after_identity;
        let path_after_img_identity_card = UploadService.saveFile(dataUpload.after_img_identity_card[0]);
        obj_seller_data.after_img_identity_card = path_after_img_identity_card;
      }else{
         if (!foundUser.after_img_identity_card) {
           return res.badRequest(userMsg.err_after_identity_required.code, userMsg.err_after_identity_required.msg);
         }
         
      }


      if (dataUpload.selfie_img.length > 0) {
        dataUpload.selfie_img[0].id = id_seller;
        dataUpload.selfie_img[0].img_type = sails.config.system.s3_img_type.selfie;
        let path_selfie_img = UploadService.saveFile(dataUpload.selfie_img[0]);
        obj_seller_data.selfie_img = path_selfie_img;
      }else{
         if (!foundUser.selfie_img) {
           return res.badRequest(userMsg.err_selfie_img_required.code, userMsg.err_selfie_img_required.msg);
         }
      }

      sails.log.debug("obj_update========" + JSON.stringify(obj_update))
      let obj_userBank_data ={user_id:id_seller,bank_id:bank_id,account_name:account_name.trim(),account_number:account_number.trim(),created_at: new Date()};

      if (_.isEmpty(findUserAndBank)) {
            sails.log.debug("new user bank");
           const user_bank_insert = await BankService.createUserBank(obj_userBank_data);
      }else{
         sails.log.debug("update user bank");
         await BankService.updateByCriteria({user_id:id_seller,bank_id:bank_id},{bank_id:bank_id,account_name:account_name.trim(),account_number:account_number.trim(),updated_at: new Date()});
      }
    
      let dataUpdate = await UserService.updateByCriteria({id:id_seller},obj_seller_data);

      dataUpdate.avatar = sails.helpers.getMultipleUrl(dataUpdate.avatar);
      dataUpdate.before_img_identity_card = sails.helpers.getMultipleUrl(dataUpdate.before_img_identity_card);
      dataUpdate.after_img_identity_card = sails.helpers.getMultipleUrl(dataUpdate.after_img_identity_card);
      dataUpdate.selfie_img = sails.helpers.getMultipleUrl(dataUpdate.selfie_img);

        let secondsEnd = new Date().getTime() / 1000;
  
        sails.log.debug('Total Time process verifyUser ' + (secondsEnd - secondsStart));
        res.ok(dataUpdate);

    } catch (err) {
      return res.negotiate('verifyUser', err);
    }
  },



  updateProfile: async (req, res) => {
    try {  
      const userMsg = sails.config.msg.user;
      let bucket_name_s3 = process.env.BUCKET_NAME_S3;  

      const currentUserId =  req.current_user.id;
      const fullname = req.param('fullname');
      const sex = req.param('sex') || 1;
      const website = req.param('website') || '';
      const birthday = req.param('birthday') || null;
      const province_id = req.param('province_id');
      const email = req.param('email');
      const phone_number = req.param('phone_number');
      const trading_address = req.param('trading_address') || '';
      const address = req.param('address') || '';
      const describe_yourself = req.param('describe_yourself') || '';

      sails.log.debug(`updateProfile param ${JSON.stringify(req.allParams())}`);

      let obj_update = {};
      obj_update.birthday = birthday;
      obj_update.sex = sex;

      let foundUser = await UserService.findOneByCriteria({id: currentUserId}, [ 'id','verify_code','avatar']);

      if (_.isEmpty(foundUser)) {
        return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
      }
      

      if (!phone_number) {
        return res.badRequest(userMsg.err_phone_required.code, userMsg.err_phone_required.msg);
      }

      let phone='';
      if(phone_number.includes('+84')){
       phone = phone_number.replace('+84', '0');
      }else{
        phone = phone_number;
      }

      if (!phone) {
        return res.badRequest(userMsg.err_phone_required.code, userMsg.err_phone_required.msg);
      }


     

      // if(!sails.helpers.validatePhoneNumber(phone)){
      //    return res.badRequest(userMsg.err_phone_format_invalid.code, userMsg.err_phone_format_invalid.msg);
      // }


      // check phone existed
      let findSellerByPhone = await UserService.findOneByCriteria({phone_number: phone,id: { '!=': currentUserId }}, [ 'id']);
      if (!_.isEmpty(findSellerByPhone)) {
        return res.serverError(userMsg.err_phone_exist.code, userMsg.err_phone_exist.msg);
      }

      obj_update.phone_number = phone;

      if (!fullname) {
        return res.badRequest(userMsg.err_full_name_required.code, userMsg.err_full_name_required.msg);
      }

      obj_update.fullname = fullname;

      if (!province_id) {
        return res.badRequest(userMsg.err_province_required.code, userMsg.err_province_required.msg);
      }

       obj_update.province_id = province_id;

       if (website) {
         if(!sails.helpers.validateWebsite(website.trim())){
          return res.badRequest(userMsg.err_website_format_invalid.code, userMsg.err_website_format_invalid.msg);
         }
      }

      obj_update.website = website;

      if (email) {
        let findSellerByEmail = await UserService.findOneByCriteria({email: email,id: { '!=': currentUserId }}, [ 'id']);
        if (!_.isEmpty(findSellerByEmail)) {
          return res.serverError(userMsg.err_email_unique.code, userMsg.err_email_unique.msg);
        }
      }
      let avatar_size=  sails.config.system.resize_img.avatar;
      obj_update.email = email;
      let dataUpload =  await  UploadService.uploadImgToServer(req.file('avatar_images'), currentUserId);
      if (dataUpload.length > 0) {
          let image = new Jimp(dataUpload[0].imgPath, function (err, image) {
            let width = image.bitmap.width ; 
            let height = image.bitmap.height; 
            avatar_size.height = height;
            obj_update.avatar_size= JSON.stringify({height:height,width:width}); 
          
        });
         if(foundUser.avatar){
           await  AwsService.emptyS3Directory(bucket_name_s3, currentUserId+"/"+sails.config.system.s3_img_type.avatar+"/");
        }
        dataUpload[0].id = currentUserId;
        dataUpload[0].img_type = sails.config.system.s3_img_type.avatar;
        let path_avatar = UploadService.saveFile(dataUpload[0]);
        obj_update.avatar  = path_avatar;
      }

      obj_update.trading_address = trading_address;
      obj_update.address = address;
      obj_update.describe_yourself = describe_yourself;
      obj_update.updated_at = new Date();

      let dataUpdate = await UserService.updateByCriteria({id:currentUserId},obj_update);
      dataUpdate.avatar = sails.helpers.getMultipleUrl(dataUpdate.avatar);
      dataUpdate.before_img_identity_card = sails.helpers.getMultipleUrl(dataUpdate.before_img_identity_card);
      dataUpdate.after_img_identity_card = sails.helpers.getMultipleUrl(dataUpdate.after_img_identity_card);
      dataUpdate.selfie_img = sails.helpers.getMultipleUrl(dataUpdate.selfie_img);
      dataUpdate.avatar_size = avatar_size

      return res.ok(dataUpdate);

    } catch (err) {
      return res.negotiate('updateProfile', err);
    }
  },


  updateAvatar: async (req, res) => {
    try {

      const userMsg = sails.config.msg.user;
      let bucket_name_s3 = process.env.BUCKET_NAME_S3;  
      
      const currentUserId =  req.current_user.id;

      let field_select = ['id', 'phone_number','avatar'];

      let foundUser = await UserService.findOneByCriteria({id: currentUserId}, [ 'id', 'avatar']);

      if (_.isEmpty(foundUser)) {
        return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
      }

      let avatar_url = foundUser.avatar;
      let obj_update ={};
      let dataUpload =  await  UploadService.uploadImgToServer(req.file('avatar_images'), currentUserId);
      let avatar_size=  sails.config.system.resize_img.avatar;
      if (dataUpload.length > 0) {

        let image = new Jimp(dataUpload[0].imgPath, function (err, image) {
            let width = image.bitmap.width ; 
            let height = image.bitmap.height; 
            avatar_size.height = height;
            obj_update.avatar_size= JSON.stringify({height:height,width:width}); 
          
        });
        if(avatar_url){
          await  AwsService.emptyS3Directory(bucket_name_s3, currentUserId+"/"+sails.config.system.s3_img_type.avatar+"/");
        }
        dataUpload[0].id = currentUserId;
        dataUpload[0].img_type = sails.config.system.s3_img_type.avatar;
        let path_avatar = UploadService.saveFile(dataUpload[0]);
        avatar_url = path_avatar;
      }else{
         return res.serverError(userMsg.err_avatar_required.code, userMsg.err_avatar_required.msg);
      }
      obj_update.avatar= avatar_url;
      obj_update.updated_at= new Date();
      let dataUpdate = await UserService.updateByCriteria({id:currentUserId},obj_update);
      
      dataUpdate.avatar = sails.helpers.getMultipleUrl(dataUpdate.avatar);
      dataUpdate.avatar_size = avatar_size

      res.ok(dataUpdate);

    } catch (err) {
      return res.negotiate('updateAvatar', err);
    }
  },


 login: async (req, res) => {
    try {
      const userMsg = sails.config.msg.user;
      let firebase_token = req.param('firebase_token');
      let phone_number = req.param('phone_number').trim();
      let password = req.param('password').trim();
      let fullname = req.param('fullname');

      sails.log.debug(`login param ${JSON.stringify(req.allParams())}`);

       if (!password) {
        return res.serverError(userMsg.err_password_required.code, userMsg.err_password_required.msg);
      }


      // if client post firebase_token
      if (firebase_token) {
        let fireBase_body = await CommonService.getInfoByTokenId(firebase_token);
        sails.log.debug('body decode ====' + JSON.stringify(fireBase_body));

        if (!fullname) {
          return res.badRequest(userMsg.err_full_name_required.code, userMsg.err_full_name_required.msg);
        }

        if (fireBase_body.phone_number) {
          let phone='';
          if(fireBase_body.phone_number.includes('+84')){
           phone = fireBase_body.phone_number.replace('+84', '0');
          }else{
            phone = fireBase_body.phone_number;
          }
          sails.log.debug('phone==================' + phone);
          let field_select = ['id', 'fullname', 'phone_number'];
          let foundUser = await UserService.findOneByCriteria({phone_number: phone}, field_select);
          if (!_.isEmpty(foundUser)) {
            return res.serverError(userMsg.err_phone_exist.code, userMsg.err_phone_exist.msg);
          } else {
            const userData = {};
            userData.phone_number = phone;
            userData.password = password;
            userData.fullname = fullname;
            userData.status = User.STATUS.Guest;
            userData.created_at = new Date();

            let createdUser = await UserService.createUser(userData);
            let token = jwToken.sign(createdUser);
            return res.ok({'user': createdUser, 'token': token});
          }
        } else {
          return res.notFound(userMsg.err_phone_not_found.code, userMsg.err_phone_not_found.msg);
        }
      }

      // if client post phone and pass

      if (!phone_number) {
        return res.serverError(userMsg.err_phone_required.code, userMsg.err_phone_required.msg);
      }

     
      let field_select = ['id', 'phone_number', 'password'];

      let foundUser = await UserService.findOneByCriteria({phone_number: phone_number}, field_select);

       console.log("=======foundUser==========" + JSON.stringify(foundUser))
      if (_.isEmpty(foundUser)) {
        return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
      } else {
        let comparePassword = await User.comparePassword(password, foundUser);
        if (comparePassword) {
          let token = jwToken.sign(foundUser);
          return res.ok({'user': foundUser, 'token': token});
        }else{
            return res.serverError(userMsg.err_password_wrong.code, userMsg.err_password_wrong.msg);
        }
      }


    } catch (err) {
      return res.negotiate('login', err);
    }
  },

  changePasswdV2: async (req, res) => {
    try {
      const currentUserId = req.current_user.id;
      const userMsg = sails.config.msg.user;
      let phone_number = req.body.phone_number;
      let oldPass = req.body.oldPass;
      let newPass = req.body.newPass;
      let verifyNewPass = req.body.verify_new_pass;

      sails.log.debug(`changePasswd param ${JSON.stringify(req.body)}`);
     
      if (!oldPass && !phone_number) {
        return res.badRequest(userMsg.err_old_pass_phone_require.code, userMsg.err_old_pass_phone_require.msg);
      }

      if (!newPass || !verifyNewPass) {
        return res.badRequest(userMsg.err_new_password_required.code, userMsg.err_new_password_required.msg);
      }

      if (newPass !== verifyNewPass) {
        return res.badRequest(userMsg.err_new_password_not_match.code, userMsg.err_new_password_not_match.msg);
      }

      let field_select = ['id', 'password'];
      let foundUser = await UserService.findOneByCriteria({id: currentUserId}, field_select);
  
      if (_.isEmpty(foundUser)) {
        return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
      } 

      let hashPassword = await User.hashPassword(newPass);
      let obj_update = {id: foundUser.id, password: hashPassword,updated_at: new Date()};
      if (phone_number) {
        let findSellerByPhone = await UserService.findOneByCriteria({phone_number: phone_number,id: { '!=': currentUserId }}, field_select);
        if (!_.isEmpty(findSellerByPhone)) {
          return res.serverError(userMsg.err_email_unique.code, userMsg.err_email_unique.msg);
        }
        obj_update.phone_number = phone_number
        let dataUpdate = await UserService.updateUser(obj_update);
        res.ok(dataUpdate);
      }

      if(oldPass) {
          let comparePassword = await User.comparePassword(oldPass, foundUser);
          if (comparePassword=== true) {
            let dataUpdate = await UserService.updateUser(obj_update);
            res.ok(dataUpdate);
          } else {
            return res.serverError(userMsg.err_old_password_not_match.code, userMsg.err_old_password_not_match.msg);
          }
      }
    } catch (err) {
      return res.negotiate('changePasswd', err);
    }
  },

  changePasswd: async (req, res) => {
    try {
      const currentUserId = req.current_user.id;
      const userMsg = sails.config.msg.user;

      let oldPass = req.param('oldPass').trim();
      let newPass = req.param('newPass').trim();
      let verifyNewPass = req.param('verify_new_pass').trim();

      sails.log.debug(`changePasswd param ${JSON.stringify(req.allParams())}`);

      if (!oldPass) {
        return res.badRequest(userMsg.err_old_password_required.code, userMsg.err_old_password_required.msg);
      }

      if (!newPass || !verifyNewPass) {
        return res.badRequest(userMsg.err_new_password_required.code, userMsg.err_new_password_required.msg);
      }

      if (newPass !== verifyNewPass) {
        return res.badRequest(userMsg.err_new_password_not_match.code, userMsg.err_new_password_not_match.msg);
      }

      let field_select = ['id', 'password'];
      let foundUser = await UserService.findOneByCriteria({id: currentUserId}, field_select);
  
      if (_.isEmpty(foundUser)) {
        return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
      } else {
        let comparePassword = await User.comparePassword(oldPass, foundUser);
        if (comparePassword=== true) {
          let hashPassword = await User.hashPassword(newPass);
          const obj_update = {id: foundUser.id, password: hashPassword,updated_at: new Date()};
          let dataUpdate = await UserService.updateUser(obj_update);
          res.ok(dataUpdate);
        } else {
          return res.serverError(userMsg.err_old_password_not_match.code, userMsg.err_old_password_not_match.msg);
        }
      }
    } catch (err) {
      return res.negotiate('changePasswd', err);
    }
  },


  forgotPasswd: async (req, res) => {
    try {
      const userMsg = sails.config.msg.user;
      let firebase_token = req.param('firebase_token').trim();
      let password = req.param('password').trim();

      sails.log.debug(`forgotPasswd param ${JSON.stringify(req.allParams())}`);

      if (!password) {
        return res.badRequest(userMsg.err_password_required.code, userMsg.err_password_required.msg);
      }

      if (!firebase_token) {
        return res.badRequest(userMsg.err_token_firebase_required.code, userMsg.err_token_firebase_required.msg);
      }

      let fireBase_body = await CommonService.getInfoByTokenId(firebase_token);

      if (fireBase_body.phone_number) {
        let phone = fireBase_body.phone_number.replace('+84', '0');
        let field_select = ['id', 'phone_number'];
        let foundUser = await UserService.findOneByCriteria({phone_number: phone}, field_select);
        if (_.isEmpty(foundUser)) {
          return res.notFound(userMsg.err_phone_not_found.code, userMsg.err_phone_not_found.msg);
        }

        let hashPassword = await User.hashPassword(password);
        const obj_update = {id: foundUser.id, password: hashPassword,updated_at: new Date()};
        let dataUpdate = await UserService.updateUser(obj_update);

        res.ok(dataUpdate);

      } else {
        return res.notFound(userMsg.err_phone_not_found.code, userMsg.err_phone_not_found.msg);
      }

    } catch (err) {
      return res.negotiate('forgotPasswd', err);
    }

  },

 loginWithFacebook: async (req, res)=> {
   try {
        const userMsg = sails.config.msg.user;
        let access_token = req.param('access_token');

        sails.log.debug(`loginWithFacebook access_token ${req.param('access_token')}`);
    
        if (!access_token.trim()) {
          return res.badRequest(userMsg.err_access_token_required.code, userMsg.err_access_token_required.msg);
        }

        await passport.authenticate('facebook-token', async (info)=> {
           sails.log.debug(`info ${JSON.stringify(info)}`);
         if(info.id){
            let field_select = ['id','phone_number'];
            let foundUser = await UserService.findOneByCriteria({social_id: info.id}, field_select);
            if (_.isEmpty(foundUser)) {
                const userData = {};
                userData.social_id = info.id;
                userData.fullname = info.name;
                userData.email = info.email || '';
                userData.status =User.STATUS.Guest;
                userData.created_at = new Date();
                let createdUser = await UserService.createUser(userData);
                 sails.log.debug(`createdUser ${JSON.stringify(createdUser)}`);
                let token = jwToken.sign(createdUser);
                return res.ok({'user': createdUser, 'token': token});
            }else{
               let token = jwToken.sign(foundUser);
               return res.ok({'token': token,'user':foundUser});
            }
        }else{
          return res.serverError(userMsg.err_login_facebook_failed.code, userMsg.err_login_facebook_failed.msg);
        }
        })(req, res);
     } catch (err) {
      return res.negotiate('loginWithFacebook', err);
    }
  },

  updatePhoneAndPass: async (req, res) => {
    try {

      const userMsg = sails.config.msg.user;
      const user_id = req.current_user.id;
      let phone_number = req.body.phone_number;
      let newPass = req.body.newPass ;
      let verifyNewPass = req.body.verify_new_pass;

      sails.log.debug(`updatePhoneAndPass param ${JSON.stringify(req.body)}`);
           
      if (!newPass || !verifyNewPass) {
        return res.badRequest(userMsg.err_new_password_required.code, userMsg.err_new_password_required.msg);
      }

      if (newPass !== verifyNewPass) {
        return res.badRequest(userMsg.err_new_password_not_match.code, userMsg.err_new_password_not_match.msg);
      }

      if (!phone_number) {
        return res.badRequest(userMsg.err_phone_required.code, userMsg.err_phone_required.msg);
      }

      //  if(!sails.helpers.validatePhoneNumber(phone_number)){
      //    return res.badRequest(userMsg.err_phone_format_invalid.code, userMsg.err_phone_format_invalid.msg);
      // }

      let userFacebook = await UserService.findOneByCriteria({id: user_id}, [ 'id']);
      if (_.isEmpty(userFacebook)) {
        return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
      }

      let field_select = ['id', 'fullname'];
      let foundUserByPhone = await UserService.findOneByCriteria({phone_number: phone_number}, field_select);

      let hashPassword = await User.hashPassword(newPass);
      const obj_update = {id: userFacebook.id,phone_number: phone_number,password: hashPassword,updated_at: new Date()};
  
      if (_.isEmpty(foundUserByPhone)) {
          // update password for facebook user and response token
        let dataUpdate = await UserService.updateUser(obj_update);
        let token = jwToken.sign(dataUpdate);
        return res.ok({token: token,user:dataUpdate,found_seller:0});
        
      } else {
          let userExist = foundUserByPhone;
          return res.ok({userExist,userFacebook,found_seller:1});
      }
    } catch (err) {
      return res.negotiate('updatePhoneAndPass', err);
    }
  },



  mergeSeller: async (req, res) => {
    try {

      const userMsg = sails.config.msg.user;

      let new_user_id = req.body.new_user_id;
      let existed_user_id = req.body.existed_user_id;
      let is_agree = req.body.is_agree;

      sails.log.debug(`mergeSeller param ${JSON.stringify(req.body)}`);

      if (!new_user_id || !existed_user_id) {
        return res.badRequest(userMsg.err_user_id_required.code, userMsg.err_user_id_required.msg);
      }
     
      if(![0,1].includes(is_agree)){
         return res.badRequest(userMsg.err_param_agree_invalid.code, userMsg.err_param_agree_invalid.msg);
      }


      let foundNewUserById = await UserService.findOneByCriteria({id: new_user_id}, [ 'id','phone_number','social_id']);
      if (_.isEmpty(foundNewUserById)) {
        return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
      }

      let foundExistedUserById = await UserService.findOneByCriteria({id: existed_user_id}, [ 'id','phone_number']);
      if (_.isEmpty(foundExistedUserById)) {
        return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
      }

      if(is_agree ==1){
        let social_id = foundNewUserById.social_id;
        // disable new_user_id
        await UserService.updateUser({id: new_user_id,is_delete:1,social_id: null,phone_number:''});
        // update social_id for existed seller
        let dataUpdate = await UserService.updateUser({id: foundExistedUserById.id,social_id: social_id});

        sails.log.debug(`is is_agree========== ${JSON.stringify(dataUpdate)}`);
        let token = jwToken.sign(dataUpdate);
        return res.ok({token: token,user:dataUpdate});
      }

      //if(is_agree ==0){
         let dataUpdate = await UserService.updateUser({id: new_user_id,phone_number:''});
         sails.log.debug(`not agree========== ${JSON.stringify(dataUpdate)}`);
         let token = jwToken.sign(dataUpdate);
        return res.ok({token: token,user:dataUpdate});
      //}


      
    } catch (err) {
      return res.negotiate('mergeSeller', err);
    }
  },

  logout: async (req, res) => {
    try {
      const deviceMsg = sails.config.msg.devices;
      const userMsg = sails.config.msg.user;

      let currentUserId = req.current_user.id;
      let device_id = req.body.device_id;
      sails.log.debug(`logout param ${JSON.stringify(req.body)}`);

      if (!device_id) {
        return res.serverError(deviceMsg.err_device_id_required.code, deviceMsg.err_device_id_required.msg);
      }

      let foundUserById = await UserService.findOneById(currentUserId);
      if (_.isEmpty(foundUserById)) {
            return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
      }
      sails.log.debug(JSON.stringify({device_id: device_id, user_id: currentUserId}));
      await DeviceService.deleteDevice({device_id: device_id, user_id: currentUserId});
     
      return res.ok();
    } catch (err) {
      return res.negotiate('logout', err);
    }
  },


  fixAvatar: async (req, res) => {
    try {

      const userMsg = sails.config.msg.user;
      let bucket_name_s3 = process.env.BUCKET_NAME_S3;  
      
      const currentUserId = req.param('user_id');

      let field_select = ['id', 'phone_number','avatar'];

      let foundUser = await UserService.findOneByCriteria({id: currentUserId}, [ 'id', 'avatar']);

      if (_.isEmpty(foundUser)) {
        return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
      }

      let avatar_url = foundUser.avatar;
    
      let dataUpload =  await  UploadService.uploadImgToServer(req.file('avatar_images'), currentUserId);
      if (dataUpload.length > 0) {
        if(avatar_url){
          await  AwsService.emptyS3Directory(bucket_name_s3, currentUserId+"/"+sails.config.system.s3_img_type.avatar+"/");
        }
        dataUpload[0].id = currentUserId;
        dataUpload[0].img_type = sails.config.system.s3_img_type.avatar;
        let path_avatar = UploadService.saveFile(dataUpload[0]);
        avatar_url = path_avatar;
      }else{
         return res.serverError(userMsg.err_avatar_required.code, userMsg.err_avatar_required.msg);
      }

      let dataUpdate = await UserService.updateByCriteria({id:currentUserId},{avatar:avatar_url,updated_at: new Date()});
      
      dataUpdate.avatar = sails.helpers.getMultipleUrl(dataUpdate.avatar);

      res.ok(dataUpdate);

    } catch (err) {
      return res.negotiate('updateAvatar', err);
    }
  },


};



