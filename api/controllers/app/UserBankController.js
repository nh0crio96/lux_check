module.exports = {
  _config: {
    actions: true,
    shortcuts: true,
    rest: true
  },

   createUserBank: async (req, res) => {
    try {
        const bankMsg = sails.config.msg.bank;
        const userMsg = sails.config.msg.user;

        let user_id = req.current_user.id;
        let bank_id = req.body.bank_id;
        let account_name = req.body.account_name;
        let branch_name = req.body.branch_name || '';
        let account_number = req.body.account_number;

        sails.log.debug(`createUserBank param ${JSON.stringify(req.body)}`);
    
        let obj ={};
          
       

        if (!bank_id) {
          return res.badRequest(bankMsg.err_bank_id_required.code, bankMsg.err_bank_id_required.msg);
        }

        if (!account_name.trim()) {
          return res.badRequest(bankMsg.err_account_name_required.code, bankMsg.err_account_name_required.msg);
        }

        if (!account_number.trim()) {
          return res.badRequest(bankMsg.err_account_number_required.code, bankMsg.err_account_number_required.msg);
        }

        // check account bank existed
        let findAccountBank = await BankService.findOneByCriteria({account_number:account_number.trim()},['id']);
        if (!_.isEmpty(findAccountBank)) {
          return res.serverError(bankMsg.err_account_number_exist.code, bankMsg.err_account_number_exist.msg);
        }


        // check seller and  bank existed
        let findUserAndBank = await BankService.findOneByCriteria({user_id:user_id,bank_id:bank_id},['id']);
        if (!_.isEmpty(findUserAndBank)) {
          return res.serverError(bankMsg.err_seller_and_bank_exist.code, bankMsg.err_seller_and_bank_exist.msg);
        }


        obj.user_id = user_id;
        obj.bank_id = bank_id;
        obj.account_name = account_name;
        obj.branch_name = branch_name;
        obj.account_number = account_number;
        obj.created_at = new Date();


        sails.log.debug("user bank body: " + JSON.stringify(obj));
        const user_bank_insert = await BankService.createUserBank(obj);
        res.ok(user_bank_insert);

      } catch (err) {
        return res.negotiate('createUserBank', err);
      }
   
  },


   updateUserBank: async (req, res) => {
    try {
        const bankMsg = sails.config.msg.bank;
        const userMsg = sails.config.msg.user;

        let seller_bank_id = req.body.id;
        let user_id = req.current_user.id;
        let bank_id = req.body.bank_id;
        let account_name = req.body.account_name;
        let branch_name = req.body.branch_name || '';
        let account_number = req.body.account_number;

        sails.log.debug(`updateUserBank param ${JSON.stringify(req.body)}`);
    
        let objCriteria ={};
        let objUpdate = {}

        if (!seller_bank_id) {
            return res.badRequest(bankMsg.err_seller_bank_id_required.code, bankMsg.err_seller_bank_id_required.msg);
        }
          
      

        if (!bank_id) {
          return res.badRequest(bankMsg.err_bank_id_required.code, bankMsg.err_bank_id_required.msg);
        }

        if (!account_name.trim()) {
          return res.badRequest(bankMsg.err_account_name_required.code, bankMsg.err_account_name_required.msg);
        }

        if (!account_number.trim()) {
          return res.badRequest(bankMsg.err_account_number_required.code, bankMsg.err_account_number_required.msg);
        }

        // check account bank existed
        let findAccCriteria = {account_number:account_number.trim(),id: { '!=': seller_bank_id }};
        let findAccountBank = await BankService.findOneByCriteria(findAccCriteria,['id']);
        if (!_.isEmpty(findAccountBank)) {
          return res.serverError(bankMsg.err_account_number_exist.code, bankMsg.err_account_number_exist.msg);
        }


        // check seller and  bank existed
        let findUserAndBankCriteria = {user_id:user_id,bank_id:bank_id,id: { '!=': seller_bank_id }};
        let findUserAndBank = await BankService.findOneByCriteria(findUserAndBankCriteria,['id']);
        if (!_.isEmpty(findUserAndBank)) {
          return res.serverError(bankMsg.err_seller_and_bank_exist.code, bankMsg.err_seller_and_bank_exist.msg);
        }


        objCriteria.user_id = user_id;
        objCriteria.id = seller_bank_id;
        
        objUpdate.bank_id = bank_id;
        objUpdate.account_name = account_name;
        objUpdate.branch_name = branch_name;
        objUpdate.account_number = account_number;
        objUpdate.updated_at = new Date();


        sails.log.debug("user bank body: " + JSON.stringify(objUpdate));
        const user_bank_insert = await BankService.updateByCriteria(objCriteria,objUpdate);
        res.ok();

      } catch (err) {
        return res.negotiate('updateUserBank', err);
      }
   
  },


  getBankBySellerId: async (req, res) => {
    try {  
    
      const bankMsg = sails.config.msg.bank;
      const userMsg = sails.config.msg.user;
      
      if(!req.param('seller_id')){
        return res.badRequest(userMsg.err_user_id_required.code, userMsg.err_user_id_required.msg);
      }
      let userBank = await BankService.getDetailSellerBank(req.param('seller_id')); 
      return res.ok(userBank);
    } catch (err) {
      return res.negotiate('getBankBySellerId', err);
    }
  },


};

