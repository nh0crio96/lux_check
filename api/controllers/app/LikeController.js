
module.exports = {
  _config: {
    actions: true,
    shortcuts: true,
    rest: true
  },

  getLikes: async (req, res) => {
     try {
        const likeMsg = sails.config.msg.like;
      
       let limit  = req.param('limit') || sails.config.system.per_page;
       let page   = req.param('page') || 1;
       let current_seller_id = req.current_user.id;

       let page_start = (parseInt(page)-1)*parseInt(limit);
     
        sails.log.debug(`getLikes param ${JSON.stringify(req.allParams())}`);
        if(!current_seller_id){
            return res.badRequest(likeMsg.err_affect_user_required.code, likeMsg.err_affect_user_required.msg);
        }

   
       LikeService.getUsersLike(current_seller_id,parseInt(limit),page_start).then(listLikes =>{
        let data_res =[];
        listLikes.forEach((like)=>{
         let obj_data={
            like_id:like.like_id,
            id:like.id,
            fullname:like.fullname,
            avatar:sails.helpers.getMultipleUrl(like.avatar),
          };

          data_res.push(obj_data);
        });
      
        res.ok(data_res);
      });
      } catch (err) {
        return res.negotiate('getLikes', err);
      }
  },

  createLike: async (req, res) => {
     try {
        const likeMsg = sails.config.msg.like;
        const userMsg = sails.config.msg.user;
        const commonMsg = sails.config.msg.common;

        let like_user_id = req.current_user.id;
        let affect_user_id =  req.body.affect_user_id;

        sails.log.debug(`createLike param ${JSON.stringify(req.body)}`);
     
        if(!affect_user_id){
            return res.badRequest(likeMsg.err_affect_user_required.code, likeMsg.err_affect_user_required.msg);
        }

        if(like_user_id == affect_user_id){
           return res.badRequest(commonMsg.err_seller_invalid.code, commonMsg.err_seller_invalid.msg);
        }

        let field_user_select = ['id','fullname'];
        let foundSender = await UserService.findOneByCriteria({id: like_user_id}, field_user_select);
        if (_.isEmpty(foundSender)) {
          return res.notFound(userMsg.err_sender_not_found.code, userMsg.err_sender_not_found.msg);
        }

         let criteria =  {like_user_id:like_user_id,affect_user_id:affect_user_id};
         let foundLike =  await LikeService.findOneByCriteria(criteria,['id']);
      
        if (_.isEmpty(foundLike[0])) {
            // if not exist ==> save data
            let saveLike =  await LikeService.createLike(criteria);
        
            let data_attach ={
              type: Notifications.TYPES.LIKE,
              like_user_id: like_user_id
            }
            // sent notification 
            await HandleNotificationService.handleLikeNotification(foundSender.fullname,affect_user_id,data_attach);
            // get total like by affect_user_id
            let total_like = await LikeService.countLikeByCriterial({affect_user_id:affect_user_id});
            // update total_like via affect_user_id
            await UserService.updateByCriteria({id:affect_user_id},{total_like:total_like});

            let time_like = new Date();
            await CommonService.updateTimeTwoSeller({id:like_user_id,time_like:time_like},{id:affect_user_id,time_like:time_like});
       
            res.ok();
        }else{
            await LikeService.deleteByCriteria(criteria);

             // get total like by affect_user_id
            let total_like = await LikeService.countLikeByCriterial({affect_user_id:affect_user_id});
            // update total_like via affect_user_id
            await UserService.updateByCriteria({id:affect_user_id},{total_like:total_like})
            res.ok();
         
        }
      } catch (err) {
        return res.negotiate('createLike', err);
      }
  },

    deleteLike: async (req, res) => {
     try {
        const likeMsg = sails.config.msg.like;
        
        let like_user_id = req.current_user.id;
        let like_id =  req.body.like_id;

        sails.log.debug(`deleteLike param ${JSON.stringify(req.body)}`);

        if(!like_id){
            return res.badRequest(likeMsg.err_like_id_required.code, likeMsg.err_like_id_required.msg);
        }
     
        let criteria =  {id:like_id,affect_user_id:like_user_id};
        let foundLike =  await LikeService.findOneByCriteria(criteria,['id']);
      
        if (_.isEmpty(foundLike[0])) {
           return res.notFound(likeMsg.err_like_not_found.code, likeMsg.err_like_not_found.msg);
        }

        let likeUpdate = await LikeService.updateByCriteria({id:like_id},{is_delete:1});

        res.ok(likeUpdate);
      } catch (err) {
        return res.negotiate('deleteLike', err);
      }
  },

}