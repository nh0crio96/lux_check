var passport = require('passport');

module.exports = {
    _config: {
        actions: true,
        shortcuts: true,
        rest: true
      },
    // loginWithFacebookV2: async (req,res)=>{
    //     await passport.authenticate('facebook',{scope:'email'})
    // },
    loginWithFacebookV2CallBack: async (req,res)=>{
      
        const userMsg = sails.config.msg.user;

        await passport.authenticate('facebook',{ failureRedirect: "/login" },async function(err, info) {
        if(info.id){
            let field_select = ['id','phone_number','fullname'];
            let foundUser = await UserService.findOneByCriteria({social_id: info.id}, field_select);
            if (_.isEmpty(foundUser)) {
                const userData = {};
                userData.social_id = info.id;
                userData.fullname = info.name;
                userData.email = info.email || '';
                userData.status =User.STATUS.Guest;
                userData.created_at = new Date();
                let createdUser = await UserService.createUser(userData);
                 sails.log.debug(`createdUser ${JSON.stringify(createdUser)}`);
                let token = jwToken.sign(createdUser);
                const redirect = `intent://callback?${new URLSearchParams({ 'userId': createdUser.id,'fullname':foundUser.fullname}).toString()}?${new URLSearchParams({ 'token': token}).toString()}#Intent;package=${
                process.env.ANDROID_PACKAGE_IDENTIFIER
                    };scheme=signinwithfb;end`;
                
                return res.redirect(307, redirect);
            }else{
               let token = jwToken.sign(foundUser);
               const redirect = `intent://callback?${new URLSearchParams({ 'userId': foundUser.id,'fullname':foundUser.fullname}).toString()}?${new URLSearchParams({ 'token': token}).toString()}#Intent;package=${
                process.env.ANDROID_PACKAGE_IDENTIFIER
                    };scheme=signinwithfb;end`;
              
                return res.redirect(307, redirect);
            }
        }else{
            return res.serverError(userMsg.err_login_facebook_failed.code, userMsg.err_login_facebook_failed.msg);
        }
        })(req, res);
      }
}