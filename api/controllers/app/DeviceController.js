module.exports = {
  _config: {
    actions: true,
    shortcuts: true,
    rest: true
  },

	registerDevice: async (req, res) => {
		try {
			const deviceMsg = sails.config.msg.devices;
			const userMsg = sails.config.msg.user;

			let currentUserId = req.current_user.id;
			let device_id = req.body.device_id;
			let os = req.body.device_os;
			let token = req.body.token;
			sails.log.debug(`registerDevice param ${JSON.stringify(req.body)}`);

			if (!device_id || !os) {
				return res.serverError(deviceMsg.err_device_id_required.code, deviceMsg.err_device_id_required.msg);
			}

			if (!token) {
				return res.serverError(deviceMsg.err_device_token_required.code, deviceMsg.err_device_token_required.msg);
			}

			 let foundUserById = await UserService.findOneById(currentUserId);
		      if (_.isEmpty(foundUserById)) {
		        return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
		      }


			let data = {
				user_id: currentUserId,
				os: os,
				updated_at: new Date()
			};
			if (token) {
				data.token = token.trim();
			}
			let device = await DeviceService.findOneByCriteria({device_id: device_id, os: os});
			if (_.isEmpty(device)) {
				data.device_id = device_id;
				data.created_at = new Date();
				let created = await DeviceService.createDevice(data);
				return res.ok({});
			}
			let updated = await DeviceService.updateById(device.id, data);
			return res.ok();
		} catch (err) {
      return res.negotiate('registerDevice', err);
    }
	},

};



