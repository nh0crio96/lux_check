module.exports = {
  _config: {
    actions: true,
    shortcuts: true,
    rest: true
  },



getComments: async (req, res)=>{
    try {
       const cmtMsg = sails.config.msg.comment;
    
       let limit  = req.param('limit') || sails.config.system.per_page;
       let page   = req.param('page') || 1;
       let page_start = (parseInt(page)-1)*parseInt(limit);
       const receiver_id =  req.param('seller_id');

       sails.log.debug(`getComments param ${JSON.stringify(req.allParams())}`);

         // check receive_id exist
        let field_user_select = ['id'];
        let foundReceiver = await UserService.findOneByCriteria({id: receiver_id}, field_user_select);
        if (_.isEmpty(foundReceiver)) {
          return res.notFound(cmtMsg.err_receiver_not_found.code, cmtMsg.err_receiver_not_found.msg);
        }

       let listComment = await CommentService.getReceiveComments(receiver_id,page_start,parseInt(limit),'DESC');
       let data_res =[];
       
     
       let data = listComment.map((item) => {
        item.avatar = sails.helpers.getMultipleUrl(item.avatar?item.avatar:"");
        return item;
       });
      
        res.ok(data);


    } catch (err) {
          return res.negotiate('getComments', err);
    }
},


getSendComments: async (req, res)=>{
    try {
       const cmtMsg = sails.config.msg.comment;
    
       let limit  = req.param('limit') || sails.config.system.per_page;
       let page   = req.param('page') || 1;
       let page_start = (parseInt(page)-1)*parseInt(limit);
       const sender_id =  req.param('seller_id') ;

        sails.log.debug(`getSendComments param ${JSON.stringify(req.allParams())}`);
         // check receive_id exist
        let field_user_select = ['id'];
        let foundReceiver = await UserService.findOneByCriteria({id: sender_id}, field_user_select);
        if (_.isEmpty(foundReceiver)) {
          return res.notFound(cmtMsg.err_receiver_not_found.code, cmtMsg.err_receiver_not_found.msg);
        }

       let listComment = await CommentService.getSendComments(sender_id,page_start,parseInt(limit),'DESC');
   
       
     
       let listComments = listComment.map((item) => {
        item.avatar = sails.helpers.getSingleUrl(item.avatar);
        return item;
       });
      
       res.ok(listComments);


    } catch (err) {
          return res.negotiate('getSendComments', err);
    }
},


getReceiverCommentsV2: async (req, res)=>{
    try {
       const cmtMsg = sails.config.msg.comment;
    
       let limit  = req.param('limit') || sails.config.system.per_page;
       let page   = req.param('page') || 1;
       let page_start = (parseInt(page)-1)*parseInt(limit);
       const receiver_id =  req.param('seller_id');
       let sort = req.param("sort") || 'DESC';

       sails.log.debug(`getComments param ${JSON.stringify(req.allParams())}`);

         // check receive_id exist
        let field_user_select = ['id'];
        let foundReceiver = await UserService.findOneByCriteria({id: receiver_id}, field_user_select);
        if (_.isEmpty(foundReceiver)) {
          return res.notFound(cmtMsg.err_receiver_not_found.code, cmtMsg.err_receiver_not_found.msg);
        }

       let listComment = await CommentService.getReceiveComments(receiver_id,page_start,parseInt(limit),sort);
       if (listComment.length == 0) {
         return res.ok();
       }
       let total_comment = await  CommentService.countCommentByCriterial({receiver_id:receiver_id});
       let data_res =[];
       
     
       let list_comment = listComment.map((item) => {
        item.avatar = sails.helpers.getMultipleUrl(item.avatar?item.avatar:"");
        return item;
       });
      
        res.ok({list_comment,total_comment});


    } catch (err) {
          return res.negotiate('getComments', err);
    }
},


getSendCommentsV2: async (req, res)=>{
    try {
       const cmtMsg = sails.config.msg.comment;
    
       let limit  = req.param('limit') || sails.config.system.per_page;
       let page   = req.param('page') || 1;
       let page_start = (parseInt(page)-1)*parseInt(limit);
       const sender_id =  req.param('seller_id') ;
       let sort = req.param("sort") || 'DESC';
        sails.log.debug(`getSendComments param ${JSON.stringify(req.allParams())}`);
         // check receive_id exist
        let field_user_select = ['id'];
        let foundReceiver = await UserService.findOneByCriteria({id: sender_id}, field_user_select);
        if (_.isEmpty(foundReceiver)) {
          return res.notFound(cmtMsg.err_receiver_not_found.code, cmtMsg.err_receiver_not_found.msg);
        }

       let listComment = await CommentService.getSendComments(sender_id,page_start,parseInt(limit),sort);
   
       if (listComment.length == 0) {
         return res.ok();
       }
       let total_comment = await  CommentService.countCommentByCriterial({sender_id:sender_id});

       let list_comment = listComment.map((item) => {
        item.avatar = sails.helpers.getSingleUrl(item.avatar);
        return item;
       });
      
       res.ok({list_comment,total_comment});


    } catch (err) {
          return res.negotiate('getSendCommentsv2', err);
    }
},



  createComment: async (req, res) => {
    try {

        const cmtMsg = sails.config.msg.comment;
        let content = req.body.content;
        const sender_id = req.current_user.id;
        let receiver_id = req.body.receiver_id;
        const commonMsg = sails.config.msg.common;

        sails.log.debug(`createComment param ${JSON.stringify(req.body)}`);
     
        let obj ={};
       
        if (!content) {
            return res.badRequest(cmtMsg.err_content_required.code, cmtMsg.err_content_required.msg);
        }
        obj.content = content;

        if (!sender_id) {
            return res.badRequest(cmtMsg.err_sender_id_required.code, cmtMsg.err_sender_id_required.msg);
        }
        
        if (!receiver_id) {
            return res.badRequest(cmtMsg.err_receiver_id_required.code, cmtMsg.err_receiver_id_required.msg);
        }

         if(sender_id == receiver_id){
           return res.badRequest(commonMsg.err_seller_invalid.code, commonMsg.err_seller_invalid.msg);
        }
       
        // check sender_id exist
        let field_user_select = ['id','fullname','avatar','address'];
        let foundSender = await UserService.findOneByCriteria({id: sender_id}, field_user_select);
        if (_.isEmpty(foundSender)) {
          return res.notFound(cmtMsg.err_sender_not_found.code, cmtMsg.err_sender_not_found.msg);
        }
        obj.sender_id = sender_id;

         // check receive_id exist
        let foundReceiver = await UserService.findOneByCriteria({id: receiver_id}, field_user_select);
        if (_.isEmpty(foundReceiver)) {
          return res.notFound(cmtMsg.err_receiver_not_found.code, cmtMsg.err_receiver_not_found.msg);
        }
        obj.receiver_id = receiver_id;
        obj.created_at = new Date();

        let dataSave = await CommentService.saveComments(obj);
        dataSave.fullname = foundSender.fullname;
        dataSave.address = foundSender.address;
        dataSave.avatar =sails.helpers.getMultipleUrl(foundSender.avatar);

        let data_attach ={
          type: Notifications.TYPES.COMMENT,
          comment_id: dataSave.id
        }
       
        //send notification
        if(sender_id !== receiver_id){
           await HandleNotificationService.handleCmtNotification(foundSender.fullname,receiver_id,data_attach);
        }
        let time_comment = new Date();

        await CommonService.updateTimeTwoSeller({id:sender_id,time_comment:time_comment},{id:receiver_id,time_comment:time_comment})
       
        res.ok(dataSave);

      } catch (err) {
        return res.negotiate('createComment', err);
      }
   
  },


  updateComment: async (req, res) => {
   try {
        const cmtMsg = sails.config.msg.comment;

        let comment_id = req.body.id;
        let content = req.body.content;
        let sender_id = req.current_user.id;
        let receiver_id = req.body.receiver_id;
        sails.log.debug(`updateComment param ${JSON.stringify(req.body)}`);

        let objCriteria ={};
         let objdataUpdate ={};
       
        
        if (!comment_id) {
            return res.badRequest(cmtMsg.err_comment_id_required.code, cmtMsg.err_comment_id_required.msg);
        }
         objCriteria.id = comment_id;
        
        
        if (!receiver_id) {
            return res.badRequest(cmtMsg.err_receiver_id_required.code, cmtMsg.err_receiver_id_required.msg);
        }

        if (!content) {
            return res.badRequest(cmtMsg.err_content_required.code, cmtMsg.err_content_required.msg);
        }
        objdataUpdate.content = content;
       

        // check sender_id exist
        let field_user_select = ['id','fullname','avatar','address'];
        let foundSender = await UserService.findOneByCriteria({id: sender_id}, field_user_select);
        if (_.isEmpty(foundSender)) {
          return res.notFound(cmtMsg.err_sender_not_found.code, cmtMsg.err_sender_not_found.msg);
        }
        objCriteria.sender_id = sender_id;


         // check receive_id exist
        let foundReceiver = await UserService.findOneByCriteria({id: receiver_id}, field_user_select);
        if (_.isEmpty(foundReceiver)) {
          return res.notFound(cmtMsg.err_receiver_not_found.code, cmtMsg.err_receiver_not_found.msg);
        }
        objCriteria.receiver_id = receiver_id;
        objdataUpdate.updated_at = new Date();


        let dataUpdate = await CommentService.updateByCriteria(objCriteria,objdataUpdate);
  
        res.ok(dataUpdate);

        dataUpdate.fullname = foundSender.fullname;
        dataUpdate.address = foundSender.address;
        dataUpdate.avatar =sails.helpers.getMultipleUrl(foundSender.avatar);
         res.ok(dataUpdate);

      } catch (err) {


        return res.negotiate('createComment', err);
      }
   
  },


  deleteComment: async (req, res)=>{
    try {
      const cmtMsg = sails.config.msg.comment;
      let receiver_id = req.current_user.id;
      if(!req.param('id')){
        return res.badRequest(cmtMsg.err_comment_id_required.code, cmtMsg.err_comment_id_required.msg);
      }
       sails.log.debug(`deleteComment param ${req.param('id')}`);
      let comment_id = req.param('id').trim();
     
     
      let field_select = ['id'];
      let foundComment = await CommentService.findOneByCriteria({id: comment_id,receiver_id:receiver_id}, field_select);
      if (_.isEmpty(foundComment)) {
        return res.serverError(cmtMsg.err_comment_not_found.code, cmtMsg.err_comment_not_found.msg);
      }

      await CommentService.deleteComment(comment_id);
      res.ok();
    } catch (err) {
          return res.negotiate('deleteComment', err);
        }
    },

};

