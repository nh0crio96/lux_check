module.exports = {
  _config: {
    actions: true,
    shortcuts: true,
    rest: true
  },

  getNotifications: async (req, res)=>{
    try {
    	
        let limit  = req.param('limit') ||  sails.config.system.per_page;
        let currentPage   = req.param('page') || 1;
        let page_start = (parseInt(currentPage)-1)*parseInt(limit);
    

        let current_user_id = req.current_user.id;
        let listNotification =[];
        let page ='';
        let list_data = await NotificationService.getNotification(page_start,parseInt(limit),current_user_id);  
      
        if(list_data.length > 0){
        	let arr_id = list_data.map((e) =>{return e.id;});
        	await NotificationService.updateIsRead(arr_id);
	    	listNotification = list_data.map((element) => {
	    		return {
	    			id: element.id,
	    			target_type: element.target_type,
	    			meta_data: element.meta_data.replace("{{username}}",element.fullname),
	    			fullname: element.fullname,
	    			image: sails.helpers.getMultipleUrl(element.avatar),
	    			target_id: element.target_id,
	    			created_at: element.created_at
	    		};
			});
 		}else{
 			res.ok();
 		}
   
        res.ok(listNotification);
      } catch (err) {
          return res.negotiate('getNotifications', err);
      }
  },


  countNotifications: async (req, res)=>{
    try {
    	
        let current_user_id = req.current_user.id;
        let totalNotification = await NotificationService.getTotalNoti(current_user_id); 
       
        res.ok({totalNotification});
      } catch (err) {
          return res.negotiate('countNotifications', err);
      }
  },
  

}