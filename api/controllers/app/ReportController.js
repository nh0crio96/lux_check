module.exports = {
  _config: {
    actions: true,
    shortcuts: true,
    rest: true
  },

 
  reportForSellerExisted: async (req, res) => {
    try {
       let secondsStart = new Date().getTime() / 1000;
      const reportMsg = sails.config.msg.report;
      const commonMsg = sails.config.msg.common;
      const userMsg = sails.config.msg.user;

    
      const seller_report =req.current_user.id;

      const seller_affect = req.param('seller_id');
      const reason_id = req.param('reason_id');
      const description = req.param('description') || '';

      sails.log.debug(`reportForSellerExisted param ${JSON.stringify(req.allParams())}`);


      if(seller_affect == seller_report){
           return res.badRequest(commonMsg.err_seller_invalid.code, commonMsg.err_seller_invalid.msg);
      }


      if (!seller_affect){
          return res.badRequest(reportMsg.err_affect_user_required.code, reportMsg.err_affect_user_required.msg);
      }

       if(!reason_id){
          return res.badRequest(reportMsg.reason_id_required.code, reportMsg.reason_id_required.msg);
      }

       let foundReport = await ReportService.findOneByCriteria({reporter_id: seller_report,affect_user_id: seller_affect},  ['id']);
      if (!_.isEmpty(foundReport)) {
        return res.notFound(reportMsg.err_report_unique.code, reportMsg.err_report_unique.msg);
      }

      let field_user_select = ['id','fullname'];
      let foundAffectUser = await UserService.findOneByCriteria({id: seller_affect}, field_user_select);
      if (_.isEmpty(foundAffectUser)) {
        return res.notFound(reportMsg.err_affect_user_not_found.code, reportMsg.err_affect_user_not_found.msg);
      }

      let foundSender = await UserService.findOneByCriteria({id: seller_report}, field_user_select);
      if (_.isEmpty(foundSender)) {
        return res.notFound(reportMsg.err_user_not_found.code, reportMsg.err_user_not_found.msg);
      }

      let obj_report = {reporter_id:seller_report,status:Reports.STATUS.Pending,affect_user_id:seller_affect,reason_id:reason_id,description:description,created_at: new Date()};
      let dataSave = await ReportService.saveReport(obj_report);
      let reportId = dataSave.id;
    
      let dataUpload =  await  UploadService.uploadImgToServer(req.file('report_images'), seller_report);
        if(dataUpload.length > 0 ){
          let url_s3_uploaded =[];
          for(let i = 0; i < dataUpload.length; i++){
               dataUpload[i].id = reportId;
               dataUpload[i].img_type = sails.config.system.s3_img_type.report;
               let path_uploaded = UploadService.saveFile(dataUpload[i]);  
               url_s3_uploaded.push({img_url:path_uploaded,report_id:reportId,created_at: new Date()})
            }
          let filesInsert = await ImageReportService.bulkInsertsImg(url_s3_uploaded);
        }

      //   //send notification
        let data_attach ={
          type: Notifications.TYPES.REPORT,
          report_id: reportId
        }

       await HandleNotificationService.handleReportNotification(foundSender.fullname,seller_affect,data_attach);


      let time_report = new Date();
      await CommonService.updateTimeTwoSeller({id:seller_report,time_report:time_report},{id:seller_affect,time_report:time_report});

      let secondsEnd = new Date().getTime() / 1000;
      sails.log.debug('Total Time process reportForSellerExisted ' + (secondsEnd - secondsStart));
      res.ok(dataSave);

    } catch (err) {
   
      return res.negotiate('createReport', err);
    }
  },

  reportForSellerNotExist: async (req, res) => {
    let user_bank_insert ={};
    let seller_insert ={};
    try {
      let secondsStart = new Date().getTime() / 1000;
      const userMsg = sails.config.msg.user;
      const bankMsg = sails.config.msg.bank;
      const reportMsg = sails.config.msg.report;

      const currentUserId  = req.current_user.id;
      const fullname = req.param('fullname');
      const sex = req.param('sex') || 1;
      const province_id = req.param('province_id');
      const phone_number = req.param('phone_number');
      const website = req.param('website') || '';
      const birthday = req.param('birthday') || null;
      const bank_id = req.param('bank_id');
      const account_name = req.param('account_name') || "";
      const account_number = req.param('account_number');
      const branch_name = req.param('branch_name') || "";

      const reason_id = req.param('reason_id');
      const description = req.param('description') || '';

      sails.log.debug(`reportForSellerNotExist param ${JSON.stringify(req.allParams())}`);

      if (!reason_id) {
          return res.badRequest(reportMsg.reason_id_required.code, reportMsg.reason_id_required.msg);
      }

      if (!fullname) {
        return res.badRequest(userMsg.err_full_name_required.code, userMsg.err_full_name_required.msg);
      }

      if (!province_id) {
        return res.badRequest(userMsg.err_province_required.code, userMsg.err_province_required.msg);
      }

      if (!phone_number) {
        return res.badRequest(userMsg.err_phone_required.code, userMsg.err_phone_required.msg);
      }
      let phone = phone_number;

      // if(!sails.helpers.validatePhoneNumber(phone)){
      //    return res.badRequest(userMsg.err_phone_format_invalid.code, userMsg.err_phone_format_invalid.msg);
      // }

      if (website) {
         if(!sails.helpers.validateWebsite(website)){
            return res.badRequest(userMsg.err_website_format_invalid.code, userMsg.err_website_format_invalid.msg);
         }
      }

      if (!bank_id) {
        return res.badRequest(bankMsg.err_bank_id_required.code, bankMsg.err_bank_id_required.msg);
      }

        // check province existed
      let findProvince = await ProviceService.findOneById(province_id);
      if (_.isEmpty(findProvince)) {
        return res.notFound(userMsg.err_province_not_found.code, userMsg.err_province_not_found.msg);
      }


       // check bank existed
      let findBank = await BankService.findOneById(bank_id);
      if (_.isEmpty(findBank)) {
        return res.notFound(bankMsg.err_bank_not_found.code, bankMsg.err_bank_not_found.msg);
      }


      // check account bank existed
      let findAccountBank = await BankService.findOneByCriteria({account_number:account_number.trim()},['id']);
      if (!_.isEmpty(findAccountBank)) {
        return res.serverError(bankMsg.err_account_number_exist.code, bankMsg.err_account_number_exist.msg);
      }


      // check phone existed
      let findSellerByPhone = await UserService.findOneByCriteria({phone_number: phone}, [ 'id']);
      if (!_.isEmpty(findSellerByPhone)) {
        return res.serverError(userMsg.err_phone_exist.code, userMsg.err_phone_exist.msg);
      }

      // insert basic info seller
      let obj_seller_affect_data ={sex:sex,fullname:fullname.trim(),province_id:province_id,phone_number:phone,website:website.trim(),birthday:birthday};
      seller_insert = await UserService.createUser(obj_seller_affect_data);
      let  id_seller_affect = seller_insert.id;


       let findUserAndBank = await BankService.findOneByCriteria({user_id:id_seller_affect,bank_id:bank_id},['id']);
        if (!_.isEmpty(findUserAndBank)) {
          return res.serverError(bankMsg.err_seller_and_bank_exist.code, bankMsg.err_seller_and_bank_exist.msg);
      }

      // insert seller banking
      let obj_userBank_data ={user_id:id_seller_affect,bank_id:bank_id,account_name:account_name.trim(),account_number:account_number.trim()};
       user_bank_insert = await BankService.createUserBank(obj_userBank_data);


      let obj_report = {reporter_id:currentUserId,status:Reports.STATUS.Pending,affect_user_id:id_seller_affect,reason_id:reason_id,description:description,created_at: new Date()};
      let dataSave = await ReportService.saveReport(obj_report);
    
      let dataUpload =  await  UploadService.uploadImgToServer(req.file('report_images'), dataSave.id);
        if(dataUpload.length > 0 ){
          let url_s3_uploaded =[];
          for(let i = 0; i < dataUpload.length; i++){
               dataUpload[i].id = dataSave.id;
               dataUpload[i].img_type = sails.config.system.s3_img_type.report;
               let path_uploaded = UploadService.saveFile(dataUpload[i]);  
               url_s3_uploaded.push({img_url:path_uploaded,report_id:dataSave.id,created_at: new Date()})
            }
          let filesInsert = await ImageReportService.bulkInsertsImg(url_s3_uploaded);
        }


      let time_report = new Date();
      await CommonService.updateTimeTwoSeller({id:id_seller_affect,time_report:time_report},{id:currentUserId,time_report:time_report});

      let secondsEnd = new Date().getTime() / 1000;
      sails.log.debug('Total Time process reportForSellerNotExist ' + (secondsEnd - secondsStart));

      res.ok(dataSave);

    } catch (err) {
      // delete bank and user
      await BankService.deleteById(user_bank_insert.id);
      await UserService.deleteById(seller_insert.id);
      return res.negotiate('reportForSellerNotExist', err);
    }
  },


  createFeedback: async (req, res) => {
    try {

      const reportMsg = sails.config.msg.report;
      const userMsg = sails.config.msg.user;

    
      const feedback_seller_id =req.current_user.id;
      const report_id = req.body.report_id;
      const feedback_content = req.body.feedback_content;

      sails.log.debug(`createFeedback param ${JSON.stringify(req.body)}`);


      if(!report_id){
          return res.badRequest(reportMsg.err_report_id_required.code, reportMsg.err_report_id_required.msg);
      }

       if(!feedback_content){
          return res.badRequest(reportMsg.err_feedback_content_required.code, reportMsg.err_feedback_content_required.msg);
      }

      let field_select = ['id'];
      let foundReport = await ReportService.findOneByCriteria({id: report_id,affect_user_id: feedback_seller_id}, field_select);
      if (_.isEmpty(foundReport)) {
        return res.notFound(reportMsg.err_report_not_found.code, reportMsg.err_report_not_found.msg);
      }

      let obj_save = {report_id:report_id,feedback_content:feedback_content,feedback_seller_id:feedback_seller_id,created_at: new Date()};

      let dataSave = await HandleReportService.saveFeedback(obj_save);

     
      res.ok(dataSave);

    } catch (err) {
   
      return res.negotiate('createFeedback', err);
    }
  },

  listReport: async (req, res)=>{
    try {
       const cmtMsg = sails.config.msg.comment;
    
       let limit  = req.param('limit') || sails.config.system.per_page;
       let page   = req.param('page') || 1;
       let page_start = (parseInt(page)-1)*parseInt(limit);
       const affect_user_id = req.param('seller_id');
       const status = req.param('status') || '';

      sails.log.debug(`listReport param ${JSON.stringify(req.allParams())}`);

       if(!affect_user_id){
          return res.badRequest(reportMsg.err_affect_user_required.code, reportMsg.err_affect_user_required.msg);
      }
     // let status = Reports.STATUS.Accept;
      let listReports = await ReportService.getReportsByAffectId(affect_user_id,status,page_start,limit);
      
      if (listReports.length > 0) {
        let arr_report_id = listReports.map(r => r.id);
        let listImg = await AccusesService.getListImgReport(arr_report_id);
        
        let data = listReports.map((item) => {
           let images=[];
           item.listImg =[];
           item.avatar = sails.helpers.getMultipleUrl(item.avatar);

           listImg.forEach((img)=>{
              if(item.id == img.report_id){
                images.push(sails.helpers.getMultipleUrl(img.img_url))
              }
            });

           item.listImg =images;
           return item;
        });
        res.ok(data);

     }else{
        res.ok();
     }

    } catch (err) {
          return res.negotiate('listReport', err);
    }
},


  listReportViaReporter: async (req, res)=>{
    try {
       const cmtMsg = sails.config.msg.comment;
    
       let limit  = req.param('limit') || sails.config.system.per_page;
       let page   = req.param('page') || 1;
       let page_start = (parseInt(page)-1)*parseInt(limit);
       const affect_user_id = req.current_user.id;

       sails.log.debug(`listReportViaReporter param ${JSON.stringify(req.allParams())}`);
      
       let listReports = await ReportService.getReportByReportertId(affect_user_id,page_start,limit);

       if (listReports.length > 0) {
            let arr_report_id = listReports.map(r => r.id);
            let listImg = await AccusesService.getListImgReport(arr_report_id);
            
            let data = listReports.map((item) => {
               let images=[];
               item.listImg =[];
               item.avatar = sails.helpers.getMultipleUrl(item.avatar);

               listImg.forEach((img)=>{
                  if(item.id == img.report_id){
                    images.push(sails.helpers.getMultipleUrl(img.img_url))
                  }
                });

               item.listImg =images;
               return item;
            });
            res.ok(data);

       }else{
          res.ok();
       }
     
    } catch (err) {
      return res.negotiate('listReport', err);
    }
 },

  getDetailReport: async (req, res)=>{
      try {
        
        const reportMsg = sails.config.msg.report;
        const commonMsg = sails.config.msg.common;


        const report_id = req.param('report_id');

        sails.log.debug(`getDetailReport param ${req.param('report_id')}`);
      
        if (!report_id) {
          return res.badRequest(reportMsg.err_report_id_required.code, reportMsg.err_report_id_required.msg);
        }
       
        let report  = await AccusesService.getDetailAccuses(report_id);

        let listImg = await AccusesService.getListImgReport(report_id);

        let reportImage = listImg.map((item) => {
            item.img_url = sails.helpers.getMultipleUrl(item.img_url);
            return item ;
        });

        if (_.isEmpty(report)) {
          return res.notFound(reportMsg.err_report_not_found.code, reportMsg.err_report_not_found.msg);
        }


        let data_report = report[0];
        const reportData={
          id : data_report.id,
          status:data_report.status,
          reason_name:data_report.reason_name,
          description:data_report.description,
          created_at:data_report.created_at
        };

        const reporter_seller={
          id : data_report.reporter_id,
          name:data_report.creator_name,
          status: data_report.ur_status,
          address:data_report.ur_address,
          phone_numbe:data_report.ur_phone_number,
          avatar: sails.helpers.getSingleUrl(data_report.ur_avatar),
          after_img_identity_card: sails.helpers.getSingleUrl(data_report.ur_after_img_identity_card),
          before_img_identity_card: sails.helpers.getSingleUrl(data_report.ur_before_img_identity_card),
          selfie_img: sails.helpers.getSingleUrl(data_report.ur_selfie_img)
        };


         const affect_seller={
          id : data_report.affect_user_id,
          name:data_report.affect_name,
          status: data_report.ua_status,
          address:data_report.ua_address,
          phone_numbe:data_report.ua_phone_number,
          website:data_report.ua_website,
          avatar: sails.helpers.getSingleUrl(data_report.ua_avatar),
          after_img_identity_card: sails.helpers.getSingleUrl(data_report.ua_after_img_identity_card),
          before_img_identity_card: sails.helpers.getSingleUrl(data_report.ua_before_img_identity_card),
          selfie_img: sails.helpers.getSingleUrl(data_report.ua_selfie_img)
        }
      
        res.ok({reportData,reporter_seller,affect_seller,reportImage});
      } catch (err) {
            return res.negotiate('getDetailAccuses', err);
          }
      },


   getFeedBack: async (req, res)=>{
      try {
        
        const reportMsg = sails.config.msg.report;
        const commonMsg = sails.config.msg.common;


        const report_id = req.param('report_id');

        sails.log.debug(`getFeedBack param ${req.param('report_id')}`);
      
        if (!report_id) {
          return res.badRequest(reportMsg.err_report_id_required.code, reportMsg.err_report_id_required.msg);
        }
       
       let listFeedBack = await ReportService.getFeedbackViaReportId(report_id);
      
        res.ok(listFeedBack);
      } catch (err) {
            return res.negotiate('getDetailAccuses', err);
          }
      },

    deleteReport: async (req, res) => {
     try {
        const reportMsg = sails.config.msg.report;
        const commonMsg = sails.config.msg.common;
        let reporter_id = req.current_user.id;
        const report_id = req.param('report_id');

        sails.log.debug(`deleteReport param ${JSON.stringify(req.allParams())}`);

        if(!report_id){
          return res.badRequest(reportMsg.err_report_id_required.code, reportMsg.err_report_id_required.msg);
        }
     
        let field_select = ['id'];
        let foundReport = await ReportService.findOneByCriteria({id: report_id,reporter_id: reporter_id}, field_select);
        if (_.isEmpty(foundReport)) {
          return res.notFound(reportMsg.err_report_not_found.code, reportMsg.err_report_not_found.msg);
        }

        let obj_update= {id:report_id,is_delete:1}
        let reportUpdate = await ReportService.updateReport(obj_update);

        res.ok(reportUpdate);
      } catch (err) {
        return res.negotiate('deleteReport', err);
      }
  },






};

