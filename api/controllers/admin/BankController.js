module.exports = {
  _config: {
    actions: true,
    shortcuts: true,
    rest: true
  },



  getBanks: (req, res)=>{
   try{
       BankService.getAll().then(listBanks =>{
         res.ok(listBanks);
      });

    } catch (err) {
      return res.negotiate('getBank', err);
    }
  },


  createBank: async (req, res) => {
    try {
    
        const bankMsg = sails.config.msg.bank;
        let current_account_id = req.current_user.id;
        let bank_name = req.body.bank_name;
         sails.log.debug(`createBank param ${JSON.stringify(req.body)}`);
        let obj ={};
       
        if (!bank_name) {
            return res.badRequest(bankMsg.err_bank_name_required.code, bankMsg.err_bank_name_required.msg);
        }
        obj.bank_name = bank_name;

        obj.created_by =current_account_id;
        obj.created_at = new Date();

        let dataSave = await BankService.createBank(obj);

        res.ok(dataSave);
    } catch (err) {
      return res.negotiate('createBank', err);
    }
  },

  updateBank: async (req, res) => {
    try {
    
        const bankMsg = sails.config.msg.bank;
        let current_account_id = req.current_user.id;
        let bank_name = req.body.bank_name;
        let id = req.body.id;
        sails.log.debug(`updateBank param ${JSON.stringify(req.body)}`);
        let obj ={};
       
        if (!bank_name) {
            return res.badRequest(bankMsg.err_bank_name_required.code, bankMsg.err_bank_name_required.msg);
        }

        if (!id) {
            return res.badRequest(bankMsg.err_bank_id_required.code, bankMsg.err_bank_id_required.msg);
        }
        obj.id = id;
        obj.bank_name = bank_name;

        obj.updated_by =current_account_id;
        obj.updated_at = new Date();

        let dataUpdate = await BankService.updateBank(obj);

        res.ok(dataUpdate);
    } catch (err) {
      return res.negotiate('updateBank', err);
    }
  },


  deleteBank: async (req, res) => {
    try {
    
       const bankMsg = sails.config.msg.bank;
        let current_account_id = req.current_user.id;


        if(!req.param('id')){
           return res.badRequest(bankMsg.err_bank_id_required.code, bankMsg.err_bank_id_required.msg);
        }

         sails.log.debug(`deleteBank param ${JSON.stringify(req.allParams())}`);
       await BankService.deleteBank(req.param('id'));

        //send notification

     
        res.ok();
    } catch (err) {
      return res.negotiate('deleteBank', err);
    }
  },

  getDetailBank: async (req, res)=>{
    try {
       const bankMsg = sails.config.msg.bank;
      
       if(!req.param('id')){
           return res.badRequest(bankMsg.err_bank_id_required.code, bankMsg.err_bank_id_required.msg);
        }

     let bank=  await BankService.findOneById(req.param('id'));
      res.ok(bank);
    } catch (err) {
          return res.negotiate('getDetailBank', err);
        }
    },



};
