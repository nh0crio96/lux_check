module.exports = {
  _config: {
    actions: true,
    shortcuts: true,
    rest: true
  },

getListComments: async (req, res)=>{
    try {
       const cmtMsg = sails.config.msg.comment;
    
       let limit  = req.param('limit') || sails.config.system.per_page;
       let page   = req.param('page') || 1;
       let page_start = (parseInt(page)-1)*parseInt(limit);
       const seller_id =  req.param('seller_id');
       const seller_name =  req.param('seller_name');

       sails.log.debug(`getListComments param ${JSON.stringify(req.allParams())}`);


       let listComments = await CommentService.getComments(seller_id,seller_name,page_start,limit);
    
       let totalData = await CommentService.getTotalComment(seller_id,seller_name);
       let pagination = sails.helpers.createPanigate(page,limit,totalData[0].total_comment);
     
        res.ok({listComments,pagination});


    } catch (err) {
          return res.negotiate('getListComments', err);
    }
},

getReceiveComments: async (req, res)=>{
    try {
       const cmtMsg = sails.config.msg.comment;
    
       let limit  = req.param('limit') || sails.config.system.per_page;
       let page   = req.param('page') || 1;
       let page_start = (parseInt(page)-1)*parseInt(limit);
       const receiver_id =  req.param('seller_id');

       sails.log.debug(`getReceiveComments param ${JSON.stringify(req.allParams())}`);

         // check receive_id exist
        let field_user_select = ['id'];
        let foundReceiver = await UserService.findOneByCriteria({id: receiver_id}, field_user_select);
        if (_.isEmpty(foundReceiver)) {
          return res.notFound(cmtMsg.err_receiver_not_found.code, cmtMsg.err_receiver_not_found.msg);
        }

       let listComment = await CommentService.getReceiveComments(receiver_id,page_start,limit);
    
       let totalData = await CommentService.countCommentByCriterial({receiver_id:receiver_id});
       let pagination = sails.helpers.createPanigate(page,limit,totalData);
     
       let listComments = listComment.map((item) => {
        item.avatar = sails.helpers.getSingleUrl(item.avatar);
        return item;
       });
      
        res.ok({listComments,pagination});


    } catch (err) {
          return res.negotiate('getReceiveComments', err);
    }
},




getSendComments: async (req, res)=>{
    try {
       const cmtMsg = sails.config.msg.comment;
    
       let limit  = req.param('limit') || sails.config.system.per_page;
       let page   = req.param('page') || 1;
       let page_start = (parseInt(page)-1)*parseInt(limit);
       const sender_id =  req.param('seller_id');
       sails.log.debug(`getSendComments param ${JSON.stringify(req.allParams())}`);
         // check receive_id exist
        let field_user_select = ['id'];
        let foundReceiver = await UserService.findOneByCriteria({id: sender_id}, field_user_select);
        if (_.isEmpty(foundReceiver)) {
          return res.notFound(cmtMsg.err_receiver_not_found.code, cmtMsg.err_receiver_not_found.msg);
        }

       let listComment = await CommentService.getSendComments(sender_id,page_start,limit);
       let totalData = await CommentService.countCommentByCriterial({sender_id:sender_id});
       let pagination = sails.helpers.createPanigate(page,limit,totalData);
       
     
       let listComments = listComment.map((item) => {
        item.avatar = sails.helpers.getSingleUrl(item.avatar);
        return item;
       });
      
       res.ok({listComments,pagination});


    } catch (err) {
          return res.negotiate('getSendComments', err);
    }
},




  deleteComment: async (req, res)=>{
    try {
      const cmtMsg = sails.config.msg.comment;
      
      if(!req.param('id')){
        return res.badRequest(cmtMsg.err_comment_id_required.code, cmtMsg.err_comment_id_required.msg);
      }

      let comment_id = req.param('id').trim();
     
      sails.log.debug(`deleteComment param ${JSON.stringify(req.allParams())}`);
      let field_select = ['id'];
      let foundComment = await CommentService.findOneByCriteria({id: comment_id}, field_select);
      if (_.isEmpty(foundComment)) {
        return res.serverError(cmtMsg.err_comment_not_found.code, cmtMsg.err_comment_not_found.msg);
      }

      await CommentService.deleteComment(comment_id);
      res.ok();
    } catch (err) {
          return res.negotiate('deleteComment', err);
        }
    },

}