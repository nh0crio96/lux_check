module.exports = {
  _config: {
    actions: true,
    shortcuts: true,
    rest: true
  },

   getListAccuses: async (req, res)=>{
    try{
      let limit = req.param("limit") || sails.config.system.per_page;
      let page = req.param("page") || 1;
      let txtName = req.param("txtName");
      let reason = req.param("reason");
      let txtStatus = req.param("txtStatus");
      let page_start = (parseInt(page)-1)*parseInt(limit);
    
      sails.log.debug(`getListAccuses param ${JSON.stringify(req.allParams())}`);
     
       let data_res =[];
       let list= await AccusesService.getAccuses(txtName,reason,txtStatus,page_start,parseInt(limit));

       let totalData = await AccusesService.getTotalAccuses(txtName,reason,txtStatus);
       let pagination = sails.helpers.createPanigate(page,limit,totalData[0].total);
  
    
      
      res.ok({list,pagination});
    } catch (err) {
      return res.negotiate('getListAccuses', err);
    }
  },

  deleteReport: async (req, res)=>{
    try {
      
      const reportMsg = sails.config.msg.report;
      const commonMsg = sails.config.msg.common;
      const report_id = req.param('accuses_id');

      sails.log.debug(`deleteReport param ${JSON.stringify(req.allParams())}`);
    
      if (!report_id) {
        return res.badRequest(reportMsg.err_report_id_required.code, reportMsg.err_report_id_required.msg);
      }
     
      await ReportService.deleteReport(report_id);
    
      res.ok();
    } catch (err) {
          return res.negotiate('deleteReport', err);
        }
    },


  handleAccuses: async (req, res)=>{
    try {
      
      const reportMsg = sails.config.msg.report;
      const commonMsg = sails.config.msg.common;
      const userMsg = sails.config.msg.user;

      let current_account_id = req.current_user.id;

      let report_id = req.body.accuses_id;
      let status  = req.body.status;
      sails.log.debug(`handleAccuses param ${JSON.stringify(req.body)}`);
      let obj ={};
     
      if (!report_id) {
        return res.badRequest(reportMsg.err_report_id_required.code, reportMsg.err_report_id_required.msg);
      }

      if(!['Reject','Accept'].includes(status)){
           return res.badRequest(reportMsg.err_param_status_invalid.code, reportMsg.err_param_status_invalid.msg);
      }

      let field_select = ['id','reporter_id','affect_user_id','status'];
      let foundReport = await ReportService.findOneByCriteria({id: report_id}, field_select);
      if (_.isEmpty(foundReport)) {
        return res.notFound(reportMsg.err_report_not_found.code, reportMsg.err_report_not_found.msg);
      }

      let foundUser = await UserService.findOneByCriteria({id: foundReport.affect_user_id}, [ 'id','reported_number','fullname']);
      if (_.isEmpty(foundUser)) {
        return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
      }


      let reporter = await UserService.findOneByCriteria({id: foundReport.reporter_id}, [ 'id','reported_number','fullname']);
      if (_.isEmpty(reporter)) {
        return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
      }

      obj.id = report_id;
      obj.status = status;
      obj.handler_id = current_account_id;
      obj.updated_at = new Date();

      let reportUpdate = await ReportService.updateReport(obj);

      if (status ==='Reject') {
        if (foundReport.status =='Accept') {
              let total_report = parseInt(foundUser.reported_number) - 1;
              await UserService.updateByCriteria({id:foundReport.affect_user_id},{reported_number:total_report});
              await ReportService.deleteReport(report_id);
        }
      
      }
    
      if (status ==='Accept') {
        let total_report = parseInt(foundUser.reported_number) +1;
        await UserService.updateByCriteria({id:foundReport.affect_user_id},{reported_number:total_report});
      }

      // handle notification 
      let statuMsg = status == 'Accept' ? 'chấp nhận' : 'từ chối';

      let payloadReporter=`Admin đã ${statuMsg} tố cáo của bạn về hoạt động bán hàng của {{username}}`;
      let payloadAffect=`Admin đã ${statuMsg} tố cáo của {{username}} về hoạt động bán hàng của bạn`;


      let admin_notification_reporter = {
        payload:payloadReporter,
        affect_seller_id:foundReport.reporter_id,
        relation_seller_id:foundReport.affect_user_id,
        notification_type:AdminNotification.TYPE.STATUS_REPORT,
        created_at:new Date(),
        created_by:current_account_id,
      };

       let admin_notification_affect = {
        payload:payloadAffect,
        affect_seller_id:foundReport.affect_user_id,
        relation_seller_id:foundReport.reporter_id,
        notification_type:AdminNotification.TYPE.STATUS_REPORT,
        created_at:new Date(),
        created_by:current_account_id,
      };

        let time_handle_report = new Date();

        await CommonService.updateTimeTwoSeller({id:foundReport.affect_user_id,time_handle_report:time_handle_report},{id:foundReport.reporter_id,time_handle_report:time_handle_report})

       await AdminNotificationService.createAdminNotification(admin_notification_reporter);
       await AdminNotificationService.createAdminNotification(admin_notification_affect);
    

      await HandleNotificationService.approveReportStatus(status,report_id,reporter.id,reporter.fullname,foundUser.id,foundUser.fullname,statuMsg);
      
      res.ok(reportUpdate);
    } catch (err) {
          return res.negotiate('handleAccuses', err);
        }
    },

    getDetailAccuses: async (req, res)=>{
      try {
        
        const reportMsg = sails.config.msg.report;
        const commonMsg = sails.config.msg.common;


        const report_id = req.param('accuses_id');
         sails.log.debug(`getDetailAccuses param ${JSON.stringify(req.allParams())}`);
        if (!report_id) {
          return res.badRequest(reportMsg.err_report_id_required.code, reportMsg.err_report_id_required.msg);
        }
       
        let report  = await AccusesService.getDetailAccuses(report_id);

        let listImg = await AccusesService.getListImgReport(report_id);
        let listFeedBack = await ReportService.getFeedbackViaReportId(report_id);
      

        let dataImage = listImg.map((item) => {
            item.img_url = sails.helpers.getMultipleUrl(item.img_url);
            return item ;
        });

        if (_.isEmpty(report)) {
          return res.notFound(reportMsg.err_report_not_found.code, reportMsg.err_report_not_found.msg);
        }


        let data_report = report[0];
        const accuses={
          id : data_report.id,
          status:data_report.status,
          reason_name:data_report.reason_name,
          description:data_report.description,
          created_at:data_report.created_at
        };

        const reporter_seller={
          id : data_report.reporter_id,
          name:data_report.creator_name,
          status: data_report.ur_status,
          address:data_report.ur_address,
          phone_numbe:data_report.ur_phone_number,
          avatar: sails.helpers.getSingleUrl(data_report.ur_avatar),
          after_img_identity_card: sails.helpers.getSingleUrl(data_report.ur_after_img_identity_card),
          before_img_identity_card: sails.helpers.getSingleUrl(data_report.ur_before_img_identity_card),
          selfie_img: sails.helpers.getSingleUrl(data_report.ur_selfie_img)
        };


         const affect_seller={
          id : data_report.affect_user_id,
          name:data_report.affect_name,
          status: data_report.ua_status,
          address:data_report.ua_address,
          phone_numbe:data_report.ua_phone_number,
          website:data_report.ua_website,
          avatar: sails.helpers.getSingleUrl(data_report.ua_avatar),
          after_img_identity_card: sails.helpers.getSingleUrl(data_report.ua_after_img_identity_card),
          before_img_identity_card: sails.helpers.getSingleUrl(data_report.ua_before_img_identity_card),
          selfie_img: sails.helpers.getSingleUrl(data_report.ua_selfie_img)
        }
      
        res.ok({accuses,reporter_seller,affect_seller,dataImage,listFeedBack});
      } catch (err) {
            return res.negotiate('getDetailAccuses', err);
          }
      },


     invalidAccusesHandle: async (req, res)=>{
      try {

        const reportMsg = sails.config.msg.report;
        const commonMsg = sails.config.msg.common;
        const userMsg = sails.config.msg.user;

        let affect_user_id = req.body.affect_user_id;
        let reset_to_zezo = req.body.reset_to_zezo || 0;
        let change_number = req.body.change_number;
        let increment     = req.body.is_increment || 0;

        let current_account_id = req.current_user.id;

        sails.log.debug(`invalidAccusesHandle body ${JSON.stringify(req.body)}`);
        
        change_number = parseInt(change_number,10)

        let foundUser = await UserService.findOneByCriteria({id: affect_user_id}, [ 'id','reported_number','fullname']);
        if (_.isEmpty(foundUser)) {
          return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
        }

        let current_report_number = parseInt(foundUser.reported_number,10)


        if(current_report_number ===0){
           return res.badRequest(reportMsg.err_cannot_handle_invalid_accuses.code, reportMsg.err_cannot_handle_invalid_accuses.msg);
        }
        let new_number_report

        if(parseInt(reset_to_zezo,10) ===0 || increment !==0){
            if(!change_number){
              return res.badRequest(reportMsg.err_change_number_is_required.code, reportMsg.err_change_number_is_required.msg);
            }
      }

        if(increment ===0){

          if(parseInt(reset_to_zezo,10) ===0){
            
              if(change_number ===0){
                return res.badRequest(reportMsg.err_number_change_invalid.code, reportMsg.err_number_change_invalid.msg);
              }

              if(change_number > current_report_number){
                return res.badRequest(reportMsg.err_less_than_or_equal.code, reportMsg.err_less_than_or_equal.msg);
              }
              new_number_report = current_report_number - change_number
          }else{
               new_number_report = 0;
          }

        }

        if(increment !==0){
            new_number_report = change_number + current_report_number
        }
        
        const user_update=await UserService.updateByCriteria({id:affect_user_id},{reported_number:new_number_report,updated_by:current_account_id});
        res.ok(user_update);
      }catch (err) {
            return res.negotiate('invalidAccusesHandle', err);
          }
      

      }


};



