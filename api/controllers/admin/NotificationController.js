module.exports = {
  _config: {
    actions: true,
    shortcuts: true,
    rest: true
  },

  getNotification: async (req, res) => {
    try {
    
      let limit  = parseInt(req.param('limit')) || sails.config.system.per_page;
      let currentPage   = req.param('page') || 1;
      let offset = (parseInt(currentPage)-1)*parseInt(limit);

        sails.log.debug(`getNotification param ${JSON.stringify(req.allParams())}`);
 
      let listNotification = await SystemNotificationService.getNotification(offset,limit);
      let totalNotification = await SystemNotificationService.countNotification();

  
      panigate = sails.helpers.createPanigate(currentPage,limit,totalNotification);
      
      return res.ok({listNotification,panigate});
    } catch (err) {
      return res.negotiate('getNotification', err);
    }
  },


  createNotification: async (req, res) => {
    try {
    
        const notiMsg = sails.config.msg.notification;
        let current_account_id = req.current_user.id;
        let content = req.body.content;
        let title = req.body.title;
        sails.log.debug(`createNotification param ${JSON.stringify(req.body)}`);
        let obj ={};
       
        if (!content) {
            return res.badRequest(notiMsg.err_content_required.code, notiMsg.err_content_required.msg);
        }
         obj.content = content;

         if (!title) {
            return res.badRequest(notiMsg.err_title_required.code, notiMsg.err_title_required.msg);
        }
       
        obj.title = title;
        obj.created_by =current_account_id;
        obj.created_at = new Date();

        let dataSave = await SystemNotificationService.createNotification(obj);

        await HandleNotificationService.handleSystemNotification({system_notification_id: dataSave.id},content);

        res.ok(dataSave);
    } catch (err) {
      return res.negotiate('createNotification', err);
    }
  },


  updateNotification: async (req, res) => {
    try {
    
        const notiMsg = sails.config.msg.notification;
        let current_account_id = req.current_user.id;

        let id = req.body.id;
        let content = req.body.content;
        let title = req.body.title;
         sails.log.debug(`updateNotification param ${JSON.stringify(req.body)}`);
        let obj ={};
       
        if (!id) {
            return res.badRequest(notiMsg.err_id_required.code, notiMsg.err_id_required.msg);
        }

        if (!content) {
            return res.badRequest(notiMsg.err_content_required.code, notiMsg.err_content_required.msg);
        }
         obj.content = content;

         if (!title) {
            return res.badRequest(notiMsg.err_title_required.code, notiMsg.err_title_required.msg);
        }

        obj.id = id;
        obj.title = title;
        obj.updated_by =current_account_id;
        obj.updated_at = new Date();

        let dataUpdate = await SystemNotificationService.updateNotification(obj);

     
        res.ok(dataUpdate);
    } catch (err) {
      return res.negotiate('updateNotification', err);
    }
  },



 delNotification: async (req, res) => {
    try {
    
        const notiMsg = sails.config.msg.notification;
        let current_account_id = req.current_user.id;


        if(!req.param('id')){
            return res.badRequest(notiMsg.err_id_required.code, notiMsg.err_id_required.msg);
        }

         sails.log.debug(`delNotification param ${JSON.stringify(req.allParams())}`);

       await SystemNotificationService.deleteNotification(req.param('id'));

        res.ok();
    } catch (err) {
      return res.negotiate('updateNotification', err);
    }
  },


   getDetailNotification: async (req, res) => {
    try {
    
        const notiMsg = sails.config.msg.notification;
   
        if(!req.param('id')){
            return res.badRequest(notiMsg.err_id_required.code, notiMsg.err_id_required.msg);
        }

         sails.log.debug(`getDetailNotification param ${JSON.stringify(req.allParams())}`);

       let notification=  await SystemNotificationService.findOneById(req.param('id'));
        res.ok(notification);
    } catch (err) {
      return res.negotiate('updateNotification', err);
    }
  },




};
