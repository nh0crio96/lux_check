module.exports = {
  _config: {
    actions: true,
    shortcuts: true,
    rest: true
  },


  getReason: (req, res)=>{
    try{
   
       ReasonService.getAll().then(listReason =>{
         res.ok(listReason);
      });

    } catch (err) {
      return res.negotiate('getReason', err);
    }
  },

};

