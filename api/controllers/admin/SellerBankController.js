module.exports = {
  _config: {
    actions: true,
    shortcuts: true,
    rest: true
  },
  getSellerBank: async (req, res)=>{
    try{
      let limit = req.param("limit") || sails.config.system.per_page;
      let page = req.param("page") || 1;

      let name = req.param("name") || '';
      let bankName = req.param("bankName") || '';
      let accountNumber = req.param("accountNumber") || '';

      let page_start = (parseInt(page)-1)*parseInt(limit);

      sails.log.debug(`getSellerBank param ${JSON.stringify(req.allParams())}`);
      let listSellerBank = await BankService.getListSellerBanks(name,bankName,accountNumber,page_start,limit);

       let totalData = await BankService.getTotalSellerBanks(name,bankName,accountNumber);
       
      let pagination = sails.helpers.createPanigate(page,limit,totalData[0].total_seller_bank);


   
      
      res.ok({listSellerBank,pagination});
    } catch (err) {
      return res.negotiate('getUser', err);
    }
  },


    createSellerBank: async (req, res) => {
    try {
        const bankMsg = sails.config.msg.bank;
        const userMsg = sails.config.msg.user;

        let current_account_id = req.current_user.id;

        let seller_id = req.body.seller_id;
        let bank_id = req.body.bank_id;
        let account_name = req.body.account_name;
        let branch_name = req.body.branch_name || '';
        let account_number = req.body.account_number;
        sails.log.debug(`createSellerBank param ${JSON.stringify(req.body)}`);
        let obj ={};
          
         if (!seller_id) {
          return res.badRequest(userMsg.err_user_id_required.code, userMsg.err_user_id_required.msg);
        }

        if (!bank_id) {
          return res.badRequest(bankMsg.err_bank_id_required.code, bankMsg.err_bank_id_required.msg);
        }

        if (!account_name.trim()) {
          return res.badRequest(bankMsg.err_account_name_required.code, bankMsg.err_account_name_required.msg);
        }

        if (!account_number.trim()) {
          return res.badRequest(bankMsg.err_account_number_required.code, bankMsg.err_account_number_required.msg);
        }

        // check account bank existed
        let findAccountBank = await BankService.findOneByCriteria({account_number:account_number.trim()},['id']);
        if (!_.isEmpty(findAccountBank)) {
          return res.serverError(bankMsg.err_account_number_exist.code, bankMsg.err_account_number_exist.msg);
        }


        // check seller and  bank existed
        let findUserAndBank = await BankService.findOneByCriteria({user_id:seller_id,bank_id:bank_id},['id']);
        if (!_.isEmpty(findUserAndBank)) {
          return res.serverError(bankMsg.err_seller_and_bank_exist.code, bankMsg.err_seller_and_bank_exist.msg);
        }


        obj.user_id = seller_id;
        obj.bank_id = bank_id;
        obj.account_name = account_name;
        obj.branch_name = branch_name;
        obj.account_number = account_number;
        obj.created_by = current_account_id;
        obj.created_at = new Date();


        sails.log.debug("user bank body: " + JSON.stringify(obj));
        const user_bank_insert = await BankService.createUserBank(obj);
        res.ok(user_bank_insert);

      } catch (err) {
        return res.negotiate('createUserBank', err);
      }
   
  },


  updateSellerBank: async (req, res) => {
    try {
        const bankMsg = sails.config.msg.bank;
        const userMsg = sails.config.msg.user;

        let current_account_id = req.current_user.id;


        let seller_bank_id = req.body.id;
        let seller_id = req.body.seller_id;
        let bank_id = req.body.bank_id;
        let account_name = req.body.account_name;
        let branch_name = req.body.branch_name || '';
        let account_number = req.body.account_number;

        sails.log.debug(`updateSellerBank param ${JSON.stringify(req.body)}`);
    
        let objCriteria ={};
        let objUpdate = {};

        if (!seller_id) {
          return res.badRequest(userMsg.err_user_id_required.code, userMsg.err_user_id_required.msg);
        }

        if (!seller_bank_id) {
            return res.badRequest(bankMsg.err_seller_bank_id_required.code, bankMsg.err_seller_bank_id_required.msg);
        }
          
      

        if (!bank_id) {
          return res.badRequest(bankMsg.err_bank_id_required.code, bankMsg.err_bank_id_required.msg);
        }

        if (!account_name.trim()) {
          return res.badRequest(bankMsg.err_account_name_required.code, bankMsg.err_account_name_required.msg);
        }

        if (!account_number.trim()) {
          return res.badRequest(bankMsg.err_account_number_required.code, bankMsg.err_account_number_required.msg);
        }

        // check account bank existed
        let findAccCriteria = {account_number:account_number,id: { '!=': seller_bank_id }};
       
        let findAccountBank = await BankService.findOneByCriteria(findAccCriteria,['id']);
        
        if (!_.isEmpty(findAccountBank)) {
          return res.serverError(bankMsg.err_account_number_exist.code, bankMsg.err_account_number_exist.msg);
        }


        // check seller and  bank existed
        let findUserAndBankCriteria = {user_id:seller_id,bank_id:bank_id,id: { '!=': seller_bank_id }};
        let findUserAndBank = await BankService.findOneByCriteria(findUserAndBankCriteria,['id']);
        if (!_.isEmpty(findUserAndBank)) {
          return res.serverError(bankMsg.err_seller_and_bank_exist.code, bankMsg.err_seller_and_bank_exist.msg);
        }


        objCriteria.user_id = seller_id;
        objCriteria.id = seller_bank_id;
        
        objUpdate.bank_id = bank_id;
        objUpdate.account_name = account_name;
        objUpdate.branch_name = branch_name;
        objUpdate.account_number = account_number;
        objUpdate.updated_by = current_account_id;
        objUpdate.updated_at = new Date();


        sails.log.debug("user bank body: " + JSON.stringify(objUpdate));
        const user_bank_insert = await BankService.updateByCriteria(objCriteria,objUpdate);
        res.ok(objUpdate);

      } catch (err) {
        return res.negotiate('updateUserBank', err);
      }
   
  },


  deleteSellerBank: async (req, res)=>{
    try {
       const bankMsg = sails.config.msg.bank;
      
      if(!req.param('id')){
          return res.badRequest(bankMsg.err_seller_bank_id_required.code, bankMsg.err_seller_bank_id_required.msg);
      }
       sails.log.debug(`deleteSellerBank param ${JSON.stringify(req.allParams())}`);
      await BankService.deleteById(req.param('id'));
      res.ok();
    } catch (err) {
          return res.negotiate('deleteSellerBank', err);
        }
    },


  getDetailSellerBank: async (req, res)=>{
    try {
       const bankMsg = sails.config.msg.bank;
      
      if(!req.param('seller_bank_id')){
          return res.badRequest(bankMsg.err_seller_bank_id_required.code, bankMsg.err_seller_bank_id_required.msg);
      }
       sails.log.debug(`getDetailSellerBank param ${JSON.stringify(req.allParams())}`);
     let seller_bank= await BankService.getDetailById(req.param('seller_bank_id'));
      res.ok(seller_bank);
    } catch (err) {
          return res.negotiate('getDetailSellerBank', err);
        }
    },

  



};
