module.exports = {
  _config: {
    actions: true,
    shortcuts: true,
    rest: true
  },

   getProfile: async (req, res) => {
    try {  
    
      const userMsg = sails.config.msg.user;
      let current_id =  req.current_user.id;

       if(!req.param('id')){
        return res.badRequest(userMsg.err_user_id_required.code, userMsg.err_user_id_required.msg);
      }

   
      let data = await AccountService.findOneById(req.param('id'));
   
      return res.ok(data);
    } catch (err) {
      return res.negotiate('getProfile', err);
    }
  },





  register: async (req, res) => {
    try {
      const userMsg = sails.config.msg.user;
   
      let email = req.body.email;
      let password = req.body.password;
      let username = req.body.username;

      const userData = {};
      userData.email = email;
      userData.password = password;
      userData.username = username;
      userData.status='Activate';
      userData.created_at = new Date();
       sails.log.debug(`register param ${JSON.stringify(req.body)}`);
      let createdAccount = await AccountService.createAccount(userData);
      let token = jwToken.signTokenAdmin(createdAccount);
      let tokenRefresh = jwToken.signRefreshTokenAdmin(createdAccount);
      let dataSaveTokenRefresh ={user_id:createdAccount.id,token_refresh: tokenRefresh,created_at : new Date()};
      await RefreshTokenService.createRefreshToken(dataSaveTokenRefresh);
     // return res.ok({'account': createdAccount, 'token': token});
         return res.ok({'account': createdAccount, 'token': token,'refreshToken': tokenRefresh});


    } catch (err) {
      return res.negotiate('register', err);
    }
  },


  getNewToken: async (req, res) => {
    try {
      const userMsg = sails.config.msg.user;
   
    
      let refresh_token = req.body.refresh_token;

      if(!refresh_token){
        return res.badRequest(userMsg.err_user_token_required.code, userMsg.err_user_token_required.msg);
      }

       sails.log.debug(`getNewToken param ${JSON.stringify(req.body)}`);
       jwToken.verifyRefreshTokenAdmin(refresh_token, async (err, decoded) =>{
        if (err) {
          sails.log.error(JSON.stringify(err));

          let errorData = {};
          errorData.error_code = err.name;
          errorData.message = err.message;

          return res.status(401).json(errorData);
        }
        
          let user = decoded.payload;
          if (_.isEmpty(user)) {
            return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
          }
           

          let field_select = ['id', 'username'];
          let foundAccount = await AccountService.findOneByCriteria({id: user.id}, field_select);

          if (_.isEmpty(foundAccount)) {
            return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
          }

          let tokenRefresh = await RefreshTokenService.findOneByUserId(user.id);


          if (_.isEmpty(tokenRefresh)) {
            return res.notFound(userMsg.err_user_token_not_found.code, userMsg.err_user_token_not_found.msg);
          }

          if(refresh_token != tokenRefresh.token_refresh){
             return res.serverError(userMsg.err_user_token_invalid.code, userMsg.err_user_token_invalid.msg);
          }

          let newToken = jwToken.signTokenAdmin(foundAccount);
          let newTokenRefresh = jwToken.signRefreshTokenAdmin(foundAccount);
           let dataSaveTokenRefresh ={user_id:foundAccount.id,token_refresh: newTokenRefresh,updated_at : new Date()};
           await RefreshTokenService.updateRefreshToken(dataSaveTokenRefresh);
          return res.ok({ 'token': newToken,'refresh': newTokenRefresh});
        

         //return res.ok(decoded);
       
      });


    } catch (err) {
      return res.negotiate('getNewToken', err);
    }
  },



 login: async (req, res) => {
    try {
      const userMsg = sails.config.msg.user;

      let password = req.param('password').trim();
      let email = req.param('email');
      sails.log.debug(`login param ${JSON.stringify(req.allParams())}`);
      if (!password) {
        return res.serverError(userMsg.err_password_required.code, userMsg.err_password_required.msg);
      }

       if (!email) {
        return res.serverError(userMsg.err_email_required.code, userMsg.err_email_required.msg);
      }

      // if client post phone and pass
     
      let field_select = ['id', 'email', 'password'];
      let foundAccount = await AccountService.findOneByCriteria({email: email}, field_select);

     
      if (_.isEmpty(foundAccount)) {
        return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
      } else {
        let comparePassword = await Accounts.comparePassword(password, foundAccount);
        if (comparePassword) {
           let token = jwToken.signTokenAdmin(foundAccount);
            let tokenRefresh = jwToken.signRefreshTokenAdmin(foundAccount);
            let dataSaveTokenRefresh ={user_id:foundAccount.id,token_refresh: tokenRefresh,updated_at : new Date()};
            await RefreshTokenService.updateRefreshToken(dataSaveTokenRefresh);
          return res.ok({'account': foundAccount, 'token': token,'refreshToken': tokenRefresh});
        }else{
            return res.serverError(userMsg.err_password_wrong.code, userMsg.err_password_wrong.msg);
        }
      }


    } catch (err) {
      return res.negotiate('login', err);
    }
  },

  changePassword: async (req, res) => {
    try {
      const currentUserId = req.current_user.id;
      const userMsg = sails.config.msg.user;

      let oldPass = req.body.oldPass;
      let newPass = req.body.newPass;
      let verifyNewPass = req.body.verify_new_pass;

      sails.log.debug(`changePassword param ${JSON.stringify(req.body)}`);

      if (!oldPass) {
        return res.badRequest(userMsg.err_old_password_required.code, userMsg.err_old_password_required.msg);
      }

      if (!newPass || !verifyNewPass) {
        return res.badRequest(userMsg.err_new_password_required.code, userMsg.err_new_password_required.msg);
      }

      if (newPass !== verifyNewPass) {
        return res.badRequest(userMsg.err_new_password_not_match.code, userMsg.err_new_password_not_match.msg);
      }

      let field_select = ['id', 'password'];
      let foundAcc = await AccountService.findOneByCriteria({id: currentUserId}, field_select);
  
      if (_.isEmpty(foundAcc)) {
        return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
      } else {
        let comparePassword = await Accounts.comparePassword(oldPass, foundAcc);
        if (comparePassword=== true) {
          let hashPassword = await Accounts.hashPassword(newPass);
          const obj_update = {id: foundAcc.id, password: hashPassword};
          let dataUpdate = await AccountService.updateAccount(obj_update);
          res.ok(dataUpdate);
        } else {
          return res.serverError(userMsg.err_old_password_not_match.code, userMsg.err_old_password_not_match.msg);
        }
      }
    } catch (err) {
      return res.negotiate('changePasswd', err);
    }
  }

};



