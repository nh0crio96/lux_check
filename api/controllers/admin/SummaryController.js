module.exports = {
  _config: {
    actions: true,
    shortcuts: true,
    rest: true
  },

  getSummary:  (req, res) => {

    let data ={};
   Promise.all([
      UserService.countUserByCriterial({status:User.STATUS.Guest}),
      UserService.countUserByCriterial({status:User.STATUS.Pending}),
      UserService.countUserByCriterial({status:User.STATUS.Verify_level_one}),
      UserService.countUserByCriterial({status:User.STATUS.Verify_level_two}),
      LikeService.countTotalLike(),
      CommentService.countTotalComment(),
      ReportService.countTotalReport(),
      ReportService.countReportCriterial({status:Reports.STATUS.Pending}),
      ReportService.countReportCriterial({status:Reports.STATUS.Accept}),
      ReportService.countReportCriterial({status:Reports.STATUS.Reject})
    ]).then(([totalRegister, totalPending,totalVerifyOne,totalVerifyTwo,totalLike,totalComment,totalAccuses,totalAccusesPending,totalAccusesAccept,totalAccusesReject]) =>{

      data.totalRegister = totalRegister ;
      data.totalPending = totalPending;
      data.totalVerifyOne = totalVerifyOne;
      data.totalVerifyTwo = totalVerifyTwo;
      data.totalLike = totalLike;
      data.totalComment = totalComment;
      data.totalSeller = totalRegister + totalPending + totalVerifyOne + totalVerifyTwo;
      data.totalAccuses = totalAccuses;
      data.totalAccusesPending = totalAccusesPending;
      data.totalAccusesAccept = totalAccusesAccept;
      data.totalAccusesReject = totalAccusesReject;
      return res.ok(data);

    }).catch((err) =>{
      throw err
    });

  },


};



