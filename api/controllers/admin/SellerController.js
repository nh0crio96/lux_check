const uniqueRandom = require('unique-random');
const Jimp = require('jimp');
module.exports = {
  _config: {
    actions: true,
    shortcuts: true,
    rest: true
  },

  getProfile: async (req, res) => {
    try {  
    
      const userMsg = sails.config.msg.user;
      if(!req.param('user_id')){
        return res.badRequest(userMsg.err_user_id_required.code, userMsg.err_user_id_required.msg);
      }

      let seller_id = req.param('user_id').trim();
      let data = await UserService.getProfile(seller_id);
     
      if (data.length ==0) {
        return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
      }
      let foundUserById = data[0];
      let textInput = foundUserById.id;
    
     
      foundUserById.avatar = sails.helpers.getMultipleUrl(foundUserById.avatar ? foundUserById.avatar :"");
      foundUserById.before_img_identity_card = sails.helpers.getMultipleUrl(foundUserById.before_img_identity_card ? foundUserById.before_img_identity_card :"");
      foundUserById.after_img_identity_card = sails.helpers.getMultipleUrl(foundUserById.after_img_identity_card ? foundUserById.after_img_identity_card :"");
      foundUserById.selfie_img = sails.helpers.getMultipleUrl(foundUserById.selfie_img ? foundUserById.selfie_img :"");

      let userBank = await BankService.getDetailSellerBank(seller_id); 
      foundUserById.banks = userBank;
      //.banks


   
      return res.ok(foundUserById);
    } catch (err) {
      return res.negotiate('getProfile', err);
    }
  },

  updateProfile: async (req, res) => {
    try {  
    
     
        const userMsg = sails.config.msg.user;
        let current_account_id = req.current_user.id;

        let user_id = req.body.user_id;
        let phone_number = req.body.phone_number;
        let fullname = req.body.fullname;
        let sex = req.body.sex || 1;
        let email = req.body.email || '';
        let birthday = req.body.birthday || null;

        let address = req.body.address || '';

        let objdataUpdate={};
        sails.log.debug(`updateProfile param ${JSON.stringify(req.body)}`);
        if (!user_id) {
            return res.badRequest(userMsg.err_user_id_required.code, userMsg.err_user_id_required.msg);
        }


        let foundUser = await UserService.findOneByCriteria({id: user_id}, [ 'id','verify_code','avatar']);

        if (_.isEmpty(foundUser)) {
          return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
        }

        if (!phone_number) {
           return res.badRequest(userMsg.err_phone_required.code, userMsg.err_phone_required.msg);
        }
      
        // if(!sails.helpers.validatePhoneNumber(phone_number)){
        //    return res.badRequest(userMsg.err_phone_format_invalid.code, userMsg.err_phone_format_invalid.msg);
        // }

          // check phone existed
        let findSellerByPhone = await UserService.findOneByCriteria({phone_number: phone_number,id: { '!=': user_id }}, [ 'id']);
        if (!_.isEmpty(findSellerByPhone)) {
          return res.serverError(userMsg.err_phone_exist.code, userMsg.err_phone_exist.msg);
        }

        if (!fullname.trim()) {
           return res.badRequest(userMsg.err_full_name_required.code, userMsg.err_full_name_required.msg);
        }

        objdataUpdate.id = user_id;
        objdataUpdate.fullname = fullname;
        objdataUpdate.phone_number = phone_number;
        objdataUpdate.address = address;
        objdataUpdate.birthday = birthday;
        objdataUpdate.email = email;
        objdataUpdate.created_by = current_account_id;
        objdataUpdate.updated_at = new Date();


        let dataUpdate = await UserService.updateUser(objdataUpdate);
  
        res.ok(dataUpdate);

    } catch (err) {
      return res.negotiate('getProfile', err);
    }
  },


  getWarningSeller: async (req, res)=>{
    try{
      let limit = req.param("limit") || sails.config.system.per_page;;
      let page = req.param("page") || 1;
      let txtSearch = req.param("txtSearch");
      sails.log.debug(`getWarningSeller param ${JSON.stringify(req.allParams())}`);
   
      let criterial = {};
      let page_start = (parseInt(page)-1)*parseInt(limit);
      if(txtSearch){
          criterial["fullname"] = {
            like: "%" + txtSearch.trim() + "%"
          };
      }

      criterial["reported_number"] = { '>': 0 };

        let arr_sort=[
          { reported_number:'DESC'},
           { id:'DESC'}
         ]
      sails.log.debug(`getWarningSeller criterial ${JSON.stringify(criterial)}`);
      let data_res =[];
      let list= await UserService.getUsers(criterial,parseInt(limit),page_start,arr_sort);
      let total = await UserService.countUserByCriterial(criterial);

       let pagination = sails.helpers.createPanigate(page,limit,total);

      let listSeller = list.map((item) => {
        item.avatar = sails.helpers.getMultipleUrl(item.avatar?item.avatar:"");
        item.before_img_identity_card = sails.helpers.getMultipleUrl(item.before_img_identity_card?item.before_img_identity_card:"");
        item.after_img_identity_card = sails.helpers.getMultipleUrl(item.after_img_identity_card?item.after_img_identity_card:"");
        item.selfie_img = sails.helpers.getMultipleUrl(item.selfie_img?item.selfie_img:"");
        return item ;
     });
      
      res.ok({listSeller,pagination});
    } catch (err) {
      return res.negotiate('getUser', err);
    }
  },


  getKycSeller: async (req, res)=>{
    try{
      let limit = req.param("limit") || sails.config.system.per_page;;
      let page = req.param("page") || 1;
      let txtSearch = req.param("txtSearch") || '';
      sails.log.debug(`getKycSeller param ${JSON.stringify(req.allParams())}`);
     let page_start = (parseInt(page)-1)*parseInt(limit);
   
      let criterial = {};
     
      let list= await UserService.getKycUser(txtSearch,page_start,parseInt(limit));
      let totalData = await UserService.countKycUser(txtSearch);

      let total  = totalData[0].total_user;


      let pagination = sails.helpers.createPanigate(page,limit,total);

      let listSeller = list.map((item) => {
        item.avatar = sails.helpers.getMultipleUrl(item.avatar?item.avatar:"");
        item.before_img_identity_card = sails.helpers.getMultipleUrl(item.before_img_identity_card?item.before_img_identity_card:"");
        item.after_img_identity_card = sails.helpers.getMultipleUrl(item.after_img_identity_card?item.after_img_identity_card:"");
        item.selfie_img = sails.helpers.getMultipleUrl(item.selfie_img?item.selfie_img:"");
        return item ;
     });
      
      res.ok({listSeller,pagination});
    } catch (err) {
      return res.negotiate('getUser', err);
    }
  },


  getListSeller: async (req, res)=>{
    try{
      let limit = req.param("limit") || sails.config.system.per_page;;
      let page = req.param("page") || 1;
      let txtSearch = req.param("txtSearch");
      let txtPhone = req.param("txtPhone");
      let txtStatus = req.param("txtStatus");

      sails.log.debug(`getListSeller param ${JSON.stringify(req.allParams())}`);
      let criterial = {};
      let page_start = (parseInt(page)-1)*parseInt(limit);
      if(txtSearch){
          criterial["fullname"] = {
            like: "%" + txtSearch + "%"
          };
      }

      if(txtPhone){
          criterial["phone_number"] = {
            like: "%" + txtPhone + "%"
          };
      }

      if(txtStatus){
          criterial["status"] =txtStatus;
      }

      let arr_sort=[
           { id:'DESC'}
         ]

       sails.log.debug(`getListSeller criterial ${JSON.stringify(criterial)}`);
      let data_res =[];
      let list= await UserService.getUsers(criterial,parseInt(limit),page_start,arr_sort);

      if(list.length == 0){
        return   res.ok();
      }

      let arr_id = list.map((user) =>{return user.id;});
      let totalReceiverComment = await CommentService.countCommentReceiver(arr_id);
      let  totalReceiver = _.mapValues(_.indexBy(totalReceiverComment, 'receiver_id'), cmt => cmt.total_comment);
      let totalSenderComment = await CommentService.countCommentSender(arr_id);
       let  totalSender = _.mapValues(_.indexBy(totalSenderComment, 'sender_id'), cmt => cmt.total_comment);
     
      let total = await UserService.countUserByCriterial(criterial);

       let pagination = sails.helpers.createPanigate(page,limit,total);

      let listSeller = list.map((item) => {
        item.seller_id = item.id;
        item.avatar = sails.helpers.getMultipleUrl(item.avatar);
        item.before_img_identity_card = sails.helpers.getMultipleUrl(item.before_img_identity_card);
        item.after_img_identity_card = sails.helpers.getMultipleUrl(item.after_img_identity_card);
        item.selfie_img = sails.helpers.getMultipleUrl(item.selfie_img);
        item.totalReceiverComment = totalReceiver[item.id] ? totalReceiver[item.id] :0 ;
        item.totalSenderComment = totalSender[item.id] ? totalSender[item.id] :0 ;
        return item ;
     });
      
      res.ok({listSeller,pagination});
    } catch (err) {
      return res.negotiate('getListSeller', err);
    }
  },


   createSeller: async (req, res) => {
    try {  
      const userMsg = sails.config.msg.user;
      
      let current_account_id = req.current_user.id;
      const fullname = req.param('fullname');
      const email = req.param('email');
      const phone_number = req.param('phone_number');
      const sex = req.param('sex') || 1;
      const birthday = req.param('birthday') || null;
      const address = req.param('address') || '';
       sails.log.debug(`createSeller param ${JSON.stringify(req.allParams())}`);
    
      let obj_created={};


      if (!phone_number.trim()) {
        return res.badRequest(userMsg.err_phone_required.code, userMsg.err_phone_required.msg);
      }
      let phone = phone_number.trim();

      // if(!sails.helpers.validatePhoneNumber(phone)){
      //    return res.badRequest(userMsg.err_phone_format_invalid.code, userMsg.err_phone_format_invalid.msg);
      // }

      // check phone existed
      let findSellerByPhone = await UserService.findOneByCriteria({phone_number: phone}, [ 'id']);
      if (!_.isEmpty(findSellerByPhone)) {
        return res.serverError(userMsg.err_phone_exist.code, userMsg.err_phone_exist.msg);
      }

      obj_created.phone_number = phone;

      if (!fullname) {
        return res.badRequest(userMsg.err_full_name_required.code, userMsg.err_full_name_required.msg);
      }

      obj_created.fullname = fullname;

      if (email) {
        let findSellerByEmail = await UserService.findOneByCriteria({email: email}, [ 'id']);
        if (!_.isEmpty(findSellerByEmail)) {
          return res.serverError(userMsg.err_email_unique.code, userMsg.err_email_unique.msg);
        }
      }

      obj_created.address = address;
      obj_created.sex = sex;
      obj_created.birthday = birthday;
      obj_created.email = email;
      obj_created.status = User.STATUS.Guest;
      obj_created.created_by = current_account_id;
      obj_created.created_at = new Date();

      let createdSeller = await UserService.createUser(obj_created);

      let avatar_size=  sails.config.system.resize_img.avatar;
      let idSeller = createdSeller.id
      let obj_update ={};

      let dataUpload =  await  UploadService.uploadImgToServer(req.file('avatar_images'), idSeller);
      let avatarUrl ='';
      if (dataUpload.length > 0) {

        let image = new Jimp(dataUpload[0].imgPath, function (err, image) {
            let width = image.bitmap.width ; 
            let height = image.bitmap.height; 
            avatar_size.height = height;
            obj_update.avatar_size= JSON.stringify({height:height,width:width}); 
          
        });

        dataUpload[0].id = idSeller;
        dataUpload[0].img_type = sails.config.system.s3_img_type.avatar;

        let path_avatar = UploadService.saveFile(dataUpload[0]);
        obj_update.avatar = path_avatar;
      }
     
     let dataUpdate =  await UserService.updateByCriteria({id:idSeller},obj_update);
     dataUpdate.avatar = sails.helpers.getMultipleUrl(dataUpdate.avatar);
    
   

      return res.ok(dataUpdate);

    } catch (err) {
      return res.negotiate('createSeller', err);
    }
  },

  updateSeller: async (req, res) => {
    try {  
      const userMsg = sails.config.msg.user;
      let bucket_name_s3 = process.env.BUCKET_NAME_S3;

      let current_account_id = req.current_user.id;

      const seller_id = req.param('seller_id');
      const fullname = req.param('fullname');
      const email = req.param('email');
      const phone_number = req.param('phone_number');
      const sex = req.param('sex') || 1;
      const birthday = req.param('birthday') || null;
      const address = req.param('address') || '';

      sails.log.debug(`updateSeller param ${JSON.stringify(req.allParams())}`);
      let obj_updated={};

      if (!seller_id) {
        return res.badRequest(userMsg.err_user_id_required.code, userMsg.err_user_id_required.msg);
      }

     let foundUser = await UserService.findOneByCriteria({id: seller_id}, [ 'id','verify_code','avatar']);
      if (_.isEmpty(foundUser)) {
        return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
      }


      if (!phone_number.trim()) {
        return res.badRequest(userMsg.err_phone_required.code, userMsg.err_phone_required.msg);
      }
      let phone = phone_number.trim();

      // if(!sails.helpers.validatePhoneNumber(phone)){
      //    return res.badRequest(userMsg.err_phone_format_invalid.code, userMsg.err_phone_format_invalid.msg);
      // }

       // check phone existed
      let findSellerByPhone = await UserService.findOneByCriteria({phone_number: phone,id: { '!=': seller_id }}, [ 'id']);
      if (!_.isEmpty(findSellerByPhone)) {
        return res.serverError(userMsg.err_phone_exist.code, userMsg.err_phone_exist.msg);
      }


      obj_updated.phone_number = phone;

      if (!fullname) {
        return res.badRequest(userMsg.err_full_name_required.code, userMsg.err_full_name_required.msg);
      }

      obj_updated.fullname = fullname;

      if (email) {
        let findSellerByEmail = await UserService.findOneByCriteria({email: email,id: { '!=': seller_id }}, [ 'id']);
        if (!_.isEmpty(findSellerByEmail)) {
          return res.serverError(userMsg.err_email_unique.code, userMsg.err_email_unique.msg);
        }
      }

      obj_updated.address = address;
      obj_updated.sex = sex;
      obj_updated.birthday = birthday;
      obj_updated.email = email;
      obj_updated.updated_by = current_account_id;
      obj_updated.updated_at = new Date();

      let avatar_size=  sails.config.system.resize_img.avatar;

      let dataUpload =  await  UploadService.uploadImgToServer(req.file('avatar_images'), seller_id);
  
      if (dataUpload.length > 0) {

        let image = new Jimp(dataUpload[0].imgPath, function (err, image) {
            let width = image.bitmap.width ; 
            let height = image.bitmap.height; 
            avatar_size.height = height;
            obj_updated.avatar_size= JSON.stringify({height:height,width:width}); 
          
        });

         if(foundUser.avatar){
           await  AwsService.emptyS3Directory(bucket_name_s3, seller_id+"/"+sails.config.system.s3_img_type.avatar+"/");
        }
        dataUpload[0].id = seller_id;
        dataUpload[0].img_type = sails.config.system.s3_img_type.avatar;
        let path_avatar = UploadService.saveFile(dataUpload[0]);
        obj_updated.avatar  = path_avatar;
      }
     
     let dataUpdate = await UserService.updateByCriteria({id:seller_id},obj_updated);
     dataUpdate.avatar = sails.helpers.getMultipleUrl(dataUpdate.avatar);
    
   

      return res.ok(dataUpdate);

    } catch (err) {
      return res.negotiate('updateSeller', err);
    }
  },

   deleteSeller: async (req, res)=>{
    try {
      
      const userMsg = sails.config.msg.user;
      let bucket_name_s3 = process.env.BUCKET_NAME_S3;

      let current_account_id = req.current_user.id;

      const seller_id = req.param('seller_id');
      sails.log.debug(`deleteSeller param ${JSON.stringify(req.allParams())}`);
      if (!seller_id) {
        return res.badRequest(userMsg.err_user_id_required.code, userMsg.err_user_id_required.msg);
      }
     
     
      let foundUser = await UserService.findOneByCriteria({id: seller_id}, [ 'id','verify_code','avatar']);
      if (_.isEmpty(foundUser)) {
        return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
      }

      await  AwsService.emptyS3Directory(bucket_name_s3, seller_id+"/"+sails.config.system.s3_img_type.avatar+"/");
      await UserService.deleteUser(seller_id,current_account_id);
    
      res.ok();
    } catch (err) {
          return res.negotiate('deleteSeller', err);
        }
    },


    updateStatus: async (req, res)=>{
      try {
        
        const userMsg = sails.config.msg.user;

        let current_account_id = req.current_user.id;
        let seller_id = req.body.seller_id;
        let note = req.body.note || '';
        let verify_code = req.body.verify_code;
        let status = req.body.status;

        sails.log.debug(`updateStatus param ${JSON.stringify(req.body)}`);
        if (!seller_id) {
          return res.badRequest(userMsg.err_user_id_required.code, userMsg.err_user_id_required.msg);
        }
      
        let foundUser = await UserService.findOneByCriteria({id: seller_id}, [ 'id','verify_code','avatar','status']);
        if (_.isEmpty(foundUser)) {
          return res.notFound(userMsg.err_user_not_found.code, userMsg.err_user_not_found.msg);
        }
        let text_status ='';
        if(!['Verify_level_one','Guest','Verify_level_two'].includes(status)){
           return res.badRequest(userMsg.err_param_status_invalid.code, userMsg.err_param_status_invalid.msg);
        }

        if (status ==User.STATUS.Guest) {
           text_status = 'guest';
           let dataUpdate = await UserService.updateByCriteria({id:seller_id},{verify_code:'',status:status,note:note});
           let body = `Admin đã cập nhật trạng thái seller của bạn thành ${text_status}.`;
           await HandleNotificationService.approveSellerStatus(seller_id,body,note);
            let data_notification = {
              payload: body + "\n" + note,
              affect_seller_id:seller_id,
              relation_seller_id:seller_id,
              notification_type:AdminNotification.TYPE.STATUS_SELLER,
              created_at:new Date(),
              created_by:current_account_id,
            };

           await AdminNotificationService.createAdminNotification(data_notification);

           return res.ok(dataUpdate);
        }

       
        if (!foundUser.verify_code) {

            if (verify_code) {

               if (isNaN(verify_code)) {
                   return res.badRequest(userMsg.err_verify_code_invalid.code, userMsg.err_verify_code_invalid.msg);
               }
                         
               if (status ==User.STATUS.Verify_level_one) {
                 verify_code = "Authvn"  + verify_code;
               }

               if (status ==User.STATUS.Verify_level_two) {
                   verify_code = "Vipauthvn" + verify_code;
               }

            }else{
              
                const random = uniqueRandom(1001, 10000);
                if (status ==User.STATUS.Verify_level_one) {
                    verify_code = "Authvn"  + random();
                 }

                if (status ==User.STATUS.Verify_level_two) {
                  verify_code = "Vipauthvn" + random();
                }
            }

        }else{
             let old_verify_code = foundUser.verify_code;
              let number_code ='';
             if (verify_code) {

              if (isNaN(verify_code)) {
                   return res.badRequest(userMsg.err_verify_code_invalid.code, userMsg.err_verify_code_invalid.msg);
               }
              
              if (status ==User.STATUS.Verify_level_one) {
                verify_code =  "Authvn"  + verify_code;
              }

               if (status ==User.STATUS.Verify_level_two) {
                verify_code =  "Vipauthvn"  + verify_code;
              }

           }else{

              if (old_verify_code.includes("Authvn")) {
                  number_code = old_verify_code.split("Authvn")[1];
              }

              if (old_verify_code.includes("Vipauthvn")) {
                number_code = old_verify_code.split("Vipauthvn")[1];
              }

              if (status ==User.STATUS.Verify_level_one) {
                verify_code =  "Authvn"  + number_code;
              }

               if (status ==User.STATUS.Verify_level_two) {
                verify_code =  "Vipauthvn"  + number_code;
              }
           }
            

        }

        let foundVerifyCode = await UserService.findOneByCriteria({id: { '!=': seller_id },verify_code: verify_code}, [ 'id']);
        if (!_.isEmpty(foundVerifyCode)) {
           return res.badRequest(userMsg.err_verify_code_exist.code, userMsg.err_verify_code_exist.msg);
        }

        let obj_updated={};

        obj_updated.status = status;
        obj_updated.note = note;
        obj_updated.verify_code = verify_code;
        obj_updated.updated_by = current_account_id;
        obj_updated.updated_at = new Date();

        let dataUpdate = await UserService.updateByCriteria({id:seller_id},obj_updated);

        if (status ==User.STATUS.Verify_level_one) {
          text_status =' xác thực cấp 1';
        }

         if (status ==User.STATUS.Verify_level_two) {
          text_status =' xác thực cấp 2';
        }

        let body = `Admin đã cập nhật trạng thái seller của bạn thành ${text_status}.`;
        let admin_notification = {
          payload: body + "\n" + note,
          affect_seller_id:seller_id,
          relation_seller_id:seller_id,
          notification_type:AdminNotification.TYPE.STATUS_SELLER,
          created_at:new Date(),
          created_by:current_account_id,
        };

        await AdminNotificationService.createAdminNotification(admin_notification);
       
        await HandleNotificationService.approveSellerStatus(seller_id, body + "\n" + note,note);
     

        return res.ok(dataUpdate);
      
      } catch (err) {
            return res.negotiate('updateStatus', err);
          }
      },

    uploadFileExcel: async (req, res)=>{
      try {
        const userMsg = sails.config.msg.user;
       //await  UploadService.uploadImgToServer(req.file('avatar_images'), currentUserId);
      let XLSX = require("xlsx");
      let current_account_id = req.current_user.id;
      let path_file_upload = await UploadService.uploadFiles(req);
      let workbook = XLSX.readFile(path_file_upload);
      const sheet_name_list = workbook.SheetNames;
      let listSeller = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);

      let listSellerValid = [];
      let listSellerInValid = [];

      for(let  [index, seller] of listSeller.entries()){
        let objSeller = {};
       
        objSeller.fullname = seller["FullName"];
        objSeller.address = seller["Address"];
        objSeller.trading_address = seller["Trading_address"];
        objSeller.phone_number = "0" +seller["Phone_Number"];
        objSeller.website = seller["Website"];
        objSeller.status = User.STATUS.Verify_level_one;
        objSeller.verify_code = seller["Verify_code"];
        objSeller.password = sails.config.system.PASS_DEAULT;
        objSeller.is_import = 1;
        objSeller.created_by = current_account_id;
        objSeller.created_at = new Date();

        if(!objSeller.fullname){
             objSeller.msg =  userMsg.err_full_name_required.msg;
             listSellerInValid.push(objSeller);
             continue;
        }

        if(!objSeller.phone_number){
             objSeller.msg =  userMsg.err_phone_required.msg;
             objSeller.index = index;
             listSellerInValid.push(objSeller);
             continue;
        }

        let foundByPhone = await UserService.findOneByCriteria({phone_number: objSeller.phone_number.trim()}, [ 'id']);
        if (!_.isEmpty(foundByPhone)) {
              objSeller.msg =  userMsg.err_phone_exist.msg;
              objSeller.index = index;
             listSellerInValid.push(objSeller);
             continue;
        }


        let foundVerifyCode = await UserService.findOneByCriteria({verify_code: objSeller.verify_code.trim()}, [ 'id']);
        if (!_.isEmpty(foundVerifyCode)) {
              objSeller.msg =  userMsg.err_verify_code_exist.msg;
               objSeller.index = index;
              listSellerInValid.push(objSeller);
             continue;
        }

        listSellerValid.push(objSeller);
      

      }
      if(listSellerInValid.length == 0){
        await UserService.bulkInsertsSeller(listSellerValid);
      }
      return res.ok({listSellerValid,listSellerInValid});
      
      } catch (err) {
            return res.negotiate('uploadFileExcel', err);
          }
      },

};
