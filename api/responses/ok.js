module.exports = function ok(data = [], message = '', code = 200) {
  let res = this.res;
  let body = {};
  body.success = 1;
  body.data = data;
  body.code = code;
  body.message = message ? message : 'OK';
  return res.json(body);
};
