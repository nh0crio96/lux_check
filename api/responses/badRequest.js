module.exports = function badRequest( errorCode = 'BAD REQUEST', message = '',data = []) {
  let res = this.res;
  let body = {};
  body.status = "error";
  body.error_code = errorCode;
  body.data = data;
  body.success = 0;
  body.message = message ? message : '';
  sails.log.error(JSON.stringify(message));
  res.status(200);
  return res.json(body);
};
