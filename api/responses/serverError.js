module.exports = function serverError(error_code = 'ERROR', message = 'Something went wrong', data = undefined) {

  let errorData = {};
  errorData.error_code = error_code;
  errorData.message = message;
  errorData.success = 0;
  if (data !== undefined) errorData.data = data;

  sails.log.error(JSON.stringify(errorData));

  return this.res.status(200).json(errorData);

};
