module.exports = {
  friendlyName: 'Get multiple url',
  sync: true,
  inputs: {
     url: {
      type: 'ref',
      required: false
    }
  },

 fn: (inputs, exits) => {
        let obj = {};

        let url = inputs.url;
        if(!url){
           return exits.success("");
        }
        let arrayImg = url ? url.split("/") : []
        let baseUrl= process.env.BASE_IMG_URL;
       
       
        if(arrayImg.length != 5){
          return exits.success(obj);
        }
    
       obj={
            high:  baseUrl +"/"+ arrayImg[1] +"/"+ arrayImg[2] +"/"+ "high" +"/"+ arrayImg[4],
            low:   baseUrl +"/"+ arrayImg[1] +"/"+ arrayImg[2] +"/"+ "low" +"/"+ arrayImg[4],
            medium:baseUrl +"/"+ arrayImg[1] +"/"+ arrayImg[2] +"/"+ "medium" +"/"+ arrayImg[4]
        }
            
        return exits.success(obj);
  }


};

