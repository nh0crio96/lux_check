module.exports = {
  friendlyName: 'Get single url',
  sync: true,
  inputs: {
     url: {
      type: 'ref',
      required: false
    }
  },

 fn: (inputs, exits) => {
   if(!inputs.url){
     return exits.success("");
   }
    return exits.success(process.env.BASE_IMG_URL + inputs.url);
  }


};

