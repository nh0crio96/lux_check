module.exports = {


  friendlyName: 'Validate website',


  sync: true,
  inputs: {
     website: {
      type: 'string',
      required: true
    }
  },

 fn: (inputs, exits) => {

    if(!inputs.website){
      return exits.success(false);
    }
    let url_regex =  /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;

    let check_regex = url_regex.test(inputs.website); 

    return exits.success(check_regex);

  }

};

