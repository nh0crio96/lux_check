module.exports = {
  friendlyName: 'Get radom number',
  description: '',
  sync: true,
  inputs: {
     min: {
      type: 'number',
      required: true
    },
    max: {
      type: 'number',
      required: true
    }
  },


  fn:  (inputs,exits) => {
    return exits.success(Math.floor(Math.random() * (inputs.max - inputs.min + 1) + inputs.min));
  }

};

