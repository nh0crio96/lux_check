module.exports = {

  friendlyName: 'Validate phone number',
  sync: true,
  inputs: {
     phone_number: {
      type: 'string',
      required: true
    }
  },

 fn: (inputs, exits) => {

    if(!inputs.phone_number){
      return exits.success(false);
    }
    let phone_regex = /((09|03|07|08|05|01|02)+([0-9]{8})\b)/g;

    let check_regex = phone_regex.test(inputs.phone_number); 

    return exits.success(check_regex);

  }


};

