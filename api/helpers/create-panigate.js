module.exports = {
  friendlyName: 'Create panigate ',
  description: '',
  sync: true,
  inputs: {
     current_page: {
      type: 'number',
      required: true
    },
    limit: {
      type: 'number',
      required: true
    },
    total_record: {
      type: 'number',
      required: true
    }
  },


  fn:  (inputs,exits) => {
    let page ={};
    let current_page = inputs.current_page;
    let size = inputs.limit;
    let totalElements = inputs.total_record;
    let totalPages =  Math.ceil(totalElements/size);
    let first = current_page === 1 ? true: false;
    let last = current_page === totalPages ? true: false;


    return exits.success({totalElements,last,totalPages,size,current_page,first});
  }

};

