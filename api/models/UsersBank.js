module.exports = {
 tableName: 'users_bank',
  attributes: {
    
    user_id: {
      type: 'number',
      required: true,
    },

    bank_id: {
      type: 'number',
      required: true,
    },
    
    account_name: {
      type: 'string',
      required: false,
    },

    branch_name: {
      type: 'string',
      required: false,
    },

    account_number: {
      type: 'string',
      required: true,
      unique: true,
    },
   
    created_at: {
      type: 'ref',
      columnType: 'datetime'
    },

    updated_at: {
      type: 'ref',
      columnType: 'datetime'
    },

    created_by: {
      type: 'number',
      required: false,
      allowNull: true
    },

    updated_by: {
      type: 'number',
      required: false,
      allowNull: true
    },
  

  },
};
