module.exports = {

  attributes: {
    sender_id:{
      type: 'number',
      required: true
    },
    receiver_id: {
      type: 'number',
      required: true
    },
    content: {
      type: 'string',
      required: true,
      description: 'content of comment',
    },
    created_at: {
      type: 'ref',
      columnType: 'datetime'
    },

    updated_at: {
      type: 'ref',
      columnType: 'datetime'
    }

  },


};