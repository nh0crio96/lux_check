module.exports = {
  attributes: {
   like_user_id:{
      type: 'number',
      required: true,
      description: 'like user_id',
    },
    affect_user_id: {
      type: 'number',
      required: true
    },

    is_delete: {
      type: 'number',
      isIn: [0, 1],
      defaultsTo: 0,
      description: '0 is activate, 1 is deleted'
    },
    created_at: {
      type: 'ref',
      columnType: 'datetime'
    }

  },


};