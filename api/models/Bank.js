module.exports = {
  tableName: 'banks',
  attributes: { 
    bank_name: {
      type: 'string',
      unique: true,
      required: true,
    },

    created_by: {
        allowNull: true,
      type: 'number'
    },
    updated_by: {
      allowNull: true,
      type: 'number'
    },
    created_at: {
      type: 'ref',
      columnType: 'datetime'
    },

    updated_at: {
      type: 'ref',
      columnType: 'datetime'
    },
  

  },
};

