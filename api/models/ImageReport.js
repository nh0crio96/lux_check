/**
 * NotificationSetting.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'image_reports',
  attributes: {
  
    report_id: {
      type: 'number',
      required: true,
      description: 'id of reported',
    },
    img_url: {
      type: 'string',
      required: true
    },
    created_at: {
      type: 'ref',
      columnType: 'datetime'
    }

  },


};

