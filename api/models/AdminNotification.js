module.exports = {
  tableName: 'admin_notification',
  attributes: {
    payload: {
      type: 'string',
      required: true
    },
    affect_seller_id: {
      type: 'number',
      required: true
    },
    relation_seller_id: {
      type: 'number',
      required: true,
      description: `seller id liên quan cho trường hợp tố cáo nhau, trường hợp thay đổi status thì giá trị giống với affect_seller_id` 
    },
     notification_type: {
      type: 'string',
      in: ['STATUS_SELLER', 'STATUS_REPORT']
    },
    created_at: {
      type: 'ref',
      columnType: 'datetime'
    },
    created_by: {
      type: 'number',
      allowNull: true
    }
  },

  TYPE: {
    STATUS_SELLER: 'STATUS_SELLER',
    STATUS_REPORT: 'STATUS_REPORT',
  },
};

