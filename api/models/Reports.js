module.exports = {

  attributes: {
    reporter_id:{
      type: 'number',
      required: true
    },
    affect_user_id: {
      type: 'number',
      required: true
    },
    reason_id: {
      type: 'number',
      required: true
    },
    description: {
      type: 'string',
      allowNull: true,
      required: false
    },
    status: {
      type: 'string',
      defaultsTo: 'Pending',
      in: ['Reject', 'Pending', 'Accept']
    },
    created_at: {
      type: 'ref',
      columnType: 'datetime'
    },

    updated_at: {
      type: 'ref',
      columnType: 'datetime'
    },
    handler_id: {
      type: 'number',
      required: false,
      allowNull: true,
      description: 'account id handle',
    },

    is_delete: {
      type: 'number',
      isIn: [0, 1],
      defaultsTo: 0,
      description: '0 is activate, 1 is deleted'
    }

  },

   STATUS: {
    Reject: 'Reject',
    Accept: 'Accept',
    Pending: 'Pending'
  },


};