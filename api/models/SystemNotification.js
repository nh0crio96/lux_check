module.exports = {
  tableName: 'system_notification',
  attributes: {
    title: {
      type: 'string',
      required: true
    },
    content: {
      type: 'string',
      required: true
    },
    created_at: {
      type: 'ref',
      columnType: 'datetime'
    },

    updated_at: {
      type: 'ref',
      columnType: 'datetime'
    },


    created_by: {
      type: 'number',
      allowNull: true
    },

    updated_by: {
      allowNull: true,
      type: 'number'
    
    }

  },
};

