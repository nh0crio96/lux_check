module.exports = {
  tableName: 'handle_report',
  attributes: {
    feedback_seller_id: {
      type: 'number',
      required: true
    },
    report_id: {
      type: 'number',
      required: true
    },
   
    feedback_content: {
      type: 'string',
      required: true
    },
    created_at: {
      type: 'ref',
      columnType: 'datetime'
    },

    updated_at: {
      type: 'ref',
      columnType: 'datetime'
    }

  },
};

