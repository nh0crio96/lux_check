module.exports = {
	attributes: {
		user_id: {
			type: 'number',
			required: true,
			description: 'User ID',
		},
		device_id: {
			type: 'string',
			required: true,
			maxLength: 100,
			description: 'Device ID',
		},
		os: {
			type: 'number',
			required: true,
			description: '1 for accept, 0 for not accept'
		},
		token: {
			type: 'string',
			required: false,
			maxLength: 300,
			description: 'Device token',
		},
		created_at: {
			type: 'ref',
			columnType: 'datetime'
		},
		updated_at: {
			type: 'ref',
			columnType: 'datetime'
		}
	},

};