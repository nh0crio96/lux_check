module.exports = {

  attributes: {
   
 
    reason_name: {
      type: 'string',
      unique: true,
    },
   
    created_at: {
      type: 'ref',
      columnType: 'datetime'
    },
  

  },
};
