/**
 * Notification.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
	attributes: {
		seller_id: {
			type: 'number',
            allowNull: false
		},
		affect_seller_id: {
			type: 'number',
            allowNull: false
		},
		target_type: {
			type: 'number',
            allowNull: false
		},
		target_id: {
			type: 'number',
            allowNull: false
		},
	
		is_read: {
			type: 'number'
		},
		meta_data: {
			type: 'string',
			allowNull: false
		},
        created_at: {
            type: 'ref',
            columnType: 'datetime'
        }
    },
	TYPES: {
		'LIKE': 1,
		'COMMENT': 2,
		'REPORT': 3,
		'ADMIN_CHANGE_REPORT_STATUTS': 4,
		'ADMIN_CHANGE_SELLER_STATUS': 5
	},
	
	TYPES_TEXT: {
		1: 'LIKE',
		2: 'COMMENT',
		3: 'REPORT',
		4: 'ADMIN_CHANGE_REPORT_STATUTS',
		5: 'ADMIN_CHANGE_SELLER_STATUS'

    },

    PUSH_MESSAGES: {
        'LIKE': '{{username}} đã thích  bạn',
        'COMMENT': '{{username}} đã gửi 1 bình luận cho bạn',
        'REPORT': '{{username}}  đã tố cáo hoạt động bán hàng của bạn'
    },

};

