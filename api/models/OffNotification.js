module.exports = {
  tableName: 'off_notification',
  attributes: {
    notifcation_id: {
      type: 'number',
      required: true,
       description: 'ID notifcation  seller_id want to off' 
    },
    seller_id: {
      type: 'number',
      required: true
    },
  
     notifcation_type: {
      type: 'string',
      in: ['ADMIN', 'SYSTEM'],
      description: 'type location from admin_notification or from system_notification' 
    },
    created_at: {
      type: 'ref',
      columnType: 'datetime'
    }
  },

   TYPE: {
    ADMIN: 'ADMIN',
    SYSTEM: 'SYSTEM'
  },
};



