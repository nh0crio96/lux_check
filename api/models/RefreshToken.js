module.exports = {
  tableName: 'refresh_token',
  attributes: { 
      
    user_id: {
      type: 'number',
      required: true,
    },

    token_refresh: {
      type: 'string',
      required: true
    },
   
    created_at: {
      type: 'ref',
      columnType: 'datetime'
    },

    updated_at: {
      type: 'ref',
      columnType: 'datetime'
    },
  
  

  },
};

