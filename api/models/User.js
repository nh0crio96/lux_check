/**
 * NotificationSetting.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */
 const bcrypt = require('bcrypt');
module.exports = {
  tableName: 'users',
  attributes: {
    fullname: {
      type: 'string',
      required: false,
      maxLength: 255,
      description: 'Full name of user',
    },
    address:{
      type: 'string',
      required: false,
      allowNull: true,
      description: 'address of user'
    },

    trading_address:{
      type: 'string',
      required: false,
      allowNull: true,
      description: 'trading address of user'
    },
    phone_number: {
      type: 'string',
      unique: true,
      required: false,
      maxLength: 15,
      minLength: 8,
      description: 'phone of user',
      example: '0356257325'
    },
    email: {
      type: 'string',
      unique: true,
      required: false,
      maxLength: 100,
      allowNull: true,
      description: 'email of user'
    },
    password: {
      type: 'string',
      allowNull: false,
      maxLength: 120,
      minLength: 8,
      description: 'Hashed password'
    },
    birthday: { type: 'ref', 
     columnType: 'datetime' 
    },
    province_id: {
      type: 'number',
      required: false,
      description: 'The province of user'
    },
    website: {
      type: 'string',
      required: false,
      description: 'The website of user'
    },
    avatar: {
      type: 'string',
      required: false,
      description: 'Avatar User'
    },

    before_img_identity_card: {
      type: 'string',
      required: false,
      description: 'before image identity card of user'
    },

    after_img_identity_card: {
      type: 'string',
      required: false,
      description: 'after image identity card of user'
    },

    selfie_img: {
      type: 'string',
      required: false,
      description: 'selfie image of user'
    },
  
    reported_number: {
      type: 'number',
      defaultsTo: 0,
      description: 'Number of user reported'
    },

    sex: {
      type: 'number',
      isIn: [0,1,2],
      defaultsTo: 0,
      description: '0: female ,1: male,2: orther'
    },

    like_notification: {
      type: 'number',
      isIn: [0, 1],
      defaultsTo: 1,
      description: '0 is not accept, 1 is accept'
    },

    cmt_notification: {
      type: 'number',
      isIn: [0, 1],
      defaultsTo: 1,
       description: '0 is not accept, 1 is accept'
    },

    common_notification: {
      type: 'number',
      isIn: [0, 1],
      defaultsTo: 1,
      description: '0 is not accept, 1 is accept'
    },

    total_like: {
      type: 'number',
      defaultsTo: 0
    },

     social_id: {
      type: 'number',
      allowNull: true,
      required: false,
      description: 'id facebook of user',
      example: '0356257325'
    },
    verify_code:{
      type: 'string',
      required: false,
      allowNull: true
    //  description: 'verify code : LUX+ID+generatenumber' 
    },

    status: {
      type: 'string',
      defaultsTo: 'Guest',
      in: ['Verify_level_one', 'Pending', 'Verify_level_two','Guest']
    },
    //kyc_level

    kyc_level: {
      type: 'number',
      defaultsTo: 1,
      in: [1, 2]
    },
   
    created_at: {
      type: 'ref',
      columnType: 'datetime'
    },
    updated_at: {
      type: 'ref',
      columnType: 'datetime'
    },

    created_by: {
      type: 'number',
       allowNull: true
    },
    updated_by: {
      type: 'number',
      allowNull: true
    },
    deleted_by: {
      type: 'number',
      allowNull: true
    },

    is_delete: {
      type: 'number',
      defaultsTo:0,
      allowNull: false
    },
    note: {
      type: 'string',
       allowNull: true,
      description: 'note from admin when change status of user'
    },

    describe_yourself: {
      type: 'string',
       allowNull: true,
      description: 'describe yourself'
    },

     avatar_size: {
      type: 'json'
    },

    is_import: {
      type: 'number',
      defaultsTo:0,
      allowNull: false
    },

    time_like: {
      type: 'ref',
      columnType: 'datetime'
    },
    time_comment: {
      type: 'ref',
      columnType: 'datetime'
    },

    time_report: {
      type: 'ref',
      columnType: 'datetime'
    },
    time_handle_report: {
      type: 'ref',
      columnType: 'datetime'
    }
  },

  STATUS: {
    Verify_level_one: 'Verify_level_one',
    Guest: 'Guest',
    Pending: 'Pending',
    Verify_level_two: 'Verify_level_two'
  },

  customToJSON: function() {
    return _.omit(this, ['password']);
  },

  beforeCreate: (user, cb) => {
    bcrypt.genSalt(10,(err, salt)=>{
      bcrypt.hash(user.password, salt,  (err, hash) =>{
        if (err) return cb(err);
        user.password = hash;
        return cb();
      });
    });
  },

  comparePassword: (password, user) => {
    return new Promise((resolve, reject) => {
      bcrypt.compare(password, user.password, (err, match) => {
        if (match) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    })

  },

  hashPassword: (password) => {
    return new Promise((resolve, reject) => {
      bcrypt.genSalt(10,  (err, salt)=> {
        bcrypt.hash(password, salt, function (err, hash) {
          if (err) {
            reject(err);
          } else {
            resolve(hash);
          }
        });
      });
    });
  },
};

