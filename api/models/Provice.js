module.exports = {
  tableName: 'provinces',
  attributes: {
    province_code: {
      type: 'string',
      required: false,
    },
 
    province_name: {
      type: 'string',
      unique: true,
    },
   
    created_at: {
      type: 'ref',
      columnType: 'datetime'
    },
  

  },
};

