/**
 * NotificationSetting.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */
 const bcrypt = require('bcrypt');
module.exports = {
  tableName: 'accounts',
  attributes: {
    username: {
      type: 'string',
      required: false,
      maxLength: 100
    },
 
    email: {
      type: 'string',
      unique: true,
      required: false,
      maxLength: 100,
      allowNull: true,
      description: 'email of user'
    },
    password: {
      type: 'string',
      allowNull: false,
      maxLength: 200,
      minLength: 6,
      description: 'Hashed password'
    },

     status: {
      type: 'string',
      defaultsTo: 'Acitvate',
    },
    created_at: {
      type: 'ref',
      columnType: 'datetime'
    },
    updated_at: {
      type: 'ref',
      columnType: 'datetime'
    },
     created_by: {
      type: 'number'
    },
    updated_by: {
      type: 'number'
    }


  },

  customToJSON: function() {
    return _.omit(this, ['password']);
  },

  beforeCreate: (user, cb) => {
    console.log("beforeCreate" + JSON.stringify(user));
    bcrypt.genSalt(10,(err, salt)=>{
      bcrypt.hash(user.password, salt,  (err, hash) =>{
        if (err) return cb(err);
        user.password = hash;
        return cb();
      });
    });
  },

  comparePassword: (password, user) => {
    return new Promise((resolve, reject) => {
      bcrypt.compare(password, user.password, (err, match) => {
        if (match) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    })

  },

  hashPassword: (password) => {
    return new Promise((resolve, reject) => {
      bcrypt.genSalt(10,  (err, salt)=> {
        bcrypt.hash(password, salt, function (err, hash) {
          if (err) {
            reject(err);
          } else {
            console.log("===hash==", hash)
            resolve(hash);
          }
        });
      });
    });
  },
};

